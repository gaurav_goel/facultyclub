module.exports = {
  'parser': 'babel-eslint',
  'env': {
    'browser': false,
    'node': true,
    'mocha': true,
    'es6': true
  },
  'ecmaFeatures': {
  },
  'extends': 'airbnb',
  'rules': {
    /* style */
    'brace-style': [1, 'stroustrup'],
    'semi': [1, 'never'],
    'quotes': [1, 'single'],
    'func-names': [0],
    'no-console': [0],
    'arrow-body-style': [1, 'as-needed', { requireReturnForObjectLiteral: false }],
    'array-bracket-spacing': [2, 'never'],
    'object-curly-spacing': [2, 'always'],
    'space-before-blocks': [2, 'always'],
    'no-use-before-define': [2, { 'functions': false, 'classes': true }],
    'new-cap': 0,
    'max-len': [2, 120],
    'camelcase': [0],
    'no-unused-vars': [2, { 'args': 'none' }],
  },
  'globals': {
    'React': false,
    'browser': false,
    'expect': false
  }
}
