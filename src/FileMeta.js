const mongoose = require('mongoose');

const fileMetaSchema = new mongoose.Schema({

  originalName: { type: String },
  encoding: { type: String },
  mimetype: { type: String },
  filename: { type: String },
  uri: { type: String },
  path: { type: String },
  size: { type: Number },
  owner: { type: mongoose.Schema.Types.ObjectId },

}, { timestamps: true });

const FileMeta = mongoose.model('FileMeta', fileMetaSchema);

module.exports = FileMeta;
