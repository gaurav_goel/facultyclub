const models = require('../models')
const ResponseBuilder = require('../helper/response-builder')
const tagsService = require('../services/tags')

exports.createTag = (req, res, next) => {
  const tag = req.body.tag
  const score = req.body.score

  if (!tag || !score) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid tag object provided. Please provide a valid tag and score.')
      .build()
    )
  }

  tagsService.getTag(tag)
  .then(persistedTag => {
    if (persistedTag) {
      res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('A tag with the same name already exists.')
        .build()
      )
    }
    return persistedTag
  }).then(persistedTag => {
    if (!persistedTag) {
      return tagsService.createTag({
        tag, score,
      }).then((tag) => {
        res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setMessage('A new tag has been successfully created.')
          .setData(tag)
          .build()
        )
      })
    }
  }).catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to create tag.')
      .build()
    )
  })
}

exports.deleteTag = (req, res, next) => {
  const tagId = req.params.tagId

  tagsService.deleteTag(tagId).then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('The tag has been successfully deleted.')
      .build()
    )
  }).catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to delete tag.')
      .build()
    )
  })
}

exports.getAllTags = (req, res, next) => {
  const offset = req.query && req.query.offset
  const count = req.query && req.query.count

  tagsService.getTags(offset, count).then(tags => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(tags)
      .build()
    )
  })
}