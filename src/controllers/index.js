const BasicController = require('./basic')
const UploadController = require('./upload')
const UserController = require('./user')

exports.basicController = BasicController
exports.uploadController = UploadController
exports.userController = UserController

exports.configureResources = (epilogue, models) => {
	UserController.configureResource(epilogue, models)
	BasicController.configureResource(epilogue, models)
}
