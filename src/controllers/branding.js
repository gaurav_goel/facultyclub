const ResponseBuilder = require('../helper/response-builder')
const brandingService = require('../services/branding')

exports.getUserBranding = (req, res, next) =>
  brandingService.getUserBranding(req.params.id)
  .then(branding => res.status(200).json(new ResponseBuilder()
    .setData(branding || {})
    .setStatus(200)
    .build()
  ))

exports.createOrUpdate = (req, res, next) => {
  const userId = req.params.id
  const brandingObj = {
    logo: req.body.logo,
    brandingText: req.body.brandingText,
    watermark: req.body.watermark,
    userId,
  }

  return brandingService.getUserBranding(userId)
  .then(branding => {
    if (branding) {
    	brandingObj.updatedOn = new Date()
      return branding.updateAttributes(brandingObj)
    }
    return brandingService.createBranding(brandingObj)
  }).then(branding => res.status(200).json(new ResponseBuilder()
    .setMessage('Branding information has been successfully updated.')
    .setData(branding)
    .setStatus(200)
    .build()
  )).catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to update branding information. Please try again later.')
      .setStatus(500)
      .build()
    )
  })
}

exports.deleteBranding = (req, res, next) => {
  const userId = req.params.id

  return brandingService.deleteBranding(userId)
  .then(branding => res.status(200).json(new ResponseBuilder()
    .setMessage('Branding has been successfully deleted.')
    .setStatus(200)
    .build()
  ))
}
