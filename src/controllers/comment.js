const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const notificationService = require('../services/notification')
const mailService = require('../helper/mailer')
const userService = require('../services/user')
const pushService = require('../services/push')

const findOrCreateFeedComment = (req, res) => {
  if (!req.body.entityId) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing entityId parameter')
      .setStatus(400)
      .build()
    )
  }

  if (!req.body.comment) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing comment parameter')
      .setStatus(400)
      .build()
    )
  }
  
  const moduleId = req.body.entityId
  const userId = req.params.tutorId
  const comment = req.body.comment
  const moduleType = 'feed'
  
  const commentObj = {
    moduleId,
    userId,
    moduleType,
    comment,
    createdOn: new Date(),
    updatedOn: new Date(),
  }
  let feedObj

  models.Feed.scope('base').findOne({
    where: {
      id: moduleId,
    },
  })
  .then(feed => {
    feedObj = feed
    return models.Comments.create(commentObj)
  })
  .then(comment => {
    // create notification
    return notificationService.createNotification(notificationService.TYPE_FEED_COMMENT, {
      actor: feedObj.tutor.name,
      actorId: userId,
      feed: feedObj.description,
    }, feedObj.tutorId)
  })
  .then(notification => {
    // send push notification
    if (commentObj.userId !== feedObj.tutorId) {
      userService.getUser(commentObj.userId).then(actor => {
        if (actor) {
          pushService.feedCommented({ actor: actor.name, postTitle: feedObj.description }, feedObj.tutorId)
          userService.getUser(feedObj.tutorId).then(tutorObj => {
            if (tutorObj && tutorObj.email) {
              mailService.sendMail(tutorObj.email, `The Faqulty Club: Comment Notification`, `${actor.name} has commented on your post`)
            }
          })
        }
      })
    }
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Comment was successfully added.')
      .setStatus(200)
      .setData(comment)
      .build()
    )
  })
}

const updateComment = (req, res) => {
  if (!req.body.comment) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing comment parameter')
      .setStatus(400)
      .build()
    )
  }

  models.Comments.findOne({
    where: {
      userId: req.params.tutorId,
      id: req.params.id,
    },
  })
  .then(comment => {
    if (!comment) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .build()
      )
    }

    comment.comment = req.body.comment || comment.comment
    comment.updatedOn = new Date()
    return comment.save({})
  })
  .then(comment => res.status(200).json(new ResponseBuilder()
    .setStatus(200)
    .setData(comment)
    .build()
  ))
  .catch(error => {
    const msg = 'Failed to update comment.'
    console.log(msg, error)
    return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage(msg)
        .build()
      )
  })
}

const deleteFeedComment = (req, res) => {
  const id = req.params.id
  const userId = req.params.tutorId

  const whereClause = {
    id,
    userId,
  }

  models.Comments.destroy({
    where: whereClause,
  })
  .then(() => res.status(200).json(new ResponseBuilder()
    .setStatus(200)
    .build()
  ))
}

const fetchFeedComment = (req, res) => {
  const id = req.params.id
  const userId = req.params.tutorId

  const whereClause = {
    id,
    userId,
  }

  models.Comments.findOne({
    where: whereClause,
  })
  .then(comment => res.status(200).json(new ResponseBuilder()
    .setStatus(200)
    .setData(comment)
    .build()
  ))
}

exports.postComment = (req, res, next) => {
  switch(req.query.type) {
    case 'feed':
      findOrCreateFeedComment(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}

exports.editComment = (req, res, next) => {
  switch(req.query.type) {
    case 'feed':
      updateComment(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}

exports.deleteComment = (req, res, next) => {
  switch(req.query.type) {
    case 'feed':
      deleteFeedComment(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}

exports.fetchComment = (req, res, next) => {
  switch(req.query.type) {
    case 'feed':
      fetchFeedComment(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}
