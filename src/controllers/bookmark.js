const models = require('../models')
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const R = require('ramda')

const VALID_BOOKMARK_TYPES = ['assignment', 'feed']

const GET_BASE_BOOKMARK_QUERY_ASIGNMENT = `select b.id, b.module_id as moduleId, b.module_type as moduleType, a.name, concat('Class ', c.class, ', ', s.name, ', ', ch.chapter_name) as description
  from bookmark b
  inner join assignments a
  on b.module_id = a.id
  inner join class c
  on a.class = c.id
  inner join subjects s
  on a.subject = s.id
  inner join chapters ch
  on ch.chapter_id = a.chapter`

const GET_BASE_BOOKMARK_QUERY_FEED = `select b.id, b.module_id as moduleId, b.module_type as moduleType, f.description as name, 'Feed content' as description
  from bookmark b
  inner join feed f
  on b.module_id = f.id`

const GET_ASSIGNMENT_BOOKMARK_BY_FOLDER_QUERY = `${GET_BASE_BOOKMARK_QUERY_ASIGNMENT}
  where b.folder_id = :folder_id and b.module_type = 'assignment'`

const GET_FEED_BOOKMARK_BY_FOLDER_QUERY = `${GET_BASE_BOOKMARK_QUERY_FEED}
  where b.folder_id = :folder_id and b.module_type = 'feed'`

const GET_UNCATEGORIZED_BOOKMARKS_QUERY_ASSIGNMENT = `${GET_BASE_BOOKMARK_QUERY_ASIGNMENT}
  where b.folder_id is null and b.tutor_id = :tutor_id and b.module_type = 'assignment'`

const GET_UNCATEGORIZED_BOOKMARKS_QUERY_FEED = `${GET_BASE_BOOKMARK_QUERY_FEED}
  where b.folder_id is null and b.tutor_id = :tutor_id and b.module_type = 'feed'`

const validateBookmarkType = type => R.contains(R.toLower(type), VALID_BOOKMARK_TYPES)

exports.createBookmark = (req, res, next) => {
  const moduleId = req.body.moduleId
  const moduleType = req.body.moduleType
  const folderId = req.body.folderId
  const tutorId = req.params.id
  const timestamp = new Date()

  if (!validateBookmarkType(moduleType)) {
    return res.status(400).json(
      new ResponseBuilder(400, 'Invalid module type provided.').build()
    )
  }

  models.Bookmark.findOne({
    where: {
      tutorId,
      moduleId,
      moduleType,
      folderId,
    }
  }).then(bookmark => {
    if (bookmark) {
      return res.status(200).json(
        new ResponseBuilder(200, 'Already bookmarked.', bookmark).build()
      )
    }
    else {
      models.Bookmark.create({
        folderId,
        tutorId,
        moduleId,
        moduleType,
      }).then(bookmark => {
		    return res.status(200).json(
		      new ResponseBuilder(200, 'Successfully bookmarked.', bookmark).build()
		    )
		  }, error => {
		    console.log(error)
		    return res.status(500).json(
		      new ResponseBuilder(500, 'Failed to create folder.').build()
		    )
		  })
    }
  })
}

exports.updateFolder = (req, res, next) => {
  const tutorId = req.params.id
  const id = req.params.folderId
  const name = req.body.name

  models.BookmarkFolder.findOne({
    where: {
      tutorId,
      id,
    }
  }).then(folder => {
    if (!folder) {
      return res.status(404).json(
        new ResponseBuilder(404, 'Invalid folder.').build()
      )
    }
    folder.name = name
    return folder.save()
  }).then(folder => res.status(200).json(
    new ResponseBuilder(200, 'Folder was successfully updated.', folder).build()
  )).catch(error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to update folder.').build()
    )
  })
}


exports.updateBookmark = (req, res, next) => {
  const tutorId = req.params.id
  const id = req.params.bookmarkId
  const moduleId = req.body.moduleId
  const moduleType = req.body.moduleType
  const folderId = req.body.folderId

  models.Bookmark.findOne({
    where: {
      tutorId,
      id,
    }
  }).then(bookmark => {
    if (!bookmark) {
      return res.status(404).json(
        new ResponseBuilder(404, 'Invalid folder.').build()
      )
    }
    bookmark.moduleId = moduleId || bookmark.moduleId
    bookmark.moduleType = moduleType || bookmark.moduleType
    bookmark.folderId = folderId || bookmark.folderId
    return bookmark.save()
  }).then(bookmark => res.status(200).json(
    new ResponseBuilder(200, 'Bookmark was successfully updated.', bookmark).build()
  )).catch(error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to update bookmark.').build()
    )
  })
}

exports.deleteBookmark = (req, res, next) => {
  const bookmarkId = req.params.bookmarkId
  const tutorId = req.params.id

  models.Bookmark.destroy({
    where: {
      id: bookmarkId,
      tutorId,
    }
  }).then(() => res.status(200).json(
    new ResponseBuilder(200, 'Bookmark was successfully deleted.').build()
  )).catch(error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to delete bookmark.').build()
    )
  })
}

exports.deleteFolder = (req, res, next) => {
  const tutorId = req.params.id
  const id = req.params.folderId

  models.Bookmark.findOne({
    where: {
      tutorId,
      folderId: id,
    }
  }).then(bookmark => {
    if (bookmark) {
      return res.status(400).json(
        new ResponseBuilder(400, 'Folder has bookmarks. Please remove the bookmarks from the folder or delete them.').build()
      )
    }
    return
  }).then(() => models.BookmarkFolder.destroy({
    where: {
      tutorId,
      id,
    }
  })).then(() => res.status(200).json(
    new ResponseBuilder(200, 'Folder was successfully deleted.').build()
  )).catch(error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to delete folder.').build()
    )
  })
}

exports.getAllBookmarked = (req, res, next) => {
  const tutorId = req.params.id

  models.sequelize.Promise.all([
    models.sequelize.query(
      GET_UNCATEGORIZED_BOOKMARKS_QUERY_ASSIGNMENT,
      {
        replacements: {
          tutor_id: tutorId,
        },  
        model: models.Bookmark,
      }
    ),
    models.sequelize.query(
      GET_UNCATEGORIZED_BOOKMARKS_QUERY_FEED,
      {
        replacements: {
          tutor_id: tutorId,
        },  
        model: models.Bookmark,
      }
    ),
    models.BookmarkFolder.findAll({
      where: {
        tutorId,
      }
    })
  ]).spread((assignmentBookmarks, feedBookmarks, folders) => {
    return res.status(200).json(
      new ResponseBuilder(200, undefined, {
        bookmarks: R.concat(assignmentBookmarks, feedBookmarks),
        folders,
      }).build()
    )
  })
  .catch(error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to fetch bookmarked entities.').build()
    )
  })
}

exports.getFolders = (req, res, next) => {
  models.BookmarkFolder.findAll({
    where: {
      tutorId: req.params.id,
    }
  }).then(folders => res.status(200).json(
      new ResponseBuilder(200, undefined, folders).build()
    ),
    error => {
      console.log(error)
      return res.status(500).json(
        new ResponseBuilder(500, 'Failed to fetch bookmark folders.').build()
      )
    }
  )
}

exports.getFolder = (req, res, next) => {
  const scope = req.query.scope
  const folderId = req.params.folderId
  const tutorId = req.params.id
  
  models.BookmarkFolder.findOne({
    where: {
      tutorId,
      id: folderId,
    }
  }).then(folder => {
    if (scope === 'detailed') {
    	models.sequelize.Promise.all([
    		models.sequelize.query(
	        GET_ASSIGNMENT_BOOKMARK_BY_FOLDER_QUERY,
	        {
	          replacements: {
	            folder_id: folderId,
	          },
	          model: models.Bookmark,
	        }
	      ),
	      models.sequelize.query(
	        GET_FEED_BOOKMARK_BY_FOLDER_QUERY,
	        {
	          replacements: {
	            folder_id: folderId,
	          },
	          model: models.Bookmark,
	        }
	      )
    	])
      .spread((assignmentBookmarks, feedBookmarks) => {
        folder.bookmarks = R.concat(assignmentBookmarks, feedBookmarks)
        return res.status(200).json(
          new ResponseBuilder(200, undefined, folder).build()
        )    
      })
    } else {
      return res.status(200).json(
        new ResponseBuilder(200, undefined, folder).build()
      )
    }
  }).then()
}

exports.createFolder = (req, res, next) => {
  const name = req.body.name
  const tutorId = req.params.id
  const timestamp = new Date()

  if (!name) {
    return res.status(400).json(
      new ResponseBuilder(400, 'Invalid folder name.').build()
    )
  }

  models.BookmarkFolder.findOne({
    where: {
      tutorId,
      name,
    }
  }).then(folder => {
    if (folder) {
      return res.status(409).json(
        new ResponseBuilder(409, 'Folder with the same name already exists.').build()
      )
    }
  }).then(() => {
    return models.BookmarkFolder.create({
      name,
      tutorId,
      createdOn: timestamp,
    })
  }, error => {
    console.log(error)
    return res.status(500).json(
      new ResponseBuilder(500, 'Failed to create bookmark folder.').build()
    )
  }).then(folder => {
    return res.status(200).json(
      new ResponseBuilder(200, undefined, folder).build()
    )
  })
}