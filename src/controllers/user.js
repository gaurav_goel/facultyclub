const Q = require('q')
const async = require('async')
const codeGenerator = require('voucher-code-generator')
const md5 = require('md5')
const passport = require('passport')
const jwt = require('jwt-simple')
const models = require('../models')
const mailer = require('../helper/mailer')
const logger = require('../config/logger').logger
const moment = require('moment')
const _ = require('lodash')
const R = require('ramda')
const URL = require('url')
const smsService = require('../services/sms')
const helperService = require('../services/helper')
const userService = require('../services/user')
const pushService = require('../services/push')

const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')

const TOKEN_LENGTH = 6

const SUPPORTED_SIGNUP_SOURCES = ['local', 'google', 'facebook']
const REGISTRATION_PHASE_PERSONAL = 'personal'
const REGISTRATION_PHASE_TUITION = 'tuition'
const REGISTRATION_PHASE_QUALIFICATION = 'qualification'
const REGISTRATION_PHASE_SOCIAL = 'social'

const REGISTRATION_PHASE_DFS = {
  undefined: REGISTRATION_PHASE_PERSONAL,
  [REGISTRATION_PHASE_PERSONAL]: REGISTRATION_PHASE_TUITION,
  [REGISTRATION_PHASE_TUITION]: REGISTRATION_PHASE_QUALIFICATION,
  [REGISTRATION_PHASE_QUALIFICATION]: REGISTRATION_PHASE_SOCIAL,
}

const alphabetic = 'abcdefghijklmnopqrstuvwxyz'
const numeric = '123456789'

const TOKEN_EXPIRATION_TIME = 1800000

const { auth/*, userTypes*/, completenessConfig } = require('../config/application')
const userTypes = {
  tutor: 2,
  tuition_centre: 3,
  student: 4,
}


const getBasicProfile = (id, cb, errCb) => models.User.findById(id).then(cb, errCb)
const getTuitionProfile = (id, cb, errCb) => models.User.scope('tuition').findById(id).then(cb, errCb)
const getQualificationProfile = (id, cb, errCb) => models.User.scope('qualification').findById(id).then(cb, errCb)

const valdiatePasswordUpdate = (req) => {
  req.assert('phone', 'Mobile number must be 10 digits long').isInt().len(10)
  if (req.body.token) {
    req.assert('token', 'The OTP token must be 6 digits long').isInt().len(6)
  }
  req.assert('password', 'Password must be at least 4 characters long.').len(4)
  req.assert('confirmPassword', 'Passwords must match.').equals(req.body.password)

  return req.validationErrors()
}

const getUserLikes = (id, cb) => {
  models.User.scope('likes').findOne({
    where: {
      id,
    },
  }).then(cb)
}

const getUserReviewsPromise = (id) => models.TutorUserReviews.findAll({
    where: {
      tutorId: id,
    },
    include: [
      {
        model: models.User,
        as: 'user',
        attributes: ['id', 'name', 'email', 'phone', 'image'],
      }
    ]
  })

const getUserReviews = (id, cb) => {
  getUserReviewsPromise(id).then(cb)
}

const checkUserType = (actualId, suppliedId) => {
  return actualId === +(suppliedId) ? true : false
}

validateSocialURI = (link, baseURI, socialNetwork, noPathNameError) => {
  const baseURI1 = `www.${baseURI}`
  try {
    if (link && link.length) {
      const uri = URL.parse(link)
      console.log(uri.hostname, uri.pathname)
      if ((uri.hostname !== baseURI && uri.hostname !== baseURI1) || !uri.pathname || uri.pathname.length <= 1) {
        return `Please enter a valid ${socialNetwork} URI`
      }
    }
  } catch (err) {
    console.log(err)
    return `Invalid ${socialNetwork} URI`
  }
  return null
}

const validateSocialURLs = props => {
  const fblinkError = validateSocialURI(props.fblink, 'facebook.com', 'Facebook')
  const lnlinkError = validateSocialURI(props.lnlink, 'linkedin.com', 'LinkedIn')
  const twitterlinkError = validateSocialURI(props.twitterlink, 'twitter.com', 'Twitter')
  const errors = []
  if (fblinkError) {
    errors.push(fblinkError)
  }
  if (lnlinkError) {
    errors.push(lnlinkError)
  }
  if (twitterlinkError) {
    errors.push(twitterlinkError)
  }

  return errors
}

const updateBasicInfo = (req, user, cb) => {
  // Checking if the userType does not belong to the user
  // const correctUserType = checkUserType(user.userType, req.body.userType)
  // if(!correctUserType) {
  //   cb(null, null, {msg: "Incorrect User Type supplied for the user"})
  // }

  // req.assert('userType', 'Please enter user type').notEmpty()
  if (userTypes.tutor === +(user.userType)) {
    req.assert('gender', 'Plase select gender').notEmpty()
    req.assert('userDetails', 'Please describe yourself').notEmpty()
  }
  else if (userTypes.tuition_centre === +(req.body.userType)) {
    req.assert('userDetails', 'Please describe your tuition center').notEmpty()
  }
  else if (userTypes.student === +(req.body.userType)) {
   req.assert('dateOfBirth', 'Please enter your date of birth').notEmpty() 
  }
  // TODO
  // req.assert('courses', 'Please select a course').notEmpty()
  req.assert('email', 'Please enter a valid email address').isEmail()
  req.assert('name', 'Please enter your full name').notEmpty()

  let errors = req.validationErrors()
  if (errors) {
    cb({
      errors: errors,
    })
    return
  }

  const nextProps = {}

  nextProps.image = !R.isNil(req.body.image) ? req.body.image : user.image
  nextProps.name = req.body.name || user.name
  nextProps.gender = req.body.gender || user.gender
  nextProps.email = req.body.email || user.email
  nextProps.dateOfBirth = req.body.dateOfBirth || user.dateOfBirth
  nextProps.tagline = !R.isNil(req.body.tagline) ? req.body.tagline : user.tagline
  nextProps.userDetails = req.body.userDetails || user.userDetails
  nextProps.registrationPhase = REGISTRATION_PHASE_DFS[REGISTRATION_PHASE_PERSONAL]
  
  nextProps.fblink = req.body.fblink || ''
  nextProps.lnlink = req.body.lnlink || ''
  nextProps.twitterlink = req.body.twitterlink || ''

  errors = validateSocialURLs(nextProps)
  if (errors && errors.length) {
    cb({
      errors: errors,
    })
    return
  }

  if (+(user.userType) === userTypes.tuition_centre) {
    nextProps.gender = null
  } else {
    // TODO: clear faculty details
  }

  user.profileCompleteness = 'personalDetail'
  if (req.body.tutorLocation) {
    user.profileCompleteness = 'location'
  }
  if (nextProps.fblink.length || nextProps.lnlink.length || nextProps.twitterlink.length) {
    user.profileCompleteness = 'social'
  }
  nextProps.profileCompleteness = user.rawPofileCompleteness
  // not sure why, but, Nitin requested this
  nextProps.active = 1

  const promises = []
  // tutorLocation must be changed to userLocation
  if (req.body.tutorLocation) {
    let isPrimaryLocationSet = false
    R.forEach((location) => {
      // check if the location sent is valid
      if (!location.city || !location.locality) {
        throw new Error("Invalid location structure.")
      }
      if (location.isPrimary) {
        isPrimaryLocationSet = true
      }
    }, req.body.tutorLocation)
    if (!isPrimaryLocationSet) {
      throw new Error("Primary location is mandatory.")
    }
    req.body.tutorLocation.map((location) => {
      if (location.isPrimary === true || location.isPrimary === 'true') {
        nextProps.city = location.city
        nextProps.location = location.locality
      } else {
        location.tutor_id = user.id
        location.tutorId = user.id
        location.city = location.city
        location.locality = location.locality
        location.createdOn = new Date()
        promises.push(helperService.createOrUpdate(models.SubCityLocality, location))
      }
    })

    // remove unwanted locations
    const toBeDeletedLocations = helperService.oneToManyCleaner(req.body.tutorLocation, user.tutorLocation)
    if (toBeDeletedLocations && toBeDeletedLocations.length) {
      promises.push(models.SubCityLocality.destroy({
        where: {
          id: {
            $in: toBeDeletedLocations,
          },
        },
      }))
    }
  }

  models.sequelize.Promise.all(promises)
  .spread(function() {
    const tutorLocationSet = []
    if (arguments && arguments.length) {
      for (let i = 0; i < arguments.length; i++) {
        tutorLocationSet.push(arguments[i])
      }
    }
    
    return user.updateAttributes(nextProps).then(() => {
      userService.updateSearchScore(user.id)
      models.User.findById(user.id).then((updatedUser) => {
        cb(updatedUser)
      })
    }, (error) => {
      logger.error('Failed to save an object', error)
      cb(null, null, error)
    })
  }).catch(function(err) {
    console.log(err)
    cb(null, null, err)
  })
}

const updateUserExtraInfo = (req, user, cb) => {
  req.assert('phone', 'Plase select gender').notEmpty()
  req.assert('userType', 'Please enter user type').notEmpty()

  const errors = req.validationErrors()
  if (errors) {
    cb(null, null, errors)
    return
  }

  const nextProps = {}
  const isPhoneBeingUpdated = req.body.phone && !user.phone

  nextProps.phone = req.body.phone || user.phone
  nextProps.userType = userTypes[req.body.userType] || user.userType
  if (isPhoneBeingUpdated) {
    nextProps.phoneVerified = 0  
  }

  if (!nextProps.userType) {
    cb(null, null, 'Invalid user type value')  
  }

  user.updateAttributes(nextProps).then(() => {
    models.User.findById(user.id).then((updatedUser) => {
      if (isPhoneBeingUpdated) {
        const verificationCode = generateToken(TOKEN_LENGTH, numeric)
        const userOTP = models.UserOTP.build({
          mobile: user.phone,
          email: user.email,
          otp: verificationCode,
          verified: 0,
          createdOn: new Date(),
        })
        userOTP.save().then((verification) => {
          smsService.smsOTP(updatedUser, verificationCode)
          cb(updatedUser, `OTP sent to mobile ${req.body.phone}`)
        })
      } else {
        cb(updatedUser)
      }
    })
  }, (error) => {
    logger.error('Failed to save an object', error)
    cb(null, error)
  })
}

const findOrCreate = (model, props, scope) => {
  if (!props) {
    return
  }

  const create = () => model.create(props)
  let returnPromise

  if (props.id) {
    const scopedModel = scope ? model.scope(scope) : model
    return scopedModel.findById(props.id)
    .then((obj) => {
      if (!obj) {
        delete props.id
        return create()
      }
      else {
        return obj.updateAttributes(props)
      }
    })
  } else {
    return create()
  }  
}

const updateTuitionInfo = (req, user, cb) => {
  let delayPromise = false
  user.experience = req.body.experience || 0

  const triggerPromises = () => {
    models.sequelize.Promise.all(promises)
    .spread((user) => {
      if (user) {
        userService.updateSearchScore(user.id)
      }
      cb(user)
      return
    }).catch(function(err) {
      logger.error('updateTuitionInfo', err)
      cb(null, null, err)
      return
    })
  }

  user.profileCompleteness = 'tuition'

  const timestamp = new Date()
  const promises = [user.save()]

  if (req.body.tutorBoard) {
    promises.push(user.setTutorBoard(req.body.tutorBoard))
  } else {
    promises.push(user.setTutorBoard(null))
  }
  if (req.body.tutorTeachingLanguage) {
    promises.push(user.setTutorTeachingLanguage(req.body.tutorTeachingLanguage))
  } else {
    promises.push(user.setTutorTeachingLanguage(null))
  }
  if (req.body.tutorTeachingMode) {
    promises.push(user.setTutorTeachingMode(req.body.tutorTeachingMode))
  } else {
    promises.push(user.setTutorTeachingMode([]))
  }

  // sanitize input for one time tution charge
  if (req.body.oneOnOneTutionCharge) {
    user.oneOnOneTutionCharge = req.body.oneOnOneTutionCharge
    user.oneOnOneTutionCharge.tutor_id = user.id
    promises.push(helperService.createOrUpdate(models.OneOnOneTutionCharge, user.oneOnOneTutionCharge))
  } else {
    promises.push(user.setOneOnOneTutionCharge(null))
  }

  // sanitize input for group time tution charge
  if (req.body.groupTutionCharge) {
    if (userTypes[user.userType] === userTypes.tuition_centre) {
      if (req.body.groupTutionCharge.batchSize
        && !isNaN(req.body.groupTutionCharge.batchSize)
        && req.body.groupTutionCharge.batchSize > 0
      ) {
        cb(null, null, 'Batch size must be a number greater than 0')
        return
      }
    }
    user.groupTutionCharge = req.body.groupTutionCharge
    user.groupTutionCharge.tutor_id = user.id

    promises.push(helperService.createOrUpdate(models.GroupTutionCharge, user.groupTutionCharge))
  } else {
    promises.push(user.setGroupTutionCharge(null))
  }

  // sanitize input for weekday timings
  if (req.body.weekdayTutionTime) {
    user.weekdayTutionTime = req.body.weekdayTutionTime
    user.weekdayTutionTime.tutor_id = user.id
    promises.push(helperService.createOrUpdate(models.WeekdayTutionTime, user.weekdayTutionTime))
  } else {
    promises.push(user.setWeekdayTutionTime(null))
  }

  // sanitize input for weekend timings
  if (req.body.weekendTutionTime) {
    user.weekendTutionTime = req.body.weekendTutionTime
    user.weekendTutionTime.tutor_id = user.id
    promises.push(helperService.createOrUpdate(models.WeekendTutionTime, user.weekendTutionTime))
  } else {
    promises.push(user.setWeekendTutionTime(null))
  }

  if (req.body.tutorSchool) {
    const toBeDeletedSchools = helperService.oneToManyCleaner(req.body.tutorSchool, user.tutorSchool)
    if (toBeDeletedSchools && toBeDeletedSchools.length) {
      promises.push(models.TutorSchool.destroy({
        where: {
          id: {
            $in: toBeDeletedSchools,
          },
        },
      }))
    }

    R.forEach((school) => {
      school.tutorId = user.id
      school.createdOn = timestamp
      promises.push()
      promises.push(findOrCreate(models.TutorSchool, school))
    }, req.body.tutorSchool)
  } else {
    promises.push(user.setTutorSchool([]))
  }

  // sanitize and persist the courses taught
  if (req.body.coursesTaught && req.body.coursesTaught.length) {
    delayPromise = true
    R.forEach((course) => {
      if (course.subjectId && course.classes && course.classes.length) {
        R.forEach((clazz) => {
          promises.push(helperService.createOrUpdate(models.TutorCourseDetail, {
            tutorId: user.id,
            subject: course.subjectId,
            class: clazz.id,
          }))
        }, course.classes)
      }
    }, req.body.coursesTaught)
    models.TutorCourseDetail.destroy({
      where: {
        tutorId: user.id,
      }
    }).then(() => {
      triggerPromises()
    })
  } else {
    promises.push(user.setTutorCourseDetail([]))
  }

  if (!delayPromise) {
    triggerPromises()
  }
}

/*
 * College is not mandatory.
 */
const updateEducationDetailModel = (educationDetail)  => {
  const deferred = Q.defer()
  educationDetail.prefered = educationDetail.prefered || 0
  const clonedEducationDetail = R.clone(educationDetail)
  delete clonedEducationDetail.tutorDegree
  delete clonedEducationDetail.tutorDegreeSubject
  delete clonedEducationDetail.tutorInstitute
  delete clonedEducationDetail.tutorInstituteAward
  const promises = [helperService.createOrUpdate(models.TutorEducationDetail, clonedEducationDetail)]

  if (educationDetail.tutorDegree) {
    // TODO: Update the default seeds
    educationDetail.tutorDegree.level = 0
    educationDetail.tutorDegree.active = 0

    promises.push(findOrCreate(models.TutorDegree, educationDetail.tutorDegree))
  } else {
    promises.push(null)
  }
  if (educationDetail.tutorDegreeSubject) {
    // TODO: Update the default seeds
    educationDetail.tutorDegreeSubject.active = 0
    promises.push(findOrCreate(models.TutorDegreeSubject, educationDetail.tutorDegreeSubject))
  } else {
    promises.push(null)
  }
  if (educationDetail.tutorInstitute && educationDetail.tutorInstitute.id) {
    // TODO: Update the default seeds
    educationDetail.tutorInstitute.level = 0
    promises.push(models.TutorInstitute.findById(educationDetail.tutorInstitute.id)) 
  } else {
    promises.push(null)
  }
  if (educationDetail.tutorInstituteAward) {
    promises.push(findOrCreate(models.TutorInstituteAward, educationDetail.tutorInstituteAward)) 
  } else {
    promises.push(null)
  }

  models.sequelize.Promise.all(promises)
  .spread((education, tutorDegree, tutorDegreeSubject, tutorInstitute, tutorInstituteAward) => {
    const newPromises = [education]

    newPromises.push(education.setTutorDegree(tutorDegree))
    newPromises.push(education.setTutorDegreeSubject(tutorDegreeSubject))
    newPromises.push(education.setTutorInstitute(tutorInstitute))
    newPromises.push(education.setTutorInstituteAward(tutorInstituteAward))

    models.sequelize.Promise.all(newPromises)
    .spread((education, tutorDegree, tutorDegreeSubject, tutorInstitute, tutorInstituteAward) => {
      education.tutorDegree = tutorDegree
      education.tutorDegreeSubject = tutorDegreeSubject
      education.tutorInstitute = tutorInstitute
      education.tutorInstituteAward = tutorInstituteAward
      deferred.resolve(education)
    }).catch(function(err) {
      logger.error('updateEducationDetailModel:newPromises', err)
      deferred.reject(err)
      return
    })
  })
  .catch(function(err) {
    logger.error('updateEducationDetailModel', err)
    deferred.reject(err)
  })

  return deferred.promise
}

const updateTutorEducationDetail = (user, tutorEducationDetail) => {
  const deferred = Q.defer()
  const educationDetailSet = []
  const promises = []

    R.forEach((educationDetail) => {
      educationDetail.tutorId = user.id
      promises.push(updateEducationDetailModel(educationDetail))
    }, tutorEducationDetail)

  models.sequelize.Promise.all(promises)
  .spread(function(education) {
    if (arguments && arguments.length) {
      for (let i = 0; i < arguments.length; i++) {
        educationDetailSet.push(arguments[i])
      }
      user.setTutorEducationDetail(educationDetailSet).then((userWithEducationDetails) => {
        deferred.resolve(educationDetailSet)
      })
    } else {
      deferred.resolve(user)
    }
  }).catch(function(err) {
    logger.error('updateTutorEducationDetail', err)
    deferred.resolve(undefined)
  })

  return deferred.promise
}

const updateTutorAwardRecognition = (user, tutorAwards) => {
  const deferred = Q.defer()
  const awardSet = []
  const promises = []

  awards = tutorAwards || []

  awards.map((award) => {
    award.tutor_id = user.id
    award.tutorId = user.id
    promises.push(helperService.createOrUpdate(models.TutorAwardsRecognition, award))
  })

  models.sequelize.Promise.all(promises)
  .spread(function() {
    if (arguments && arguments.length) {
      for (let i = 0; i < arguments.length; i++) {
        awardSet.push(arguments[i])
      }
      deferred.resolve(user.setTutorAwards(awardSet))
    } else {
      deferred.resolve(user)
    }
  })
  return deferred.promise
}

const updateQualificationInfo = (req, user, cb) => {
  user.experience = req.body.experience !== undefined
    ? req.body.experience : user.experience
  user.prefered = req.body.prefered === true ? 1 : 0
  user.profileCompleteness = 'qualification'
  const promises = [user.save()]
  if (req.body.tutorEducationDetail) {
    promises.push(updateTutorEducationDetail(user, req.body.tutorEducationDetail))
  }
  else {
    promises.push(user.setTutorEducationDetail([]))
  }
  if (req.body.tutorAwards) {
    promises.push(updateTutorAwardRecognition(user, req.body.tutorAwards))
  } else {
    promises.push(user.setTutorAwards([]))
  }

  // remove education details
  let toBeCleaned = helperService.oneToManyCleaner(req.body.tutorEducationDetail, user.tutorEducationDetail)
  if (toBeCleaned && toBeCleaned.length) {
    promises.push(models.TutorEducationDetail.destroy({
      where: {
        id: {
          $in: toBeCleaned,
        },
      },
    }))
  }
  // remove unwanted awards
  toBeCleaned = helperService.oneToManyCleaner(req.body.tutorAwards, user.tutorAwards)
  if (toBeCleaned && toBeCleaned.length) {
    promises.push(models.TutorAwardsRecognition.destroy({
      where: {
        id: {
          $in: toBeCleaned,
        },
      },
    }))
  }

  models.sequelize.Promise.all(promises)
  .spread(updatedUser => {
    if (updatedUser) {
      userService.updateSearchScore(updatedUser.id)
    }
    cb(updatedUser)
  }, err => {
    console.log('Failed to update user qualifications.', err)
    cb(null, null, err)
  }).catch(err => {
    console.log(err)
    cb(null, null, err)
  })
}

exports.tokenAnalyzer = (req, res, next) => {
  var token = req.body.token || req.query.token || req.headers['x-access-token']

  if (token) {
    if (token == "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjo1MjR9") {
      next()
    } else {
      let decoded
      try {
        decoded = jwt.decode(token, auth.secret)
      } catch (err) {
        return res.status(403).send({
          msg: 'Invalid auth token.' 
        })
      }

      models.User.findById(decoded.user).then((user) => {
        req.currentUser = user
        next()
      }, (err) => {
        return res.status(403).send({
          msg: 'Invalid auth token.'
        })
      })
    }
  } else {
    return res.status(403).send({
      msg: 'No auth token provided.'
    })
  }
}

exports.likes = (req, res, next) => {
  getUserLikes(req.params.id, (users) => {
    if (users) {
      res.json(users.get({
        plain: true,
      }).userLikes)
    } else {
      res.json({})
    }
  })
}

exports.reviews = (req, res, next) => {
  getUserReviews(req.params.id, (reviews) => {
    if (reviews) {
      res.json(reviews)
    } else {
      res.json([])
    }
  })
}

exports.update = (req, res, next) => {
  let scope = 'basic'
  let updateFn
  switch(req.query.type) {
    case 'basic':
      updateFn = updateBasicInfo
      break
    case 'extra-info':
      updateFn = updateUserExtraInfo
      break
    case 'tuition':
      scope = 'tuition'
      updateFn = updateTuitionInfo
      break
    case 'qualification':
      scope = 'qualification'
      updateFn = updateQualificationInfo
      break
    default:
      updateFn = updateBasicInfo
  }
  try {
  getScopedUserProfile(req.params.id, scope, (response) => {
    if (response.data) {
      try {
        updateFn(req, response.data, (data) => {
          if (!data) {
            return res.status(500).send({ msg: 'Unknown exception occured. Please check request body.' })
          }
          if (data.errors && data.errors.length) {
            return res.status(400).json(new ResponseBuilder()
              .setStatus(400)
              .setMessage(`Failed to update user.`)
              .setErrors(data.errors)
              .build()
            )
          } else {
            getScopedUserProfile(req.params.id, scope, (updatedData) => {
              return res.status(200).send(updatedData.message ? {msg: updatedData.message} : updatedData.data)
            })
          }
        })
      } catch (err) {
        return res.status(400).send({ msg: err.message })
      }
    } else {
      logger.error(response.errors)
      return res.status(response.status).json(new ResponseBuilder()
        .setMessage(response.message)
        .setStatus(response.status)
        .setErrors(response.errors)
        .build()
      )
    }
  })
  } catch(err) {
    console.log(err)
  }
}

exports.configureResource = (epilogue, models) => {
  // const users = epilogue.resource({
  //   model: models.User,
  //   endpoints: ['/users', '/users/:id'],
  //   actions: ['create', 'read'],
  //   // associations: true,
  // })
}



const isValidSource = (source) => {
  const deferred = Q.defer()
  if (SUPPORTED_SIGNUP_SOURCES.indexOf(source) > -1) {
    deferred.resolve()
  } else {
    deferred.reject({
      msg: `Invalid signup source: ${source}`
    })
  }

  return deferred.promise
}

const isAuthorizedSignUp = (loggedInUser, email) => {
  const deferred = Q.defer()
  if (!loggedInUser || email === loggedInUser.email) {
    deferred.resolve()
  } else {
    deferred.reject({
      msg: 'Unauthorized signup/profile update request'
    })
  }
  return deferred.promise
}

const isValidPassword = (confirmPassword, password, source, user) => {
  const deferred = Q.defer()
  if (source === 'local' && !user) {
    if (!password) {
      deferred.reject({ msg: 'Password is mandatory' })
    }
    if (password !== confirmPassword) {
      deferred.reject({ msg: 'Password and confirm password do not match' })
    }
    deferred.resolve()
  } else {
    deferred.resolve()
  }
  return deferred.promise
}


const isNewEmail = (email) => {
  const deferred = Q.defer()
  models.User.scope("allUsers").findOne({
    where: {
      email,
      invitationStatus: 1,
    }
  }).then((user) => {
    if(user) {
      deferred.reject({ msg: 'Email already registered.' })
    }
    else {
      deferred.resolve()
    }
  },(err) => {
    deferred.resolve()
  })
  return deferred.promise
}

const isNewPhone = (phone) => {
  const deferred = Q.defer()
  models.User.scope("allUsers").findOne({
    where: {
      phone,
      invitationStatus: 1,
    }
  }).then((user) => {
    if(user) {
      if (user.phoneVerified === 0) {
        deferred.resolve()
      } else {
        deferred.reject({ msg: 'Contact Number is already registered.' })
      }
    }
    else {
      deferred.resolve()
    }
  },(err) => {
    console.log(err)
    deferred.resolve()
  })
  return deferred.promise
}

const isValidUserType = (userType) => {
  const deferred = Q.defer()
  if (userTypes[userType]) {
    deferred.resolve(true)
  } else {
    deferred.reject({ msg: 'Invalid user type specified' })
  }
  return deferred.promise
}

const signUp = (req, cb) => {
  const deferred = Q.defer()

  isAuthorizedSignUp(req.currentUser, req.body.email)
  // validate the sign up source
  .then(() => { return isValidSource(req.body.source) })
  // validate the password and confirm password
  .then(() => {
    return isValidPassword(req.body.confirmPassword, req.body.password,
      req.body.source, req.currentUser)
  })
  .then(() => { return isValidUserType(req.body.userType) })
  .then(() => { return isNewEmail(req.body.email) })
  .then(() => { return isNewPhone(req.body.phone) })
  .then(function () {
    cb(req).then((user) => {
      deferred.resolve(user)
    }, (err) => {
      console.log(err)
      deferred.reject(err)
    })
  }).catch(function (err) {
    console.log(err)
    deferred.reject(err)
  })

  return deferred.promise
}

const validateSignup = (req) => {
  req.assert('phone', 'Mobile number must be 10 digits long').isInt().len(10)
  req.assert('source', 'The signup source must be specified').notEmpty()

  return req.validationErrors()
}

const setResponse = (res, msg = '', status = 200) => {
  if (res) {
    res.status(status).json({
      msg: msg
    })
  }
}

const isValid = (field, value, errorMsg = 'Invalid', shouldExist = true) => {
  const deferred = Q.defer()

  if (value) {
    User.findOne({[field]: value}, function(data, err) {
      if(err){
        deferred.reject(err)
      }
      if ((data && shouldExist) || (!data && !shouldExist)) {
        deferred.resolve(true)
      } else {
        deferred.reject({
          msg: `${errorMsg} ${field}: ${value}`
        })
      }
    })
  } else {
    deferred.resolve(true)
  }

  return deferred.promise
}

const generateToken = (length = 8, charset = 'alphanumeric') => {
  return 123456;
  // return codeGenerator.generate({
  //   length: length,
  //   count: 1,
  //   charset,
  // })[0]
}

const signUpNewUser = (req) => {
  const deferred = Q.defer()
  const timestamp = new Date()

  models.User.findOne({
    where: {
      phone: req.body.phone,
    },
  }).then((userObj) => {
    if (!userObj) {
      const user = models.User.build(models.User.getEmptyUser())

      user.phone = req.body.phone
      user.password = md5(req.body.password)
      user.userType = userTypes[req.body.userType]
      user.email = req.body.email || ''
      user.invitationStatus = 1

      return user.save()
    }
    else {
      const userProps = {
        password: md5(req.body.password),
        userType: userTypes[req.body.userType],
        email: req.body.email || '',
        invitationStatus: 1
      }
      if (userObj.inviterId) {
        // send push notification
        pushService.studentRegistered({ actor: userObj.name ? userObj.name : userObj.phone }, userObj.inviterId)
      }
      return userObj.updateAttributes(userProps)
    }
  }).then((user) => {
    const verificationCode = generateToken(TOKEN_LENGTH, numeric)

    // check if the user already has an unexpired OTP
    models.UserOTP.findOne({
      order: [
        ['createdOn', 'DESC']
      ],
      where: {
        mobile: user.phone,
        createdOn: {
          $gt: new Date((new Date()).getTime() - TOKEN_EXPIRATION_TIME),
        },
        verified: 0,
      }
    }).then((otp) => {
      if (otp) {
        otp.createdOn = new Date()
        return otp.save()
      } else {
        const userOTP = models.UserOTP.build({
          mobile: user.phone,
          email: user.email,
          otp: verificationCode,
          verified: 0,
          createdOn: timestamp,
        })

        return userOTP.save()
      }
    }).then(verification => {
      delete user.phone
      deferred.resolve(user)
      smsService.smsOTP(user, verification.otp)
    })
  },(err)=> {
    console.log(err)
    deferred.reject({})
  })

  return deferred.promise
}

/**
 * GET /logout
 * Log out.
 */
exports.logout = (req, res) => {
  req.logout()
  setResponse(res, 'You have been successfully logged out', 200)
  return res
}

exports.postForgot = (req, res, next) => {
  req.assert('phone', 'Please enter a valid mobile.').isInt().len(10)
  const errors = req.validationErrors()

  if (errors) {
    return res.status(400).
    setResponse(res, errors, 400)
    return res
  }

  async.waterfall([
    function (done) {
      models.User.findOne({
        where: {
          phone: req.body.phone,
          phoneVerified: 1,
        },
      }).then((user) => {
        if (!user) {
          return res.status(400).json({
            msg: 'Either the phone number is not registered. Or it is not verified.'
          })
        }

        models.UserOTP.findOne({
          order: [
            ['createdOn', 'DESC']
          ],
          where: {
            mobile: req.body.phone,
            verified: 0,
            createdOn: {
              $gt: new Date((new Date()).getTime() - TOKEN_EXPIRATION_TIME),
            },
          }
        }).then((otp) => {
          if (otp) {
            done(null, user, otp.otp)
          } else {
            const token = generateToken(TOKEN_LENGTH, numeric)
            const userOTP = models.UserOTP.build({
              mobile: user.phone,
              email: user.email,
              otp: token,
              verified: 0,
              createdOn: new Date(),
            })
            userOTP.save().then((verification) => {
              done(null, user, token)
            })
          }
        })
      })
    },
    function (user, token, done) {
      smsService.smsResetPassword(user, token)
      return res.status(200).json({ msg: `OTP sent to mobile ${req.body.phone}` })
    }
  ], (err) => {
    if (err) {
      logger.error(err)
      return res.status(500).json({
        msg: 'Unexpected error occurred while generating reset password token'
      })
    }
  })
}

exports.postUpdatePassword = (req, res, next) => {
  const errors = valdiatePasswordUpdate(req)
  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  async.waterfall([
    function (done) {
      models.User
      .findOne({
        where: {
          phone: req.body.phone,
          password: md5(req.body.currentPassword),
        },
      }).then((user) => {
        if (!user) {
          return res.status(400)
            .json({ msg: 'Invalid credentials.' })
        } else {
          done(null, user)
        }
      })
    }, function (user, done) {
      user.password = md5(req.body.password)
      user.save().then(() => {
        return res.status(200).json({
          msg: 'The password has been successfully updated.',
          token: generateJWTToken(user.id),
        })
      })
    }
  ], (err) => {
    if (err) {
      logger.error(err)
      return res.status(500).json({
        msg: 'Unexpected error occurred while updating password token'
      })
    }
  })
}

exports.postResetPassword = (req, res, next) => {
  const errors = valdiatePasswordUpdate(req)
  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  async.waterfall([
    function (done) {
      models.User
      .findOne({
        where: {
          phone: req.body.phone,
        },
      }).then((user) => {
        if (!user) {
          return res.status(400)
            .json({ msg: 'Unrecognized phone number provided.' })
        } else {
          done(null, user)
        }
      })
    }, function (user, done) {
      models.UserOTP.findOne({
        order: [
          ['createdOn', 'DESC']
        ],
        where: {
          mobile: req.body.phone,
          verified: 0,
          createdOn: {
            $gt: new Date((new Date()).getTime() - TOKEN_EXPIRATION_TIME),
          },
          otp: req.body.token,
        }
      }).then((otp) => {
        if (!otp) {
          return res.status(400)
            .json({ msg: 'Either the OTP is incorrect or it has expired.' })
        } else {
          done(null, user, otp)
        }
      })
    }, function (user, userOTP, done) {
      user.password = md5(req.body.password)
      user.save().then(() => {
        userOTP.verified = 1
        userOTP.save().then(() => {
          return res.status(200).json({
            msg: 'The password has been successfully updated.',
            token: generateJWTToken(user.id),
          })
        })
      })
    },
  ], (err) => {
    if (err) {
      logger.error(err)
      return res.status(500).json({
        msg: 'Unexpected error occurred while updating password token'
      })
    }
  })
}

/**
 * POST /signup
 * Create a new local account.
 */
exports.postSignup = (req, res, next) => {
  const errors = validateSignup(req)

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  signUp(req, signUpNewUser).then((user) => {
    // mailer.sendMail(user.email, 'Welcome',
    //   'Welcome . Mobile verification code -' )
    // + user.verification.code)
    return res.status(200).json({
      msg: 'OTP has been sent to the registered mobile number'
    })
  }, (err) => {
    return res.status(err.status || 400).json(err)
  })
}

exports.verifyToken = (req, res, next) => {
  req.assert('token', 'Invalid token supplied').isInt().len(TOKEN_LENGTH)
  req.assert('phone', 'Mobile number must be 10 digits long').isInt().len(10)

  const errors = req.validationErrors()
  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  models.User.scope('allUsers').findOne({
    where: {
      phone: req.body.phone,
    }
  }).then((user) => {
    if (user) {
      if (user.phoneVerified === 1) {
        return res.status(400).json({
          msg: 'Already verified',
        })
      }
      models.UserOTP.findOne({
        order: [
          ['createdOn', 'DESC']
        ],
        where: {
          otp: req.body.token,
          mobile: req.body.phone,
          verified: 0,
        }
      })
      .then((userOTP) => {
        if (!userOTP) {
          throw 'Invalid OTP'
        }
        userOTP.verified = 1
        return userOTP.save()
      }).then((userOTP) => {
        user.phoneVerified = 1
        return user.save()
      }).then(() => {
        return res.status(204).json({})
      }).catch(err => {
        console.log(err)
        return res.status(400).json({
          msg: err,
        })
      })
    } else {
      return res.status(400).json({
        msg: 'Unrecognized phone number',
      })
    }
  })
}

const generateJWTToken= (payload) => {
  return jwt.encode({
    user: payload
  }, auth.secret)
}

exports.resendOTP = (req, res, next) => {
  req.assert('phone', 'Mobile number must be 10 digits long').isInt().len(10)

  if (req.validationErrors()) {
    setResponse(res, errors, 400)
    return res
  }

  models.User.findOne({
    where: {
      phone: req.body.phone,
    }
  }).then((user) => {
    models.UserOTP.findOne({
      order: [
        ['createdOn', 'DESC']
      ],
      where: {
        mobile: req.body.phone,
        createdOn: {
          $gt: new Date((new Date()).getTime() - TOKEN_EXPIRATION_TIME),
        },
        verified: 0,
      }
    }).then((otp) => {
      if (otp) {
        smsService.smsOTP(user, otp.otp)
        return res.status(200).json({ msg: `OTP sent to mobile ${req.body.phone}` })
      } else {
        const verificationCode = generateToken(TOKEN_LENGTH, numeric)
        const userOTP = models.UserOTP.build({
          mobile: user.phone,
          email: user.email,
          otp: verificationCode,
          verified: 0,
          createdOn: new Date(),
        })
        userOTP.save().then((verification) => {
          smsService.smsOTP(user, verificationCode)
          return res.status(200).json({ msg: `OTP sent to mobile ${req.body.phone}` })
        })
      }
    })
  })
}

/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = (req, res, next) => {
  // either the password will have to supplied or a pair of bearer and source
  let mode = req.body.password ? 'password' : (req.body.bearer ? 'bearer': null)
  switch(mode) {
    case 'password':
      req.phone = req.body.phone
      passport.authenticate('local', (err, user, info) => {
        if (err) {
          logger.warn('Failed to authenticate using local strategy.',
            err)
        }
        
        if (info) {
          return res.status(401).send(info)
        } else {
          if ((req.body.userType === 'student' && user.userType === userTypes.student)
            || (!req.body.userType && (user.userType === userTypes.tutor || user.userType === userTypes.tuition_centre))) {
            user.lastLogged = new Date()
            user.save().then(() => {
              return res.status(200).send({
                token: generateJWTToken(user.id),
                userId: user.id,
              })
            });
          } else {
            return res.status(401).send({ msg: `Invalid phone or password` });
          }
        }
      })(req, res, next)
      break
    case 'bearer':
      req.body.access_token = req.body.bearer
      passport.authenticate(auth.strategy[req.body.source],
        function(err, profile, info) {
          if (err) return res.status(500).send({
            msg: err
          })
          if (info) return res.status(401).send({
            msg: info
          })

          const response = {
            token: generateJWTToken(profile._id)
          }

          // if (!profile.phone || !profile.verification.verified) {
          //   response.verified = false
          // } else {
          //   response.verified = true
          // }
          response.user = profile

          return res.status(200).send(response)
        }
      )(req, res, next)
      break
    default:
      return res.status(400).json({
        msg: 'A password or bearer field is expected for login.'
      })
  }
}

/**
 * POST /login
 * Sign in using email and password.
 */
exports.preLogin = (req, res, next) => {
  if (!req.body.source) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Source is mandatory')
      .build()
    )
  } else if (req.body.source === 'facebook' && (!req.body.email && !req.body.facebookID)) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid login request')
      .setErrors(['For facebook source one of email or facebookID must be specified'])
      .build()
    )
  } else if (!req.body.email) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Email must be specified')
      .build()
    )
  }

  if (req.body.email) {
    req.assert('email', 'Please enter a valid email address').isEmail()
    if (req.validationErrors()) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Encountered validation errors')
        .setErrors(req.validationErrors())
        .build()
      )
    }
  }

  const createOTP = (user) => {
    const verificationCode = generateToken(TOKEN_LENGTH, numeric)
    const userOTP = models.UserOTP.build({
      mobile: user.phone,
      email: user.email,
      otp: verificationCode,
      verified: 0,
      createdOn: new Date(),
    })

    return models.UserOTP.update({
      verified: 1,
    }, {
      where: {
        mobile: user.phone,
      }
    }).then(() => {
      return userOTP.save().then((verification) => {
        if (user && verification) {
          smsService.smsOTP(user, verificationCode)
          return res.status(200).json(new ResponseBuilder()
            .setData({
              user,
              token: generateJWTToken(user.id),
            })
            .setMessage(`User has been saved. OTP has been sent to ${user.phone}.`)
            .setStatus(200)
            .build()
          )
        }
        else {
          return res.status(500).json(new ResponseBuilder()
            .setMessage('User has been saved. But failed to sent OTP.')
            .setStatus(500)
            .build()
          )
        }
      })
    })
  }

  models.User.findOne({
    where: {
      email: req.body.email,
    },
  }).then((user) => {
    let shouldSendOTP
    // the user doesn't exist. sign the user up.
    if (!user) {
      const user = models.User.build(models.User.getEmptyUser())
      const timestamp = new Date()

      if (req.body.phone) {
        user.phone = req.body.phone
      }
      if (req.body.password) {
        user.password = md5(req.body.password)
      }
      if (req.body.email) {
        user.email = req.body.email
      }
      if (req.body.name) {
        user.name = req.body.name
      }
      if (req.body.gender) {
        user.gender = req.body.gender
      }
      if (req.body.email) {
        user.email = req.body.email
      }
      user.userType = userTypes[req.body.userType]

      shouldSendOTP = !user.phoneVerified && user.phone !== undefined

      user.save().then((userObj) => {
        if (shouldSendOTP) {
          createOTP(userObj)
        } else {
          return res.status(200).json(new ResponseBuilder()
            .setData({
              user,
              token: generateJWTToken(user.id),
            })
            .setMessage('User has been successfully logged in.')
            .setStatus(200)
            .build()
          )
        }
      }, (err)=> {
        logger.error(err)
        return res.status(500).json(new ResponseBuilder()
          .setErrors(err.message ? err.message : err)
          .setMessage('Failed to login user. Unexpected error occurred.')
          .setStatus(500)
          .build()
        )
      })
    }
    // the user exists. return whatever you have
    else {
      shouldSendOTP = !user.phoneVerified && user.phone !== undefined
      if (shouldSendOTP) {
        createOTP(user)
      } else {
        return res.status(200).json(new ResponseBuilder()
          .setData({
            user,
            token: generateJWTToken(user.id),
          })
          .setMessage('User has been successfully logged in.')
          .setStatus(200)
          .build()
        )
      }
    }
  })
}

const formatCoursesTaught = (detail) => {
  /* 
   * Maps subject to classes
   * subject => class { ranges }
   */
  const courses = {}
  const coursesTaught = []
  if (detail) {
    R.forEach((info) => {
      const rawInfo = info.get({ plain: true })
      const subject = info && info.subjectObj && info.subjectObj.subject
      const clazz = info && info.classObj

      if (subject && clazz) {
        if (!courses[subject]) {
          courses[subject] = []
        }
        courses[subject].push(clazz)
      }
    }, detail)

    Object.keys(courses).map((subj) => {
      courses[subj] = courses[subj].filter(function(item, pos) {
        return courses[subj].indexOf(item) == pos
      })
      coursesTaught.push({
        subject: subj,
        classes: R.sort((a, b) => a.sortOrder - b.sortOrder, courses[subj]),
      })
    })
  }

  return coursesTaught
}

const getTuitionScopedPromises = (user) => {
  const promises = []
  promises.push(user.getOneOnOneTutionCharge())
  promises.push(user.getGroupTutionCharge())
  promises.push(user.getWeekdayTutionTime())
  promises.push(user.getWeekendTutionTime())
  promises.push(user.getTutorBoard())
  promises.push(user.getTutorTeachingMode())
  promises.push(user.getTutorTeachingLanguage())
  promises.push(user.getTutorSchool())
  promises.push(models.TutorCourseDetail.findAll({
    where: {
      tutorId: user.id,
    },
    include: [{
      model: models.Class,
      as: 'classObj',
    },
    {
      model: models.ClassSubject,
      as: 'subjectObj',
    }],
  }))

  return promises
}

const getTutionDetails = (user, cb) => {
  const promises = getTuitionScopedPromises(user)
  models.sequelize.Promise.all(promises)
  .spread((tutionCharge, groupTutionCharge, weekdayTime, weekendTime, teachingBoards, teachingModes,
      teachingLanguages, tutorSchools, courseDetails) => {
    user.oneOnOneTutionCharge = tutionCharge
    user.groupTutionCharge = groupTutionCharge
    user.weekdayTutionTime = weekdayTime
    user.weekendTutionTime = weekendTime
    user.coursesTaught = formatCoursesTaught(courseDetails)
    user.tutorBoard = teachingBoards
    user.tutorTeachingMode = teachingModes
    user.tutorTeachingLanguage = teachingLanguages
    user.tutorSchool = tutorSchools

    cb(user)
    return
  }).catch(function(err) {
    logger.error(err)
    cb(null, err)
    return
  })
}

const getDetailedUserProfile = (user, cb) => {
  const promises = getTuitionScopedPromises(user)
  promises.push(models.SubCityLocality.findAll({
    where: {
      tutorId: user.id,
    },
  }))
  promises.push(user.getTutorAwards())
  promises.push(user.getUserLikes())
  promises.push(getUserReviewsPromise(user.id))

  if (user.userType === userTypes.tuition_centre) {
    promises.push(models.User.findAll({
      where: {
        tutionCenterParent: user.id,
      },
      include: [
        {
          model: models.TutorEducationDetail,
          as: 'tutorEducationDetail',
          include: [
            {
              model: models.TutorDegree,
              as: 'tutorDegree',
            },
            {
              model: models.TutorDegreeSubject,
              as: 'tutorDegreeSubject',
            },
            {
              model: models.TutorInstitute,
              as: 'tutorInstitute',
            },
            {
              model: models.TutorInstituteAward,
              as: 'tutorInstituteAward',
            },
          ],
        },
        {
          model: models.TutorAwardsRecognition,
          as: 'tutorAwards',
        },
      ]
    }))
  } else {
    promises.push(null)
  }

  models.sequelize.Promise.all(promises)
  .spread((tutionCharge, groupTutionCharge, weekdayTime, weekendTime, teachingBoards,
    teachingModes, teachingLanguages, tutorSchools, courseDetails, locations, awards,
    likes, reviews, faculties) => {
    user.oneOnOneTutionCharge = tutionCharge
    user.groupTutionCharge = groupTutionCharge
    user.weekdayTutionTime = weekdayTime
    user.weekendTutionTime = weekendTime
    user.coursesTaught = formatCoursesTaught(courseDetails)
    user.tutorBoard = teachingBoards
    user.tutorTeachingMode = teachingModes
    user.tutorTeachingLanguage = teachingLanguages
    user.tutorSchool = tutorSchools
    user.tutorLocation = locations
    user.tutorAwards = awards
    user.likeCount = likes && likes.length || 0
    user.reviewCount = reviews && reviews.length || 0
    if (user.userType === userTypes.tuition_centre) {
      user.faculties = faculties || []
    }
    cb(user)
    return
  }).catch(function(err) {
    logger.error(err)
    cb(null, err)
    return
  })
}

const getScopedUserProfile = (userId, scope, cb) => {
  scope = scope || 'basic'
  // switch case using the scope
  const USER_NOT_FOUND_MSG = `Failed to find the user with id: ${userId}`
  switch(scope) {
    case 'basic':
      getBasicProfile(userId, (user) => {
        if (user) {
          cb({
            status: 200,
            data: user,
          })
        } else {
          cb({
            status: 404,
            message: USER_NOT_FOUND_MSG,
          })
        }
      })
      break
    case 'tuition':
      getTuitionProfile(userId, (user) => {
        if (user) {
          getTutionDetails(user, (user, error) => {
            if (user) {
              cb({
                status: 200,
                data: user,
              })
            }
            else if (error) {
              cb({
                status: 500,
                message: 'An unexpected error occurred.',
                errors: error ? [error] : [],
              })
            }
          })
        } else {
          cb({
            status: 404,
            message: USER_NOT_FOUND_MSG,
          })
        }
      })
      break
    case 'qualification':
      getQualificationProfile(userId, (user) => {
        if (user) {
          cb({
            status: 200,
            data: user,
          })
        } else {
          cb({
            status: 404,
            message: USER_NOT_FOUND_MSG,
          })
        }
      })
      break
    case 'detailed':
      getQualificationProfile(userId, (user) => {
        if (user) {
          getDetailedUserProfile(user, (user, error) => {
            if (user) {
              cb({
                status: 200,
                data: user,
              })
            }
            else if (error) {
              logger.error(error)
              cb({
                status: 500,
                message: 'An unexpected error occurred.',
                errors: error ? [error] : [],
              })
            }
          })
        } else {
          cb({
            status: 404,
            message: USER_NOT_FOUND_MSG,
          })
        }
      })
      break
    default:
      cb({
        status: 400,
        message: 'Unrecognized scope value',
      })
  }
}

exports.getUser = (req, res, next) => {
  getScopedUserProfile(req.params.userId, req.query.scope, (resp) => {
    if (resp.data) {
      return res.status(200).json(resp.data)
    } else {
      const responseObj = new ResponseBuilder()
      if (resp.message) {
        responseObj.setMessage(resp.message)
      }
      if (resp.errors) {
        responseObj.setErrors(resp.errors)
      }
      if (resp.status) {
        responseObj.setStatus(resp.status)
      }
      return res.status(resp.status).json(responseObj.build())
    }
  })
}

exports.updateProfileCompleteness = (req, res) => {
  const userId = req.params.id

  userService.calculateProfileCompleteness(userId).then(updatedUser => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('Profile completeness has been updated successfully.')
      .build()
    )
  })
}

const validateFacultyInfo = (req) => {
  const customErrors = []

  if (!req.body.faculties) {
    customErrors.push({
      msg: 'No faculties specified'
    })
  } else if (!R.is(Array, req.body.faculties)) {
    customErrors.push({
      msg: 'Faculties must be specified as an array'
    })
  } else {
    let facultyIndex = 0
    R.forEach((faculty) => {
      if (!faculty.name) {
        customErrors.push({
          msg: `Please enter full name for faculty ${facultyIndex}`
        })
      }
      if (!faculty.gender) {
        customErrors.push({
          msg: `Please enter gender for faculty ${facultyIndex}`
        })
      }
      if (!faculty.tutorEducationDetail) {
        customErrors.push({
          msg: `Please enter tutorEducationDetail for faculty ${facultyIndex}`
        })
      }

      if (faculty.tutorEducationDetail && faculty.tutorEducationDetail.length === 1) {
        if (!faculty.tutorEducationDetail[0].tutorDegree) {
          customErrors.push({
            msg: `Please enter degree for faculty ${facultyIndex}`
          })
        }
        if (!faculty.tutorEducationDetail[0].tutorDegreeSubject) {
          customErrors.push({
            msg: `Please enter subject for faculty ${facultyIndex}`
          })
        }
      } else if (faculty.tutorEducationDetail.length >= 1) {
        customErrors.push({
          msg: 'Only single tutor education detail is supported'
        })
      }
      facultyIndex++
    }, req.body.faculties)
  }

  return customErrors
}

const addFacultyToTuitionCenter = (faculty)  => {
  const deferred = Q.defer()

  const newUser = faculty.id ? {} : models.User.build(models.User.getEmptyUser())
  // based on the id, get persisted user or get a new user
  newUser.id = faculty.id
  newUser.name = faculty.name
  newUser.userType = userTypes.tutor
  newUser.gender = faculty.gender
  newUser.tutionCenterParent = faculty.tutionCenterParent
  newUser.image = faculty.image

  const props = faculty.id ? newUser : newUser.get({
    plain: true,
  })

  helperService.createOrUpdate(models.User, props, true).then((user) => {
    // only 1 education detail is allowed
    const educationDetail = faculty.tutorEducationDetail
    educationDetail.tutor_id = user.id
    educationDetail.tutorId = user.id
    educationDetail.prefered = 1 
    const promises = [updateTutorEducationDetail(user, [educationDetail])]

    models.sequelize.Promise.all(promises)
    .spread((education) => {
      // Not sure if we need this anymore
      const newPromises = [education]

      deferred.resolve(user)
    })
    .catch(function(err) {
      console.log(err)
      logger.error('addFacultyToTuitionCenter', err)
      deferred.reject(err)
    })
  },(err)=> {
    console.log(err)
    deferred.reject(err)
  })

  return deferred.promise
}

const addFacultiesToTuitionCenter = (req, user, cb) => {
  const promises = []
  const faculties = req.body.faculties ?
    (R.is(Array, req.body.faculties) ? req.body.faculties : [req.body.faculties])
    : []
  if (faculties && faculties.length) {
    R.forEach((faculty) => {
      const clonedFaculty = R.clone(faculty)
      clonedFaculty.tutionCenterParent = req.params.id
      clonedFaculty.tutorEducationDetail = clonedFaculty.tutorEducationDetail[0]
      promises.push(addFacultyToTuitionCenter(clonedFaculty))
    }, faculties)
  }

  const toBeDeletedFaculties = helperService.oneToManyCleaner(req.body.faculties, user.faculties)
  if (toBeDeletedFaculties && toBeDeletedFaculties.length) {
    promises.push(models.User.destroy({
      where: {
        id: {
          $in: toBeDeletedFaculties,
        },
      },
    }))
  }

  if (req.body.tutorAwards) {
    promises.push(updateTutorAwardRecognition(user, req.body.tutorAwards))
  }

  // remove unwanted awards
  const toBeCleaned = helperService.oneToManyCleaner(req.body.tutorAwards, user.tutorAwards)
  if (toBeCleaned && toBeCleaned.length) {
    promises.push(models.TutorAwardsRecognition.destroy({
      where: {
        id: {
          $in: toBeCleaned,
        },
      },
    }))
  }

  models.sequelize.Promise.all(promises)
  .spread((...users) => {
    cb(users, null)
  }).catch(function(err) {
    console.log(err)
    cb(null, err)
  })
}

exports.addFaculty = (req, res, next) => {
  // check if the user (tuttion center) exists
  getScopedUserProfile(req.params.id, 'detailed', (resp) => {
    if (resp.data) {
      // validate the faculty information
      const errors = validateFacultyInfo(req)
      if (req.body.faculties && errors && errors.length) {
        // if not, throw 400
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setErrors(errors)
          .build()
        )
      } else {
        // if valid, insert into data
        addFacultiesToTuitionCenter(req, resp.data, (faculties, err) => {
          if (err) {
            return res.status(500).json(new ResponseBuilder()
              .setStatus(500)
              .setMessage('Unkown exception occurred.')
              .setErrors([err.message])
              .build()
            )
          } else {
            getScopedUserProfile(req.params.id, 'detailed', response => {
              response.data.profileCompleteness = 'qualification'
              response.data.save().then(updatedUser => {
                userService.updateSearchScore(updatedUser.id)
                return res.status(200).json(new ResponseBuilder()
                  .setStatus(200)
                  .setMessage('Faculty successfully associated with tuition centre.')
                  .setData(response)
                  .build()
                )
              }).catch(err => {
                // TODO: check this
                console.log(err)
                return res.status(200).json(new ResponseBuilder()
                  .setStatus(200)
                  .setMessage('Faculty successfully associated with tuition centre.')
                  .setData(response)
                  .build()
                )
              })
            })
          }
        }, (errors) => {
          return res.status(500).json(new ResponseBuilder()
            .setStatus(500)
            .setMessage('Unknown exception occurred.')
            .setErrors(errors)
            .build()
          )
        })
      }
    } else {
      // if no, return 404
      const responseObj = new ResponseBuilder()
      if (resp.message) {
        responseObj.setMessage(resp.message)
      }
      if (resp.errors) {
        responseObj.setErrors(resp.errors)
      }
      if (resp.status) {
        responseObj.setStatus(resp.status)
      }
      return res.status(resp.status).json(responseObj.build())
    }
  })
}
