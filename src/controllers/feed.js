const Q = require('q')
const R = require('ramda')
const fs = require('fs')
const path = require('path')
const moment = require('moment')
const models = require('../models')
const helperService = require('../services/helper')
const feedService = require('../services/feed')
const ResponseBuilder = require('../helper/response-builder')
const GMUtils = require('../helper/gmUtil')

const validateSubjects = subjects => {
  const deferred = Q.defer()
  models.ClassSubject.findAll({
    where: {
      id: {
        $in: subjects ? R.pluck('id')(subjects) : [],
      }
    },
  }).then(subjectObjs => {
    const invalidSubjects = R.differenceWith((a, b) => a.id === b.id, subjects || [], subjectObjs || [])
    if (invalidSubjects && invalidSubjects.length) {
      deferred.reject(invalidSubjects)
    } else {
      deferred.resolve()
    }
  })
  return deferred.promise
}

const validateClasses = classes => {
  const deferred = Q.defer()
  models.Class.findAll({
    where: {
      id: {
        $in: classes ? R.pluck('id')(classes) : [],
      }
    },
  }).then(classesObjs => {
    const invalidClasses = R.differenceWith((a, b) => a.id === b.id, classes || [], classesObjs || [])
    if (invalidClasses && invalidClasses.length) {
      deferred.reject(invalidClasses)
    } else {
      deferred.resolve()
    }
  })
  return deferred.promise
}

exports.create = (req, res) => {
  const tutorId = req.params.id
  const description = req.body.description
  const content = req.body.content
  const subjects = req.body.subjects
  const classes = req.body.classes
  const tags = req.body.tags

  validateSubjects(subjects).then(() => {
    return validateClasses(classes)
  }, invalidSubjects => {
    res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage(`Invalid subject ID provided - ${R.pluck('id')(invalidSubjects)}`)
      .build()
    )
    throw null
  }).then(() => {
    return models.User.findOne({
      where: {
        id: tutorId,
      }
    })
  }, invalidClasses => {
    if (invalidClasses) {
      res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(`Invalid class ID provided - ${R.pluck('id')(invalidClasses)}`)
        .build()
      )
    }
    throw null
  }).then(user => {
    // validate that the user exists
    if (!user) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage(`Failed to find a tutor with ID: ${tutorId}`)
        .build()
      )
    }
    return user
  })
  .then(user => {
    // create the tutor content data
    const timeNow = moment()
    models.Feed.create({
      tutorId,
      description: description || '',
      numViews: 0,
      isModerated: 0,
      updatedOn: timeNow,
      createdOn: timeNow,
    }).then(contentUpload => {
      const promises = [contentUpload]
      let error = undefined
      if (content && content.length) {
        for (let i = 0; i < content.length; i++) {
          // validate content items
          if (!content[i] || !content[i].uri || !content[i].mimetype) {
            error = 'Cotent items must have a valid uri and mimetype.'
            break
          }
        }
      }
      if (!error) {
        if (content && content.length) {
          R.forEach(contentItem => {
            // persist all the content items
            promises.push(models.FeedContent.create({
              feedId: contentUpload.id,
              contentUrl: contentItem.uri,
              mimetype: contentItem.mimetype,
            }))
          }, content)
        }

        if(subjects && subjects.length) {
          R.forEach(subject => {
            promises.push(models.FeedSubjects.create({
              feedId: contentUpload.id,
              subjectId: subject.id,
            }))
          }, subjects)
        }

        if(classes && classes.length) {
          R.forEach(classObj => {
            promises.push(models.FeedClasses.create({
              feedId: contentUpload.id,
              classId: classObj.id,
            }))
          }, classes)
        }

        if(tags && tags.length) {
          R.forEach(tag => {
            promises.push(models.FeedTags.create({
              feedId: contentUpload.id,
              tag,
            }))
          }, R.uniq(tags))
        }
      } else {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage(error)
          .build()
        )
        return
      }
      return models.sequelize.Promise.all(promises)
    })
    .then(results => {
      if (results && results.length) {
        const feed = results[0]
        const contentItems = content && content.length ? R.take(content.length, R.remove(0, 1, results)) : []
        feed.content = contentItems
        feed.subjects = subjects
        feed.classes = classes
        feed.tags = R.uniq(tags)

        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setMessage('The feed content was successfully uploaded')
          .setData(feed)
          .build()
        )
      }
    })
  })
  .catch(err => {
    if (err) {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Failed to upload feed content')
        .build()
      )
    }
  })
}

exports.delete = (req, res) => {
  const tutorId = req.params.id
  const feedId = req.params.feedId

  models.Feed.findOne({
    where: {
      tutorId,
      id: feedId,
    },
  }).then(content => {
    if (!content) {
      return res.status(404).json(new ResponseBuilder()
        .setMessage('Invalid tutor ID or feed ID')
      )
    }

    models.FeedClasses.destroy({
      where: {
        feedId,
      },
    }).then(() => {
      return models.FeedSubjects.destroy({
        where: {
          feedId,
        },
      })
    }).then(() => {
      return models.FeedTags.destroy({
        where: {
          feedId,
        },
      })
    }).then(() => {
      return models.Feed.destroy({
        where: {
          id: feedId,
        },
      })  
    }).then((destroyedContent) => {
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setMessage('Content successfully deleted')
      )
    }).catch(err => {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Failed to delete content')
        .build()
      )
    })
  })
}

exports.moderate = (req, res) => {
  const isModerated = req.body.isModerated || false
  const tutorId = req.params.id
  const feedId = req.params.feedId

  models.Feed.findOne({
    where: {
      // tutorId,
      id: feedId,
    }
  })
  .then(feed => {
    if (!feed) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Failed to find the specified feed.')
      )
    } else {
      feed.isModerated = isModerated
      feed.save().then(udpatedFeed => {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(udpatedFeed)
          .setMessage('Feed updated successfully.')
        )  
      })
    }
  })
  .catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Unexpected error occurred while updating the feed.')
    )
  })
}

exports.update = (req, res) => {
  const tutorId = req.params.id
  const feedId = req.params.feedId
  const description = req.body.description
  const content = req.body.content
  const subjects = req.body.subjects
  const classes = req.body.classes
  const tags = req.body.tags

  validateSubjects(subjects).then(() => {
    return validateClasses(classes)
  }, invalidSubjects => {
    res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage(`Invalid subject ID provided - ${R.pluck('id')(invalidSubjects)}`)
      .build()
    )
    throw null
  }).then(() => {
    return models.Feed.scope('base').findOne({
      where: {
        tutorId,
        id: feedId,
      }
    })
  }, invalidClasses => {
    if (invalidClasses) {
      res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(`Invalid class ID provided - ${R.pluck('id')(invalidClasses)}`)
        .build()
      )
    }
    throw null
  }).then(feed => {
    if (!feed) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Failed to find the specified feed.')
      )
    }

    const promises = []
    // deal with content
    if (content) {
      content.map((contentItem) => {
        contentItem.feed_id = feed.id
        contentItem.feedId = feed.id
        contentItem.contentUrl = contentItem.uri
        contentItem.mimetype = contentItem.mimetype
        promises.push(helperService.createOrUpdate(models.FeedContent, contentItem, true))
      })

      const toBeDeletedContent = helperService.oneToManyCleaner(req.body.content, feed.content)
      if (toBeDeletedContent && toBeDeletedContent.length) {
        promises.push(models.FeedContent.destroy({
          where: {
            id: {
              $in: toBeDeletedContent,
            },
          },
        }))
      }
    }

    promises.push(models.FeedSubjects.destroy({
      where: {
        feedId: feed.id,
      }
    }))

    promises.push(models.FeedClasses.destroy({
      where: {
        feedId: feed.id,
      }
    }))

    promises.push(models.FeedTags.destroy({
      where: {
        feedId: feed.id,
      }
    }))

    if(subjects) {
      R.forEach(subject => {
        promises.push(models.FeedSubjects.create({
          feedId: feed.id,
          subjectId: subject.id,
        }))
      }, subjects)
    }

    if(classes) {
      R.forEach(classObj => {
        promises.push(models.FeedClasses.create({
          feedId: feed.id,
          classId: classObj.id,
        }))
      }, classes)
    }

    if(tags) {
      R.forEach(tag => {
        promises.push(models.FeedTags.create({
          feedId: feed.id,
          tag,
        }))
      }, R.uniq(tags))
    }

    models.sequelize.Promise.all(promises)
    .spread(function() {
      const contentItemSet = []
      if (arguments && arguments.length) {
        for (let i = 0; i < arguments.length; i++) {
          contentItemSet.push(arguments[i])
        }
      }
      
      return feed.updateAttributes({
        description: description || feed.description
      })
      .then(() => {
        models.Feed.scope('detailed').findOne({
          where: {
            id: feedId,
            tutorId,
          },
        })
        .then(feed => {
          if (feed) {
            feed.likesCount = feed.likes ? feed.likes.length : 0
            feed.commentsCount = feed.comments ? feed.comments.length : 0
            return res.status(200).json(
              new ResponseBuilder().setStatus(200).setData(feedService.transformSingleFeed(feed)).build()
            )
          } else {
            return res.status(404).json(
              new ResponseBuilder().setStatus(404).setMessage(`Failed to find feed ${feedId} for user ${tutorId}.`).build()
            )
          }
        })
      }, (error) => {
        console.log('Failed to udpate feed object.', error)
        return res.status(500).json(
          new ResponseBuilder().setStatus(500).setMessage('Failed to update feed').build()
        )
      })
    }).catch(err => {
      console.log(err)
      return res.status(500).json(
        new ResponseBuilder().setStatus(500).setMessage('Failed to update feed').build()
      )
    })
  })
  .catch(err => {
    if (err) {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Unexpected error occurred while updating the feed.')
      )
    }
  })
}

exports.postFeedViews = (req, res, next) => {
  const tutorId = req.params.id
  const feedIds = req.body.feedIds

  console.log(tutorId, feedIds)

  models.Feed.findAll({
    where: {
      id: {
        $in: feedIds,
      },
    },
  })
  .then(feeds => {
    const feedViews = []
    const promises = []
    R.forEach(feed => {
      feed.numViews++
      feedViews.push({
        id: feed.id,
        numViews: feed.numViews,
      })
      promises.push(feed.save())
    }, feeds)
    models.sequelize.Promise.all(promises).spread(function() {
      return res.status(200).json(
        new ResponseBuilder()
          .setStatus(200)
          .setData(feedViews)
          .setMessage('Feed views have been updated.')
          .build()
      )
    })
  }).catch(err => {
    console.log(err)
    return res.status(500).json(
      new ResponseBuilder()
        .setStatus(500)
        .setMessage('Unexpected error occurred.')
        .build()
    )
  })
}
