const spawn   = require("child_process").spawn
const fs = require('fs')
const path = require('path')
const Q = require('q')
const R = require('ramda')
const pandoc = require('node-pandoc')
const helperService = require('../services/helper')
const brandingService = require('../services/branding')
const GMUtils = require('../helper/gmUtil')
const excelParser = require('excel-parser')
const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const codeGenerator = require('voucher-code-generator')
const ResponseBuilder = require('../helper/response-builder')

const { SERVER_URI } = require('../../config/application')
const IMAGE_REGEX = /<img[\s\t]*src="(.*?)"[\s\t]*other ="(.*?)"/g
const ASSIGNMENT_FILES_LOCATION = path.join(__dirname, '..', '..', 'uploads', 'assignments')
const TEMPLATES = path.join(__dirname, '../../templates')
const ASSIGNMENT_TEMPLATE = path.join(__dirname, '../../templates/assignment.html')
const ASSIGNMENT_FULL_SET_TEMPLATE = path.join(__dirname, '../../templates/assignment_full_set.html')
const IMAGES_LOCATION = path.join(__dirname, '../../images')

const { assignmentImagesLocation } = require('../../config/application')

const GET_SUBJECT_INFO_QUERY = 
`select s.id, s.name as degreeSubject, COALESCE(ch.chapter_count, 0) as chapterCount
from (
  select c.subject_id, count(c.chapter_id) as chapter_count
  from chapters c group by c.subject_id
) as ch
right join subjects s
on s.id = ch.subject_id
where s.class_id = :class_id
order by chapterCount desc`

const GET_CHAPTERS_INFO_QUERY = 
`select c.chapter_id as chapterId, c.chapter_name as chapterName, c.sort_order as sortOrder, COALESCE(a.assignment_count, 0) as assignmentCount
from (
  select a.chapter, count(a.id) as assignment_count
  from assignments a group by a.chapter
) as a
right join chapters c
on c.chapter_id = a.chapter
where c.subject_id = :subject_id
order by assignmentCount desc`

exports.subjects = (req, res, next) => {
  const classId = req.query.class
  models.sequelize.query(
    GET_SUBJECT_INFO_QUERY,
    {
      replacements: {
        class_id: classId,
      },
      model: models.TutorDegreeSubject,
    }
  ).then((results) => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(results)
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch subjects informations.')
      .build()
    )
  })
}

exports.chapters = (req, res, next) => {
  const subjectId = req.query.subject
  models.sequelize.query(
    GET_CHAPTERS_INFO_QUERY,
    {
      replacements: {
        subject_id: subjectId,
      },
      model: models.Chapters,
    }
  ).then((results) => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(results)
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch chapters informations.')
      .build()
    )
  })
}

exports.getAssignment = (req, res, next) => {
  const assignmentId = req.params.id
  const view = req.query.view
  const tutorId = req.query.tutor_id

  models.Assignments.scope('detailed').findOne({
    where: {
      id: assignmentId,
    },
  })
  .then((assignment) => {
    if (!assignment) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setData('Invalid assignment ID.')
        .build()
      )
    }

    if (view === 'true' && tutorId) {
      models.AssignmentViews.destroy({
        where: {
          assignmentId,
          tutorId,
        },
      }).then(() => {
        models.AssignmentViews.create({
          assignmentId,
          tutorId,
        }).then(view => console.log('View successfully generated.'),
          err => console.log(err)
        )
      })
    }
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(assignment)
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch assignment.')
      .build()
    )
  })
}

exports.filterAssignments = (req, res, next) => {
  const classId = req.query.class
  const subjectId = req.query.subject
  const chapterId = req.query.chapter
  const label = req.query.label
  const tutorId = req.query.tutorId

  const whereClause = {}
  if (classId) {
    whereClause.class = classId
  }
  if (subjectId) {
    whereClause.subject = subjectId
  }
  if (chapterId) {
    whereClause.chapter = chapterId
  }
  if (label) {
    whereClause.label = label
  }
  if (tutorId) {
    whereClause.tutorId = tutorId
  }

  models.Assignments.scope('withInfo').findAll({
    where: whereClause,
  })
  .then((assignments) => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(assignments || [])
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch assignments.')
      .build()
    )
  })
}

exports.filterTutorAssignments = (req, res, next) => {
  const type = req.query.type
  const tutorId = req.params.id

  switch(type) {
    case 'recent':
    console.log(type)
      models.AssignmentViews.findAll({
        where: {
          tutorId: tutorId
        },
        limit: 10,
        order: [
          ['createdOn', 'DESC']
        ],
      }).then(views => {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(views)
          .build()
        )
      }, (error) => {
        console.log(error)
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to fetch recent assignment views.')
          .build()
        )
      })
      break;
    default:
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Unsupported type value.')
        .build()
      )
  }
}

const isMigrated = (file, loc, umbrellaSetName) => {
  const deferred = Q.defer()
  models.AssignmentMigration.findOne({
    where: {
      name: file,
      status: 1,
    }
  }).then(migration => {
    if (migration) {
      console.log(`### ${file} has already been migrated.`)
    } else {
      console.log(`### ${file} should start to migrate.`)
    }
    deferred.resolve({
      loc,
      file,
      umbrellaSetName,
      isMigrated: !R.isNil(migration)
    })
  })

  return deferred.promise
}

const sanitizeClass = course => {
  const index = course.indexOf('Class ')
  if (index > -1) {
    return course.substring(index + 6)
  }
  return course
}

const findOrCreate = (model, props) => {
  const deferred = Q.defer()
  model.findOne({
    where: props,
  }).then(obj => {
    if (obj) {
      deferred.resolve(obj)
    } else {
      model.create(props)
      .then(createdObj => {
        deferred.resolve(createdObj)
      })
    }
  })
  return deferred.promise
}

const convertToAbsoluteImages =  (data, name, umbrellaSetName) => {
  const transformedName = umbrellaSetName
  let result = data.replace(IMAGE_REGEX, '<img src="' + SERVER_URI + '/$1" other="' + SERVER_URI + '/$2"')
  result = result.replace(new RegExp(umbrellaSetName, 'ig'), transformedName)  
  return result.replace(/\\/g, "\/")
}

/*
  Use the excel data to create an assignment. Make sure that this is a single transaction and updating is migrated table
  is a part of the same transaction also.
*/
const persistAssignment = (records, file, umbrellaSetName) => {
  const deferred = Q.defer()
  const header = records[0]
  // Find the index of each significant data column
  const courseIndex = R.findIndex(x => x === 'Course')(header)
  const subjectIndex = R.findIndex(x => x === 'Subject')(header)
  const chapterIndex = R.findIndex(x => x === 'Chapter')(header)
  const difficultyIndex = R.findIndex(x => x === 'Difficulty_level')(header)
  const questionIndex = R.findIndex(x => x === 'Question_Statement')(header)
  const answerIndex = R.findIndex(x => x === 'Explanation_Text')(header)
  const hasImageIndex = R.findIndex(x => x === 'Is the Q or Solution textual+ image')(header)

  console.log('### Got all the data indices.')

  // Get all the data that is required
  let course
  let subject
  let chapter
  let difficulty
  if (records.length > 1) {
    course = records[1][courseIndex]
    subject = records[1][subjectIndex]
    chapter = records[1][chapterIndex]
    difficulty = records[1][difficultyIndex]
  }

  // Remove the header
  let data = R.remove(0, 1, records)
  data = R.map(record => {
    return {
      question: record[questionIndex],
      answer: record[answerIndex],
      hasImage: record[hasImageIndex],
    }
  }, data)

  console.log('### Data is mapped and ready to be persisted.')

  // return models.sequelize.transaction(function (t) {
  // TODO: Bind this in one big transaction
  // Create a class
  findOrCreate(models.Class, {
    class: sanitizeClass(course)
  }).then(courseObj => {
    course = courseObj
    // Create subject
    return findOrCreate(models.Subjects, {
      classId: course.id,
      name: subject,
    })
  }).then(subjectObj => {
    subject = subjectObj
    // Create chapters
    return findOrCreate(models.Chapters, {
      subjectId: subject.id,
      chapterName: chapter,
    })
  }).then(chapterObj => {
    chapter = chapterObj
    return models.Assignments.create({
      name: file.split('.')[0],
      label: difficulty,
      class: course.id,
      subject: subject.id,
      chapter: chapter.chapterId,
      tutorId: 1,
    })
  })
  .then(assignment => {
    const promises = []
    R.forEach(rec => {
      promises.push(models.Question.create({
        assignmentId: assignment.id,
        question: convertToAbsoluteImages(rec.question, assignment.name, umbrellaSetName),
      }))
    }, data)

    models.sequelize.Promise.all(promises)
    .spread(function() {
      const promises = []
      for (let i = 0; i < arguments.length; i++) {
        promises.push(
          models.Answer.create({
            questionId: arguments[i].id,
            answer: convertToAbsoluteImages(data[i].answer, assignment.name, umbrellaSetName),
          })
        )
      }

      models.sequelize.Promise.all(promises)
      .spread(answers => {
        deferred.resolve(answers)
      })
    }, err => {
      console.log(err)
    })
    .catch(err => {
      console.log(err)
    })
  }, err => {
    console.log(err)
  })
  // })
  // .then(answers => {
  //   console.log('####### transactions have been committed')
  // })
  .catch(function (error) {
    console.log(`Failed to create assignment ${file}`, error)
    deferred.reject(error)
  })
  return deferred.promise
}

/*
  This method must ensure that the assignment if migrated, is tagged as migrated.
*/
const migrate = (filePath, fileName, umbrellaSetName) => {
  const deferred = Q.defer()
  // Parse the excel file where all the questions and answers reside.
  console.log(`Parsing excel - ${filePath}/${fileName}`)
  excelParser.parse({
    inFile: `${filePath}/${fileName}`,
    worksheet: 1,
  }, (err, records) => {
    if(err) {
      console.log(err)
      deferred.reject(err)
      return
    }
    persistAssignment(records, fileName, umbrellaSetName)
    .then(() => {
      console.log('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
      const dateNow = new Date()
      models.AssignmentMigration.create({
        name: fileName,
        startOn: dateNow,
        createdOn: dateNow,
        status: 1,
      }).then(() => {
        console.log('#######################################')
        deferred.resolve()
      }, err => {
        console.log('#######################################')
        console.log(err)
        deferred.reject()
      }).catch(err => {
        console.log('#######################################')
        console.log(err)
        deferred.reject()
      })
    })
  })
  return deferred.promise
}

// Assignment migration
const createFolderIfNotExists = location => {
  try {
    if (!fs.existsSync(location)){
      fs.mkdirSync(location)
    } else {
      console.log('### Found the target directory.')
    }
  } catch(err) {
    console.log('### Failed to find or create the migration source folder.')
    throw err
  }
}

exports.assignmentMigration = (req, res, next) => {
  // Create the upload file if not exists
  createFolderIfNotExists(ASSIGNMENT_FILES_LOCATION)
  
  try {
    let promises = []
    let pendingMigrations = []
    let noOfFiles = 0
    let filesSkipped = 0
    let filesMigrated = 0
    // Read the files and folders
    const files = fs.readdirSync(ASSIGNMENT_FILES_LOCATION)
    // If files are found, loop over them
    if (files && files.length) {
      noOfFiles = files.length
      R.forEach(file => {
        const fileLocation = path.join(ASSIGNMENT_FILES_LOCATION, file)
        const isDirectory = !fs.statSync(fileLocation).isFile()

        // Process the folders only.
        if (isDirectory) {
          const subFiles = fs.readdirSync(fileLocation)
          R.forEach(subFile => {
            const subFileLocation = path.join(fileLocation, subFile)
            if (fs.statSync(subFileLocation).isFile()) {
              // Migrate only if it hasn't been already migrated
              promises.push(isMigrated(subFile, fileLocation, file))
            }
          }, subFiles)
        }
        // check if the file has already been migrated, if yes, skip. if no, add to queue
        // console.log(file)
      }, files)
    }

    models.sequelize.Promise.all(promises).spread(function() {
      
      if (arguments && arguments.length) {
        for (let i = 0; i < arguments.length; i++) {
          if (arguments[i].isMigrated) {
            console.log(`${arguments[i].file} is already migrated`)
          } else {
            pendingMigrations.push(arguments[i])
          }
        }
      }

      const execute = () => {
        console.log('Execute is called')
        if (pendingMigrations.length) {
          const pr = pendingMigrations.pop()
          migrate(pr.loc, pr.file, pr.umbrellaSetName).then(() => {
            execute()
          })
        } else {
          console.log('### Migration is complete.')
        }
      }

      execute()
    })

    
    console.log(`### Migrating ${noOfFiles} file(s).`)
  } catch(err) {
    console.log(err)
    console.log('### Failed to catalog the files available for migration.')
  }

  return res.status(200).send('Please check logs for migration status.')
}

const shortenImageURIs = (text, assignmentName) => {
  const imageURI = `${SERVER_URI}/Images/${assignmentName}/`
  return text/*.replace('_', '\\_')
    .replace('|', '\\|')*/
    .replace(imageURI, '')
    .replace(imageURI, '')
    .replace(encodeURIComponent(imageURI), '')
    .replace('.PNG', '')
    // .replace('&deg;', '\\degree')
}

exports.exportAssignment = (req, res, next) => {
  const assignmentId = req.params.id
  models.Assignments.scope('detailed').findOne({
    where: {
      id: assignmentId,
    },
  })
  .then((assignment) => {
    if (!assignment) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setData('Invalid assignment ID.')
        .build()
      )
    }

    let questionSet = ''
    let answerSet = ''
    let template = fs.readFileSync(ASSIGNMENT_TEMPLATE).toString()
    template = template.replace('{assignmentName}', assignment.name)
    template = template.replace('{className}', assignment.classInfo.class)
    template = template.replace('{subjectName}', assignment.subjectInfo.name)
    template = template.replace('{chapterName}', assignment.chapterInfo.chapterName)

    let answerStr
    let questionStr
    R.addIndex(R.forEach)((question, idx) => {
        // console.log(question.id)
        if (question.answer && question.answer.answer) {
          answerStr = question.answer.answer
        } else {
          answerStr = ''
        }
        questionStr = question.question || ''

        questionStr = shortenImageURIs(questionStr, assignment.name)
        answerStr = shortenImageURIs(answerStr, assignment.name)

        questionSet += `${idx}. ${questionStr}\n<br />\n`
        answerSet += `<div><p>${idx}. ${answerStr}</p></div>`
    }, assignment.questionSet)

    template = template.replace('{questionSet}', questionSet)
    template = template.replace('{answerSet}', answerSet)

    // return res.status(200).send(template)
    // /home/gaurav/practice/faculty/images/Template/
    pandoc(template, '--from html --to latex', function(err, result) {
      if (err) {
        console.log(err)
      } else {
        let array = result.toString().split('\n')
        var start = [
          "\\documentclass{article}",
          "\\usepackage{graphicx}",
          "\\graphicspath{ {/home/gaurav/practice/faculty/images/Template/} }",
          "\\begin{document}",
          "\\"]
        var end = ["\\", "\\end{document}"]

        var transformedResult = "\\documentclass{article}\n\\usepackage{graphicx}\n\\graphicspath{ {/home/gaurav/practice/faculty/images/Template/} }\n\\begin{document}\n"
        + result
        + "\n\\end{document}\n"


        fs.writeFile('/home/gaurav/practice/faculty/latex', transformedResult, function(err) {
          if(err) {
              return console.log(err);
          }

          console.log("The latex file was saved!");
          try{
            // require("latex")((start.concat(array)).concat(end)).pipe(res)
            var child = spawn('pdflatex', ['-interaction=nonstopmode', '/home/gaurav/practice/faculty/latex']);

            child.stdout.on('data', function (data) {
              // console.log(data)
            });

            child.on('data', function (data) {
              console.log('stderr: ' + data)
              return res.status(500)
            });

            child.on('close', function (code) {
              return res.status(500)
                console.log('child process exited with code ' + code);
            })
            child.on('exit', function (code, signal) {
              console.log(code, signal)

              var output_file = '/home/gaurav/practice/faculty/latex.pdf'
              fs.exists(output_file, function(exists) {
                if(exists) {
                  var stream = fs.createReadStream(output_file)
                  stream.pipe(res)
                } else {
                  return res.status(500).send('Oh no')
                }
              })

              return res.status(500)
                console.log('child process exited with code ' + code);
            })
          }catch(err) {
            console.log(err)
          }
        })

      }
    })

    // return res.status(200).send(template)
  })
}

const generateAssignmentPDF = (template, name, id, type) => {
  const transformedName = name
  const graphicsPath = `${assignmentImagesLocation}${transformedName}/`
  const latexFilePath = `${ASSIGNMENT_FILES_LOCATION}/${transformedName}_latex_${type}`
  // console.log(graphicsPath)
  // use pandoc to convert html to latex
  return pandoc(template, '--latexmathml --from html --to latex', function(err, result) {
    // return in case of error
    if (err) {
      console.log(err)
      return 
    }

    // complete the latex document for conversion to PDF
    let array = result.toString().split('\n')
    var start = [
      "\\documentclass{article}",
      "\\usepackage{gensymb}",
      "\\usepackage{graphicx}",
      "\\graphicspath{ {" + graphicsPath + "} }",
      "\\begin{document}",
      "\\"]
    var end = ["\\", "\\end{document}"]

    var transformedResult = "\\documentclass{article}\n\\usepackage{graphicx}\n\\graphicspath{ {" + graphicsPath + "} }\n\\begin{document}\n"
    + result
    + "\n\\end{document}\n"

    fs.writeFile(latexFilePath, transformedResult, function(err) {
      if(err) {
          return console.log(err);
      }

      console.log("The latex file was saved!");
      try{
        var child = spawn('pdflatex', ['-interaction=nonstopmode', latexFilePath]);

        child.stdout.on('data', function (data) {
          // console.log(data)
        });

        child.on('data', function (data) {
          console.log('stderr: ' + data)
        });

        child.on('close', function (code) {
          console.log('generate pdf close: ' + latexFilePath)
        })
        child.on('exit', function (code, signal) {
          console.log('PDF generation process ends.', code, signal)
        })
      } catch(err) {
        console.log(err)
      }
    })
  })
}

const modifyTemplates = (template, assignmentName) => {
  let assignmentNameUnderScored = assignmentName.replace(/\s/g, '_')
  let finalTemplate = template.replace(/\degree/g, '&deg;')
  // finalTemplate = finalTemplate.replace(new RegExp('<br />\n(\w\d)', 'g'), '<br /><br />\n$1')
  finalTemplate = template.replace(/src=".*?"\s*other="(.*?)"/g, 'src="$1" other="$1"')
  return finalTemplate.replace(new RegExp(`/${assignmentName}/`, 'g'), `/${assignmentNameUnderScored}/`)
}

const generateAssignmentDocuments = (id) => {
  const assignmentId = id
  // fetch assignment
  return models.Assignments.scope('detailed').findOne({
    where: {
      id: assignmentId,
    },
  })
  .then((assignment) => {
    // if not found return
    if (!assignment) {
      console.log('Invalid assignment.')
      return
    }

    // create content
    let questionSet = '<h2>Question Set</h2>'
    let answerSet = '<h2>Answer Set</h2>'
    let template = fs.readFileSync(ASSIGNMENT_TEMPLATE).toString()
    let templateFullSet = fs.readFileSync(ASSIGNMENT_FULL_SET_TEMPLATE).toString()
    template = template.replace('{assignmentName}', assignment.name)
    template = template.replace('{className}', assignment.classInfo.class)
    template = template.replace('{subjectName}', assignment.subjectInfo.name)
    template = template.replace('{chapterName}', assignment.chapterInfo.chapterName)

    templateFullSet = templateFullSet.replace('{assignmentName}', assignment.name)
    templateFullSet = templateFullSet.replace('{className}', assignment.classInfo.class)
    templateFullSet = templateFullSet.replace('{subjectName}', assignment.subjectInfo.name)
    templateFullSet = templateFullSet.replace('{chapterName}', assignment.chapterInfo.chapterName)

    let answerStr
    let questionStr
    let fullSetStr = ''
    R.addIndex(R.forEach)((question, idx) => {
      if (question.answer && question.answer.answer) {
        answerStr = question.answer.answer
      } else {
        answerStr = ''
      }
      questionStr = question.question || ''

      questionStr = shortenImageURIs(questionStr, assignment.name)
      answerStr = shortenImageURIs(answerStr, assignment.name)

      fullSetStr += `<div class="clear"><div class="numbering">Q${idx + 1}.</div> <div class="content">${questionStr}</div></div>\n<br />\n<div class="clear"><div class="numbering">A${idx + 1}.</div> <div class="content">${answerStr}</div></div>\n<br /><br />\n`
      questionSet += `<div class="clear"><div class="numbering">Q${idx + 1}.</div> <div class="content">${questionStr}</div></div>\n<br />\n<br />\n`
      answerSet += `<div class="clear"><div class="numbering">A${idx + 1}.</div> <div class="content">${answerStr}</div></div>\n<br />\n<br />\n`
    }, assignment.questionSet)

    const questionsOnlyTemplate = template.replace('{questionSet}', questionSet).replace('{answerSet}', '')
    const answersOnlyTemplate = template.replace('{answerSet}', answerSet).replace('{questionSet}', '')
    const fullSetTemplate = templateFullSet.replace('{questionSet}', fullSetStr)

    const folder = TEMPLATES + '/' + assignment.name
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder)
    }
    fs.writeFile(`${folder}/${assignment.name}_questions.html`, modifyTemplates(questionsOnlyTemplate, assignment.name), function(err) {
      if(err) {
        console.log(err);
      }
    })
    fs.writeFile(`${folder}/${assignment.name}_answers.html`, modifyTemplates(answersOnlyTemplate, assignment.name), function(err) {
      if(err) {
        console.log(err);
      }
    })
    fs.writeFile(`${folder}/${assignment.name}_fullset.html`, modifyTemplates(fullSetTemplate, assignment.name), function(err) {
      if(err) {
        console.log(err);
      }
    })

    // return models.sequelize.Promise.all([
    //   generateAssignmentPDF(questionsOnlyTemplate, assignment.name, assignment.id, 'questions'),
    //   generateAssignmentPDF(answersOnlyTemplate, assignment.name, assignment.id, 'answers'),
    //   generateAssignmentPDF(fullSetTemplate, assignment.name, assignment.id, 'fullset'),
    // ]).spread(() => true)
  })
}

exports.generateAllDocuments = (req, res, next) => {
  return models.Assignments.findAll({
    attributes: ['id'],
  }).then(assignments => {
    const assignmentIDs = []
    R.forEach(assignment => {
      assignmentIDs.push(assignment.id)
    },assignments)
    const execute = function() {
      if (assignmentIDs.length) {
        const assignmentId = assignmentIDs.pop()
        generateAssignmentDocuments(assignmentId).then(() => {
          execute()
        })
      }
    }

    execute()
    return res.status(200).send('Assignment templates will be generated. Please have a look at the logs.')
  })
  .catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
}

exports.generateDocuments = (req, res, next) => {
  const assignmentId = req.params.id
  generateAssignmentDocuments(assignmentId).then(() => {
    return res.status(200).send('Assignment documents have been generated.')
  }).catch(err => {
    console.log(err)
    return res.status(500).send(err)
  })
}

exports.generateDocumentTags = (req, res) => {
  const promises = []

  const sanitizeTag = tag => tag.replace('#', '')

  R.map(labelSet => {
    console.log(labelSet.assignment)
    promises.push(models.Assignments.find({
      where: {
        name: labelSet.assignment,
      },
    }))
  }, req.body)

  models.sequelize.Promise.all(promises).spread(function() {
    const updatePromises = [null]
    for(var i = 0; i < arguments.length; i++) {
      const assignment = arguments[i]
      const assignmentLabels = req.body[i]
      if (assignment) {
        const tags = []
        if(assignmentLabels.tag1) {
          tags.push(sanitizeTag(assignmentLabels.tag1))
        }
        if(assignmentLabels.tag2) {
          tags.push(sanitizeTag(assignmentLabels.tag2))
        }
        if(assignmentLabels.tag3) {
          tags.push(sanitizeTag(assignmentLabels.tag3))
        }
        if (tags.length) {
          assignment.label = R.join(',', tags)
          updatePromises.push(assignment.save())
        }
      }
    }

    models.sequelize.Promise.all(updatePromises).then(() => {
      return res.status(200).send('Update is successful.')
    })
  }).catch(err => {
    console.log(err)
    return res.status(200).send('Failed to update the assignments.')
  })
}

const egestAssignment = (assignmentId, userId, type, res) => {
  models.Assignments.findOne({
    where: {
      id: assignmentId,
    },
  }).then(assignment => {
    if (!assignment) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Invalid assignment ID provided.')
        .build()
      )
    }

    const assignmentName = assignment.name
    const filePath = path.join(__dirname, `../../pdfs/${assignmentName}_${type}.pdf`)
    fs.exists(filePath, exists => {
      if (exists) {
        const stats = fs.statSync(filePath)
        res.setHeader('Content-disposition', `attachment; filename=${assignmentName}_${type}.pdf`)
        res.setHeader('Content-Length', stats['size'])
        res.setHeader('Content-type', 'application/pdf')

        if (userId) {
          brandingService.getUserBranding(userId).then(branding => {
            if (!branding) {
              fs.createReadStream(filePath, { bufferSize: 2 * 1024 }).pipe(res)
            } else {
              GMUtils.stampPDF(filePath, branding, assignmentName, (err, newFilePath) => {
                if (err) {
                  console.log(err)
                  return res.status(500).json(new ResponseBuilder()
                    .setStatus(500)
                    .setMessage('Failed to download the file. Please try again later.')
                    .build()
                  )
                }
                res.download(newFilePath)
              })
            }
          })
        } else {
          fs.createReadStream(filePath, { bufferSize: 2 * 1024 }).pipe(res)
        }
      } else {
        return res.status(404).json(new ResponseBuilder()
          .setStatus(404)
          .setMessage('Failed to find the file. Please try again later.')
          .build()
        )
      }
    })
  })
}

exports.egestAssignment = egestAssignment

exports.egest = function (req, res) {
  const assignmentId = req.params.id
  const userId = req.query.user_id
  const type = req.query.type || 'fullset'

  egestAssignment(assignmentId, userId, type, res)
}

exports.documentLink = function(req, res) {
  const assignmentId = req.params.id
  const type = req.query.type || 'fullset'

  models.Assignments.findOne({
    where: {
      id: assignmentId,
    },
  }).then(assignment => {
    if (!assignment) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Invalid assignment ID provided.')
        .build()
      )
    }
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData({
        uri: `${SERVER_URI}/api/assignments/${assignmentId}/documents?type=${type}`
      })
      .build()
    )
  })
}
