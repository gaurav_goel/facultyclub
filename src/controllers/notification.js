const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const notificationService = require('../services/notification')

exports.deleteNotification = (req, res, next) => {
  const id = req.params.id
  const tutorId = req.params.tutorId
  notificationService.deleteNotification(id, tutorId)
  .then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Notification was successfully deleted.')
      .setStatus(200)
      .build()
    )
  }, err => {
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to delete user notification.')
      .setStatus(500)
      .build()
    )
  })
}

exports.deleteAllNotification = (req, res, next) => {
  const tutorId = req.params.tutorId
  notificationService.deleteAllNotification(tutorId)
  .then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Notifications were successfully cleared.')
      .setStatus(200)
      .build()
    )
  }, err => {
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to delete user notifications.')
      .setStatus(500)
      .build()
    )
  })
}

exports.markSeen = (req, res, next) => {
  const id = req.params.id
  const tutorId = req.params.tutorId

  notificationService.markSeen(id, tutorId)
  .then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Notification was successfully marked seen.')
      .setStatus(200)
      .build()
    )
  }, err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to mark notifications seen.')
      .setStatus(500)
      .build()
    )
  })
}

exports.markAllSeen = (req, res, next) => {
  const tutorId = req.params.tutorId

  notificationService.markAllSeen(tutorId)
  .then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Notification was successfully marked seen.')
      .setStatus(200)
      .build()
    )
  }, err => {
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to mark notifications seen.')
      .setStatus(500)
      .build()
    )
  })
}

exports.fetchNotifications = (req, res, next) => {
  const tutorId = req.params.tutorId
  const offset = req.query && req.query.offset
  const count = req.query && req.query.count

  models.sequelize.Promise.all([
    notificationService.fetchNotifications(tutorId, offset, count),
    models.Notifications.count({
      where: {
        seen: 0,
      },
    })
  ])
  .spread((notifications, totalUnseenNotifications) => {
    return res.status(200).json(new ResponseBuilder()
      .setData({
        notifications,
        totalUnseenNotifications,
      })
      .setStatus(200)
      .build()
    )
  }, err => {
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to fetch notifications.')
      .setStatus(500)
      .build()
    )
  })
}
