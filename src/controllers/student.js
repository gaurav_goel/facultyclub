const studentService = require('../services').StudentService
const smsService = require('../services').SMSService
const helperService = require('../services').HelperService
const groupService = require('../services').GroupService
const userService = require('../services/user')
const pushService = require('../services/push')
const Q = require('q')
const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const R = require('ramda')


const TOKEN_LENGTH = 6
const numeric = '0123456789'

// validation
const valdiateInviteRequest = (req) => {
  req.assert('pupilInfo.billingCycle', 'Billing cycle is mandatory.').notEmpty()
  req.assert('tutorId', 'Tutor Id is mandatory.').notEmpty()

  return req.validationErrors()
}

const GET_STUDENT_ATTENDANCE = 
`select s.id as sessionId, ss.performance, ss.attendance, s.created_on as createdOn,
s.duration_hours as durationHours, s.duration_mins as durationMins
from session s
inner join session_student ss
on s.id = ss.session_id
where s.tutor_id = :tutor_id
and ss.student_id = :student_id
order by createdOn desc
LIMIT :offset,:count`

const GET_GROUP_ATTENDANCE = 
`select s.id as sessionId, ss.performance, ss.attendance, s.created_on as createdOn,
s.duration_hours as durationHours, s.duration_mins as durationMins, u.name,
u.email, u.phone, u.image, u.invitation_status as invitationStatus
from session s
inner join session_student ss
on s.id = ss.session_id
inner join user u
on u.id = ss.student_id
where s.group_id = :group_id
order by createdOn desc
LIMIT :offset,:count`

/*
 * Finds out the IDs which are to be deleted. input is the array provided by user and persisted is the already saved array.
 */
const oneToManyCleaner = (input, persisted) => {
  const inputValues = input || []
  const savedValues = persisted || []
  const inputs = []
  const saved = []
  inputValues.map((val) => {
    if (val.id) {
      inputs.push(val.id)
    }
  })
  savedValues.map((val) => {
    if (val.id) {
      saved.push(val.id)
    }
  })

  return _.difference(saved, inputs)
}

const createStudentGroupMap = (groupId, studentId) => {
  return models.StudentGroupMap.create({
    groupId: groupId,
    studentId: studentId,
  })
}

const createOrUpdatePupilInfo = (req, student, cb) => {
  const deferred = Q.defer()
  switch (req.body.tuitionType) {
    case 'group':
      return null
      break
    case 'individual':
    default:
      const pupilInfoProps = {
        tutorId: req.body.tutorId,
        tuitionType: req.body.tuitionType || 'individual',
        billingCycle: req.body.pupilInfo.billingCycle,
        fee: req.body.pupilInfo.fee || null,
        classDurationHours: req.body.pupilInfo.classDurationHours || null,
        classDurationMins: req.body.pupilInfo.classDurationMins || null,
      }

      // if pupil info is already created update the pupil info, else create it
      models.StudentTeacherMap.findOne({
        where: {
          studentId: student && student.id || -1,
          tutorId: req.body.tutorId,
        },
        include: [
          {
            model: models.PupilInfo,
            as: 'pupilInfo',
          },
        ],
      }).then((studentTeacherMap) => {
        let promise
        existingPupilInfo = studentTeacherMap && studentTeacherMap.pupilInfo
        if (existingPupilInfo) {
          promise = existingPupilInfo.updateAttributes(pupilInfoProps)
        } else {
          promise = models.PupilInfo.create(pupilInfoProps)
        }

        promise.then((info) => {
          deferred.resolve(info)
        })
        .catch(err => {
          console.log(err)
        })
      })
  }
  return deferred.promise
}

const createStudentTeacherMap = (studentId, tutorId, pupilInfoId) => {
  return models.StudentTeacherMap.create({
    tuitionType: 'individual',
    studentId,
    tutorId,
    pupilInfoId,
  })
}

exports.getEngagements = (req, res, next) => {
  models.StudentTeacherMap.findAll({
    where: {
      studentId: req.params.id,
    },
    include: [
      {
        model: models.User,
        as: 'tutor',
        attributes: ['id', 'name', 'tagline', 'image', 'userType', 'createdOn'],
      }
    ],
  }).then((results) => {
    return res.send(results.map((result) => result.tutor))
  }, (error) => {
    console.log(error)
  })
}

const createIndividualInvites = (req, student, userMap, existingUsers, tutor) => {
  return createOrUpdatePupilInfo(req, userMap['' + student.phone]).then((pupil) => {
    const newPromises = []
    if (!R.find(R.propEq('phone', student.phone))(existingUsers)) {
      newPromises.push(studentService.createStudentInfo(student, pupil.id, tutor))
      newPromises.push(pupil)
      return Promise.resolve(newPromises)
    }
    return Promise.resolve([student, pupil])
  }).spread((student, pupil) => {
    if (student && pupil) {
      const studentId = student.id ? student.id : userMap['' + student.phone].id
      return createStudentTeacherMap(studentId, tutor.id, pupil.id).then(()=>{}, (error)=>console.log(error))
    }
  })
}

/*
  1. Find out what all students have already been invited. Do not send invites to these students.
  2. Send invite to only those to unknown users. Also, associate these with the tutor.
  3. Send re-invites to users with appropriate invitation status. These must have already been associated with the tutors.
*/
exports.createInvite = (req, res, next) => {
  const students = req.body.students
  const tutorId = req.body.tutorId
  if (req.body.tuitionType !== 'group') {
    const errors = valdiateInviteRequest(req)
    if (errors) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Invalid invite student request object.')
        .setErrors(errors)
        .build()
      )
    }
  }

  const phones = []
  const emails = []

  if (!req.body.students || req.body.students.length === 0) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Students are mandatory.')
      .build()
    )
  } else {
    R.forEach((student) => {
      if (!student.phone) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Students phone number is mandatory.')
          .build()
        )
      } else {
        phones.push(student.phone)
        if (student.email) {
          emails.push(student.email)
        }
      }
    }, req.body.students)

    let errors = studentService.validateNewInvites(phones, emails)
    if (errors) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors([errors])
        .build()
      )
    }
  }

  groupService.validateStudents(req.body.students)
  .then(validateTutors => {
    if (validateTutors) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(validateTutors)
        .build()
      )
    }
    // validate phone number and emails
    const promises = [
      models.User.findAll({
        where: {
          phone: {
            $in: [phones],
          },
        },
      }),
      models.User.findOne({
        where: {
          id: req.body.tutorId,
        },
      })
    ]
    if (emails && emails.length) {
      promises.push(models.User.findAll({
        where: {
          email: {
            $in: [emails],
          },
        },
      }))
    } else {
      promises.push(null)
    }
    models.sequelize.Promise.all(promises)
    .spread((existingPhoneUsers, tutor, existingEmailUsersUnsafe) => {
      const existingEmailUsers = existingEmailUsersUnsafe || []
      if (!tutor) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Invalid tutor ID provided.')
          .build()
        )
      }
      const promises = []
      const existingUsers = R.concat(existingEmailUsers, existingPhoneUsers)
      const newInvitees = R.differenceWith((x, y) => x.phone == y.phone, req.body.students, existingUsers)

      if ('individual' == (req.body.tuitionType || 'individual')) {
        // check if the students are not group students
        studentService.filterIndividualStudents(students, tutorId)
        .then(individualStudents => {
          if (individualStudents && individualStudents.length) {
            return res.status(400).json(new ResponseBuilder()
              .setStatus(400)
              .setMessage(`Students already added - ${R.join(', ', R.pluck('phone')(individualStudents))}.`)
              .build()
            )
          } else {
            studentService.areGroupStudents(R.concat(existingPhoneUsers, existingEmailUsers), req.body.tutorId)
            .then(err => {
              if (err && err.length) {
                return res.status(400).json(new ResponseBuilder()
                  .setStatus(400)
                  .setMessage(`Some students are already registered with us as group students - ${R.join(', ', err)}`)
                  .build()
                )
              }
              else {
                // resend invites
                R.forEach((student) => {
                  if (student.invitationStatus < 1) {
                    studentService.resendStudentInvite(student, tutor)
                  }
                  // send push notification
                  if (tutor.id !== student.id) {
                    pushService.tutorRegisteredStudent({ actor: tutor.name }, student.id)
                  }
                }, existingUsers)
                const userMap = {}
                R.forEach(user => {
                  userMap[user.phone] = {
                    id: user.id,
                  }
                }, existingUsers)

                // create new students
                R.forEach((student) => {
                  promises.push(createIndividualInvites(req, student, userMap, existingUsers, tutor))
                }, req.body.students)

                models.sequelize.Promise.all(promises ? promises : []).spread(() => {
                  return res.status(200).json(new ResponseBuilder()
                    .setStatus(200)
                    .setMessage('The students were successfully invited.')
                    .build()
                  )
                })
              }
            })
          }
        })
      }
      else {
        studentService.filterIndividualStudents(students, tutorId)
        .then(individualStudents => {
          if (individualStudents && individualStudents.length) {
            return res.status(400).json(new ResponseBuilder()
              .setStatus(400)
              .setMessage(`Students already added - ${R.join(', ', R.pluck('phone')(individualStudents))}.`)
              .build()
            )
          }
          else {
            // resend invites
            R.forEach((student) => {
              if (student.invitationStatus < 1) {
                studentService.resendStudentInvite(student, tutor)
              }
            }, existingUsers)
            const groupId = req.body.groupId
            R.forEach((student) => {
              // TODO: this 1 is a hack for sending invitation status as group type. This shouldn't be needed at all.
              promises.push(studentService.createStudentInfo(student, null, tutor, 1))
            }, newInvitees)

            R.forEach((student) => {
              promises.push(groupService.createGroupInvite(student.id, tutor.id, groupId))
            }, existingUsers)

            models.sequelize.Promise.all(promises)
            .spread(function() {
              const groupAssociationPromises = []
              if (arguments && arguments.length) {
                if (groupId) {
                  for (let i = 0; i < arguments.length; i++) {
                    if (arguments[i]) {
                      groupAssociationPromises.push(createStudentGroupMap(groupId, arguments[i].id))
                    }
                  }
                }
                // mark new invitees as group invites
                R.forEach((student) => {
                  if (student.id) {
                    promises.push(groupService.createGroupInvite(student.id, tutor.id, groupId))
                  }
                }, newInvitees)
                models.sequelize.Promise.all(promises)
                .spread(() => res.status(200).json(new ResponseBuilder()
                  .setStatus(200)
                  .setMessage('The students were successfully invited.')
                  .build()
                )).catch((errors) => {
                  console.log(errors)
                  return res.status(500).json(new ResponseBuilder()
                    .setStatus(500)
                    .setMessage('Failed to create student invite. Failed to associate students to groups.')
                    .build()
                  )
                })
              }
              else {
                return res.status(200).json(new ResponseBuilder()
                  .setStatus(200)
                  .setMessage('The students were successfully invited.')
                  .build()
                )
              }
            })
          }
        })
      }
    })
  })
}

/* Marking Attendance  */
const valdiateAttendanceSessionInfo = (req) => {
  req.assert('session.name', 'Session Name is mandatory.').notEmpty()
  req.assert('session.date', 'Session Date is mandatory.').notEmpty()
  req.assert('session.hours', 'Session Hours is mandatory.').notEmpty()
  req.assert('session.mins', 'Session Mins is mandatory.').notEmpty()
  req.assert('session.tutorId', 'Tutor Id is mandatory.').notEmpty()

  return req.validationErrors()
}

validateAttendance = (student) => {
  if(student.studentId) {
    if(student.attendance === undefined && student.performance === undefined) {
      return true
    }
    return false
  }
  return true
}

const createStudentAttendanceSession = (attendanceSession) => {
  return models.Session.create({
    name: attendanceSession.name,
    createdOn: new Date(attendanceSession.date),
    durationHours: attendanceSession.hours,
    durationMins: attendanceSession.mins,
    tutorId: attendanceSession.tutorId,
    groupId: attendanceSession.groupId,
  })
}


const createSessionStudent = (sessionId, studentId, performance, attendance) => {
  return models.SessionStudent.create({
    sessionId,
    studentId,
    performance: performance!== undefined ? performance : null,
    attendance: attendance!== undefined ? attendance : 1,
  })
}


exports.markAttendance = (req, res, next) => {
  const session = req.body.session
  const attendances = req.body.attendances
  if (session && attendances) {
    const errors = valdiateAttendanceSessionInfo(req)
    if (errors) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Session information is mandatory.')
          .setErrors(errors)
          .build()
        )
    }
    else {
      createStudentAttendanceSession(session).then((session) => {
        if (session) {
          const studentAttendances = []
          for (let i = 0; i<attendances.length; i++) {
            const error = validateAttendance(attendances[i])
            if (error) {
              return res.status(400).json(new ResponseBuilder()
                .setStatus(400)
                .setMessage('The student at ' + i +'th position does not have id, attendance or performance marked.')
                .build()
              )
            }
            else {
              studentAttendances.push(createSessionStudent(session.id, attendances[i].studentId, attendances[i].performance,
                attendances[i].attendance))
            }
          }
          models.sequelize.Promise.all(studentAttendances)
          .spread(() => {
            return res.status(200).json(new ResponseBuilder()
              .setStatus(200)
              .setMessage('The attendance was succesfully marked.')
              .setData(session)
              .build()
            )
          }).catch((error) => {
            console.log(error)
            if (error instanceof models.sequelize.ForeignKeyConstraintError) {
              return res.status(400).json(new ResponseBuilder()
                .setStatus(400)
                .setMessage(`Invalid student id.`)
                .build()
              )
            } else {
              return res.status(500).json(new ResponseBuilder()
                .setStatus(500)
                .setMessage('Failed to mark attendance.')
                .build()
              )
            }
          })
        }
        else {
          return res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage('The session was not created.')
            .build()
          )
        }
      }, (error) => {
        console.log(error)
        if (error instanceof models.sequelize.ForeignKeyConstraintError) {
          return res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Invalid tutor id.`)
            .build()
          )
        } else {
          return res.status(500).json(new ResponseBuilder()
            .setStatus(500)
            .setMessage('Failed to mark attendance.')
            .build()
          )
        }
      })

    }
  }
  else {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('The request must contain session and attendances.')
      .build()
    )
  }
}

const getSession = (sessionId, tutorId) => {
  return models.Session.findOne({
    where: {
      id: sessionId,
      tutorId,
    },
    include: [
      {
        model: models.SessionStudent,
        as: 'studentSessions',
      },
    ],
  })
}

exports.updateAttendance = (req, res, next) => {
  const sessionId = req.params.id
  const tutorId = req.body.session && req.body.session.tutorId
  const originalSession = req.body.session
  const attendances = req.body.attendances
  const errors = valdiateAttendanceSessionInfo(req)
  if (errors) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Session information is mandatory.')
      .setErrors(errors)
      .build()
    )
  }
  else {
    getSession(sessionId, tutorId).then((session) => {
      if (!session) {
        return res.status(404).json(new ResponseBuilder()
          .setStatus(404)
          .setMessage('Invalid session ID provided - ${sessionId}')
          .build()
        )
      }
      return session
    }).then((session) => {
      const promises = []

      promises.push(session.updateAttributes({
        name: originalSession.name || session.name,
        date: originalSession.date || session.date,
        durationHours: originalSession.hours || session.durationHours,
        durationMins: originalSession.mins || session.durationMins,
        groupId: originalSession.groupId || session.groupId,
      }))

      if (attendances) {
        // update attendances
        attendances.map((attendance) => {
          const error = validateAttendance(attendance)
          if (error) {
            return res.status(400).json(new ResponseBuilder()
              .setStatus(400)
              .setMessage('The student at ' + i +'th position does not have id, attendance or performance marked.')
              .build()
            )
          }

          attendance.studentId = attendance.studentId
          attendance.tutorId = attendance.tutorId
          attendance.tutor_id = attendance.tutor_id
          attendance.performance = attendance.performance
          attendance.attendance = attendance.attendance
          attendance.sessionId = sessionId
          promises.push(helperService.createOrUpdateStudentSession(models.SessionStudent, attendance))
        })
        // remove unwanted values
        const toBeDeleted = helperService.oneToManyCleaner(attendances, session.studentSessions)
        if (toBeDeleted && toBeDeleted.length) {
          promises.push(models.SessionStudent.destroy({
            where: {
              id: {
                $in: toBeDeleted,
              },
            },
          }))
        }
      }

      models.sequelize.Promise.all(promises)
      .spread(function(sess) {
        models.Session.findAll({
          where: {
            id: sessionId,
            tutorId
          },
          order: [
            ['id', 'desc']
          ],
          include: [{
            model: models.SessionStudent,
            as: 'studentSessions',
            include: [{
              model: models.User,
              as: 'student',
              attributes: ['id', 'name', 'email', 'phone', 'image', 'invitationStatus'],
            }],
          }],          
        }).then((updateSession) => {
          return res.status(200).json(new ResponseBuilder()
            .setStatus(200)
            .setMessage('Attendance successfully updated.')
            .setData(updateSession)
            .build()
          )
        })
      }).catch((error) => {
        console.log(error)
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to update attendance.')
          .build()
        )
      })
    })
  }
}

/* Marking Attendance Ends  */

/* Get Attendance */
exports.getAttendance = (req, res, next) => {
  const offset = req.query && req.query.offset || 0
  const count = req.query && req.query.count || 100
  const studentId = req.params.id
  const tutorId = req.params.tutorId

  models.sequelize.query(
    GET_STUDENT_ATTENDANCE,
    {
      replacements: {
        tutor_id: tutorId,
        student_id: studentId,
        count: +(count),
        offset: +(offset),
      },
      model: models.SessionStudent,
    }
  ).then((results) => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(results)
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch attendances.')
      .build()
    )
  })

}

exports.getGroupAttendance = (req, res, next) => {
  const offset = req.query && req.query.offset || 0
  const count = req.query && req.query.count || 100
  const groupId = req.params.id

  // models.sequelize.query(
  //   GET_GROUP_ATTENDANCE,
  //   {
  //     replacements: {
  //       group_id: groupId,
  //       count: +(count),
  //       offset: +(offset),
  //     },
  //     model: models.SessionStudent,
  //   }
  // )
  models.Session.findAll({
    where: {
      groupId,
    },
    include: [{
      model: models.SessionStudent,
      as: 'studentSessions',
      include: [{
        model: models.User,
        as: 'student',
        attributes: ['id', 'name', 'email', 'phone', 'image', 'invitationStatus'],
      }]
    }],
    offset: +(offset),
    limit: +(count),
  })
  .then((results) => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(results)
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch attendances.')
      .build()
    )
  })

}
/* Get Attendance Ends */

exports.deleteAttendance = (req, res, next) => {
  const sessionId = req.query.sessionId
  const studentId = req.query.studentId

  let promise = null

  if (!sessionId && !studentId) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Please specify either the session ID and student ID.')
      .build()
    )
  }
  else if (sessionId && !studentId) {
    promise = models.SessionStudent.destroy({
      where: {
        sessionId,
      }
    }).then(() => {
      return models.Session.destroy({
        where: {
          id: sessionId,
        }
      })
    })
  }
  else if (sessionId && studentId) {
    promise = models.SessionStudent.destroy({
      where: {
        sessionId,
        studentId,
      }
    })
  }

  promise.then(() => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('Succesfully deleted attendance(s).')
      .build()
    )
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to delete attendances.')
      .build()
      )
  })
}

exports.getStudentGroups = (req, res) => {
  const studentId = req.params.id

  models.StudentGroupMap.findAll({
    where: {
      studentId,
    },
  }).then(studentGroupMaps => {
    if (studentGroupMaps && studentGroupMaps.length) {
      models.StudentGroups.findAll({
        where: {
          id: {
            $in: R.pluck('groupId')(studentGroupMaps),
          },
        },
      }).then(studentGroups => {
        const promises = []
        R.forEach(studentGroup => {
          promises.push(studentGroup.getStudents());
          promises.push(models.User.findById(studentGroup.tutorId));
        }, studentGroups)

        models.sequelize.Promise.all(promises).then(responses => {
          let i = 0
          R.forEach(studentGroup => {
            studentGroup.students = responses[i++]
            studentGroup.tutor = responses[i++]
          }, studentGroups)
          return res.status(200).json(new ResponseBuilder()
            .setStatus(200)
            .setData(studentGroups)
            .build()
          )
        })
      })
    } else {
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData([])
        .build()
      )
    }
  })
}
