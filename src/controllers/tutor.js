const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const R = require('ramda')
const _ = require('lodash')
const moment = require('moment')
const groupService = require('../services/group')
const feedService = require('../services/feed')
const transactionService = require('../services/transaction')
const userService = require('../services/user')
const studentService = require('../services/student')
const DATE_FORMAT = 'MM-DD-YYYY'

const LIKES_COUNT_LITERAL = '(select count(1) from likes where module_id = Feed.id && module_type = "feed")'
const COMMENTS_COUNT_LITERAL = '(select count(1) from comments where module_id = Feed.id && module_type = "feed")'

// fetches all the students which the tutor teaches in a group
const GROUP_STUDENT_QUERY =
`select id, name, image, date_of_birth as dateOfBirth, invitation_status as invitationStatus, phone from
(
  select u.id, u.name, u.date_of_birth, u.invitation_status, u.phone, u.image
  from student_groups sg
  inner join student_group_map sgm
  on sg.id = sgm.group_id
  inner join user u
  on u.id = sgm.student_id
  where sg.tutor_id = :tutor_id
) as T group by id, name, date_of_birth`

const GROUP_STUDENT_GROUP_QUERY = 
`select sg.id, sgm.student_id as tutorId from student_group_map sgm
inner join student_groups sg
on sgm.group_id = sg.id
where sgm.student_id in (:student_ids) and tutor_id = :tutor_id`

const FEED_ATTRIBUTES = [
  'id', 'tutorId', 'description', 'numViews', 'updatedOn', 'createdOn',
  [models.sequelize.literal(LIKES_COUNT_LITERAL), 'likesCount'],
  [models.sequelize.literal(COMMENTS_COUNT_LITERAL), 'commentsCount'],
]

const FEED_WITH_SUBJECTS =
`(select f.id
from feed f inner join feed_subjects fs
on f.id = fs.feed_id
where fs.subject_id in (:subject_ids)
and f.is_moderated = :is_moderated)`

const FEED_WITH_CLASSES =
`(select f.id
from feed f inner join feed_classes fc
on f.id = fc.feed_id
where fc.class_id in (:class_ids)
and f.is_moderated = :is_moderated)`

const FEED_WITH_HASHTAGS =
`(select f.id
from feed f inner join feed_tags ft
on f.id = ft.feed_id
where ft.tag in (:hash_tags)
and f.is_moderated = :is_moderated)`

const MY_FEED =
`select f.id from feed f
inner join feed_subjects fs
on fs.feed_id = f.id
inner join feed_classes fc
on fc.feed_id = f.id
where :query`

const BARE_MINIMUM_FEED =
`select f.id, count(subject_id) as subject_count, count(class_id) as class_count from feed f
left join feed_subjects fs on fs.feed_id = f.id
left join feed_classes fc on fc.feed_id = f.id
where is_moderated = 1
group by id having subject_count = 0 and class_count = 0`

const SUBJECT_ONLY_FEED =
`select f.id from
(select f.id, count(subject_id) as subject_count, count(class_id) as class_count from feed f
left join feed_subjects fs on fs.feed_id = f.id
left join feed_classes fc on fc.feed_id = f.id
where is_moderated = 1
group by id having subject_count > 0 and class_count = 0) sof
inner join feed f on sof.id = f.id
inner join feed_subjects fs on fs.feed_id = f.id
where fs.subject_id in (:subject_ids)`

const CLASS_ONLY_FEED =
`select f.id from
(select f.id, count(subject_id) as subject_count, count(class_id) as class_count from feed f
left join feed_classes fc on fc.feed_id = f.id
left join feed_subjects fs on fs.feed_id = f.id
where is_moderated = 1
group by id having subject_count = 0 and class_count > 0)  sof
inner join feed f on sof.id = f.id
inner join feed_classes fc on fc.feed_id = f.id
where fc.class_id in (:class_ids)`

const moneyValue = value => Number(
  parseFloat(Math.round(value * 100) / 100).toFixed(2)
)

const transformFeed = feeds => R.map(feed => feedService.transformSingleFeed(feed), feeds)

const isTodayTheDate = (dateOfBirth) => {
  if (!dateOfBirth) {
    return false
  }
  const dob = moment(dateOfBirth)
  // TODO: clean this up
  const today = moment().utcOffset('-05:30')
  const month = dob.month()
  const day = dob.day()
  const curMonth = today.month()
  const curDay = today.day()

  if (day === curDay && month === curMonth) {
    return true
  } else {
    return false
  }
}

const getStudentTeacherAssociation = (studentId, tutorId) => {
  return models.StudentTeacherMap.findOne({
    where: {
      studentId: studentId,
      tutorId: tutorId,
    },
    include: [
      {
        model: models.PupilInfo,
        as: 'pupilInfo'
      }
    ],
  })
}

const getInvoicesForTutorStudentPair = (studentId, tutorId) => {
  return models.Invoices.findAll({
    where: {
      payee: studentId,
      recipient: tutorId,
    },
    order: 'id desc',
  })
}

const getBalanceForTutorStudentPair = (studentId, tutorId) => {
  return models.Balance.findOne({
    where: {
      payee: studentId,
      recipient: tutorId,
    },
  })
}

const getStudent = (studentId) => {
  return models.User.findOne({
    where: {
      id: studentId,
    },
    attributes: ['id', 'email', 'name', 'phone', 'image', 'userType', 'dateOfBirth', 'invitationStatus'],
  })
}

exports.getStudentProfile = (req, res, next) => {
  const studentId = req.params.studentId
  const tutorId = req.params.id

  getStudentTeacherAssociation(studentId, tutorId)
  .then((association) => {
    models.sequelize.Promise.all([
      association && association.pupilInfo,
      getStudent(studentId),
      groupService.getTutorStudentGroups(studentId, tutorId),
      getInvoicesForTutorStudentPair(studentId, tutorId),
      getBalanceForTutorStudentPair(studentId, tutorId)  
    ])
    .spread((pupilInfo, student, groups, invoices, balance) => {
      const data = {
        student,
        pupilInfo,
        groups,
        invoices,
        pendingAmount: balance ? balance.amount : 0
      }
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData(data)
        .build()
      )
    }).catch(err => {
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setErrors([err])
        .build()
      )
    })
  })
}

exports.updateStudentProfile = (req, res, next) => {
  const studentId = req.params.studentId
  const tutorId = req.params.id

  const errors = []
  if (!req.body.tuitionType) {
    errors.push('Tuition type is mandatory')
  }
  if (!req.body.pupilInfo) {
    errors.push('PupilInfo is mandatory')
  } else {
    if (!req.body.pupilInfo.billingCycle) {
      errors.push('billingCycle in pupil info is mandatory')
    }
    if (R.isNil(req.body.pupilInfo.fee)) {
      errors.push('fee in pupil info is mandatory')
    }
    if (R.isNil(req.body.pupilInfo.classDurationHours)) {
      errors.push('classDurationHours in pupil info is mandatory')
    }
    if (R.isNil(req.body.pupilInfo.classDurationMins)) {
      errors.push('classDurationMins in pupil info is mandatory')
    }
  }

  if (errors && errors.length) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid request.')
      .setErrors(errors)
      .build()
    )
  }

  getStudent(studentId)
  .then((student) => {
    if (!student) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage(`Failed to find student ${studentId}.`)
        .build()
      )
    } else {
      if (req.body.tuitionType === 'individual') {
        getStudentTeacherAssociation(studentId, tutorId)
        .then((studentTeacherMap) => {
          if (!studentTeacherMap.pupilInfo) {
            // create pupil info
            models.PupilInfo.create({
              billingCycle: req.body.pupilInfo.billingCycle,
              fee: req.body.pupilInfo.fee,
              classDurationHours: req.body.pupilInfo.classDurationHours,
              classDurationMins: req.body.pupilInfo.classDurationMins,
            }).then((pupilInfo) => {
              return res.status(200).json(new ResponseBuilder()
                .setStatus(200)
                .setData({
                  student,
                  tuitionType: 'individual',
                  pupilInfo,
                })
                .build()
              )
            }, (error) => {
              console.log(error)
              return res.status(500).json(new ResponseBuilder()
                .setStatus(500)
                .setMessage(`Failed to create pupil info.`)
                .build()
              )
            })
          }
          else {
            // update pupil info
            studentTeacherMap.pupilInfo.billingCycle = req.body.pupilInfo.billingCycle
            studentTeacherMap.pupilInfo.fee = req.body.pupilInfo.fee
            studentTeacherMap.pupilInfo.classDurationHours = req.body.pupilInfo.classDurationHours
            studentTeacherMap.pupilInfo.classDurationMins = req.body.pupilInfo.classDurationMins

            studentTeacherMap.pupilInfo.save().then((updatedPupilInfo) => {
              return res.status(200).json(new ResponseBuilder()
                .setStatus(200)
                .setData({
                  student,
                  tuitionType: 'individual',
                  pupilInfo: updatedPupilInfo,
                })
                .build()
              )
            })
          }
        })
      } else {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Tuition type group updation is not supported yet. It is comming soon.')
          .build()
        )
      }
    }
  })
}

exports.getStudents = (req, res, next) => {
  const tutorId = req.params.id
  const STUDENT_ATTRIBUTES = ['id', 'name', 'tagline', 'image', 'userType', 'createdOn', 'dateOfBirth', 'invitationStatus', 'phone']
  const students = models.StudentTeacherMap.findAll({
    where: {
      tutorId,
    },
    include: [
      {
        model: models.User,
        as: 'student',
        attributes: STUDENT_ATTRIBUTES,
      },
    ],
  })
  const groupStudents = models.sequelize.query(GROUP_STUDENT_QUERY, {
    replacements: { tutor_id: tutorId },
    model: models.User
  })
  const groupStudentInvites = models.GroupInvite.findAll({
    where: {
      tutorId: tutorId,
    },
    include: [{
      model: models.User,
      as: 'student',
      attributes: STUDENT_ATTRIBUTES,
    }],
  })
  const groupTypeStudents = models.User.findAll({
    where: {
      inviterId: tutorId,
      isGroupInvite: 1,
    },
    attributes: STUDENT_ATTRIBUTES,
  })

  const promises = [students, groupStudents, groupStudentInvites, groupTypeStudents]

  models.sequelize.Promise.all(promises)
  .spread((studentResults, groupResults, groupInvites, groupTypeStudents) => {
    const newPromises = [null]
    let individualStudents = R.map((result) => {
      const student = result.student
      student.isBdayToday = isTodayTheDate(student.dateOfBirth)
      student.tuitionType = 'individual'
      newPromises.push(transactionService.getMoneyOwed(tutorId, student.id))
      return student
    }, studentResults)
    let groupStudents = R.map((student) => {
      student.isBdayToday = isTodayTheDate(student.dateOfBirth)
      student.tuitionType = 'group'
      return student
    }, groupResults)
    groupStudents = R.concat(groupStudents, R.map((invites) => {
      invites.student.isBdayToday = isTodayTheDate(invites.student.dateOfBirth)
      invites.student.tuitionType = 'group'
      return invites.student
    }, groupInvites))
    groupStudents = R.concat(groupStudents, R.map((groupTypeStudent) => {
      groupTypeStudent.isBdayToday = isTodayTheDate(groupTypeStudent.dateOfBirth)
      groupTypeStudent.tuitionType = 'group'
      return groupTypeStudent
    }, groupTypeStudents))

    if (groupStudents.length > 0) {
      let groupStudentIDs = R.pluck('id')(groupStudents)
      groupStudentIDs = R.map(id => +(id), groupStudentIDs)
      newPromises.push(
        models.sequelize.query(GROUP_STUDENT_GROUP_QUERY
        .replace(':student_ids', R.join(', ', groupStudentIDs))
        .replace(':tutor_id', +(tutorId)), {
          model: models.StudentGroups,
        })
      )
    } else {
      newPromises.push(null)
    }

    models.sequelize.Promise.all(newPromises)
    .spread(function() {
      for(var i = 0; i < arguments.length - 2; i++) {
        individualStudents[i].moneyOwed = arguments[i + 1]
      }

      const studentGroupMaps = arguments[arguments.length - 1];
      if (studentGroupMaps && studentGroupMaps.length > 0) {
        R.forEach(groupMap => {
          const studentId = groupMap.tutorId
          const groupId = groupMap.id          
          R.forEach(groupStudent => {
            if (groupStudent.id === studentId) {
              groupStudent.groupId = groupId
            }
          }, groupStudents)
        }, studentGroupMaps)
      }

      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData({
          individual: individualStudents,
          group: R.uniqBy(x => x.id, groupStudents),
        })
        .build()
      )
    })
  })
  .catch((error) => {
    console.log(error)
  })
}
/*
Get sessions for a tutor for a particular date.
*/
exports.getSessionsForDate = (req, res, next) => {
  const tutorId = req.params.id
  const createdOn = req.query && req.query.date ? new Date(req.query && req.query.date) : ''
  const whereClause = createdOn ? { tutorId, createdOn } : { tutorId }
  const sessions = models.Session.findAll({
    where: whereClause
  }).then((sessions) => {
    return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(sessions)
          .build())
  }).catch((error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch sessions.')
      .build()
    )
  })
}

const validateDate = (date, fieldName, dateFormat = DATE_FORMAT) => {
  const errorMsgs = []
  if (!date) {
    errorMsgs.push(`${fieldName} is mandatory`)
  } else if (!moment(date, DATE_FORMAT, true).isValid()) {
    errorMsgs.push(`Please use format ${DATE_FORMAT} for ${fieldName}`)
  }

  return errorMsgs
}

const validateMandatory = (data, fieldName) => {
  if (!data) {
    return [`${fieldName} is mandatory`]
  } else {
    return []
  }
}

const validatePositiveInt = (data, field) => {
  if(/^\+?(0|[1-9]\d*)$/.test(data)) {
    return []
  } else {
    return [`${field} must be a positive integer`]
  }
}

const validateAmount = (data, fieldName) => {
  if (/^\d+(.\d{1,2})?$/.test(data)) {
    return []
  } else {
    return [`${fieldName} is not a valid amount`]
  }
}

const validateDateRange = (req) => {
  let errorMsgs = []
  // validate date from
  errorMsgs = R.concat(errorMsgs, validateDate(req.query.dateFrom, 'Date from'))
  // validate date to
  errorMsgs = R.concat(errorMsgs, validateDate(req.query.dateTo, 'Date to'))
  // validate the date range supplied
  if (moment(req.body.dateTo, DATE_FORMAT).diff(moment(req.body.dateFrom, DATE_FORMAT)) < 0) {
    errorMsgs.push('invalid date range selected')
  }
  return errorMsgs
}

const validateInvoice = (req) => {
  let errorMsgs = []
  // validate date from
  errorMsgs = R.concat(errorMsgs, validateDate(req.body.dateFrom, 'Date from'))
  // validate date to
  errorMsgs = R.concat(errorMsgs, validateDate(req.body.dateTo, 'Date to'))
  // validate amount
  errorMsgs = R.concat(errorMsgs, validateAmount(req.body.amount, 'amount'))
  // validate suggested sessions
  errorMsgs = R.concat(errorMsgs, validatePositiveInt(req.body.suggestedSessions, 'suggested sessions count'))
  // validate suggested hours
  errorMsgs = R.concat(errorMsgs, validatePositiveInt(req.body.suggestedHours, 'suggested hours count'))
  // validate the date range supplied
  if (moment(req.body.dateTo, DATE_FORMAT).diff(moment(req.body.dateFrom, DATE_FORMAT)) < 0) {
    errorMsgs.push('invalid date range selected')
  }
  return errorMsgs
}

const validatePayment = (req) => {
  let errorMsgs = []
  // validate amount
  errorMsgs = R.concat(errorMsgs, validateAmount(req.body.amount, 'amount'))
  // validate discount
  errorMsgs = R.concat(errorMsgs, validateAmount(req.body.discount, 'discount'))
  // validate invoiceId
  errorMsgs = R.concat(errorMsgs, validateMandatory(req.body.invoiceId, 'invoice ID'))
  return errorMsgs
}

exports.updateInvoice = (req, res, next) => {
  const id = req.params.id
  const dateFrom = moment(req.body.dateFrom, DATE_FORMAT)
  const dateTo = moment(req.body.dateTo, DATE_FORMAT)
  const amount = req.body.amount ? parseFloat(req.body.amount) : 0
  const name = req.body.name
  const type = 'custom'

  const errors = validateInvoice(req)
  if (!R.isEmpty(errors)) {
    console.log(errors)
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setErrors(errors)
      .setMessage('Invalid request body')
    )
  }

  models.Invoices.findOne({
    where: {
      status: 'pending',
      id,
    }
  }).then(invoice => {
    if (!invoice) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors(errors)
        .setMessage('Invalid invoice ID')
      )
    }
    const amountDiff = amount - invoice.amount
    let pendingAmount = 0
    const props = {
      dateFrom,
      dateTo,
      amount,
      name: name || invoice.name,
    }
    
    return models.sequelize.transaction(function (t) {
      return models.Balance.findOne({
        where: {
          payee: invoice.payee,
          recipient: invoice.recipient,
        }
      }, {transaction: t})
      .then(balance => {
        balance.amount += amountDiff
        pendingAmount = balance.amount
        return balance.save({transaction: t})
      })
      .then(balance => {
        return invoice.updateAttributes(props, {transaction: t})
      })
    }).then(invoice => {
      invoice.pendingAmount = pendingAmount
      return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(invoice)
      .build())
    }).catch(function (error) {
      console.log(error)
      let status = 500
      let message = 'Failed to create invoice.'

      if (error instanceof models.sequelize.ForeignKeyConstraintError) {
        status = 400
        message = 'Either the tutor ID or student ID is invalid.'  
      }
      return res.status(status).json(new ResponseBuilder()
        .setStatus(status)
        .setMessage(message)
        .setErrors(error)
        .build()
      )
    })
  })
}

exports.createInvoice = (req, res, next) => {
  const tutorId = req.params.tutorId
  const studentId = req.params.studentId
  const dateFrom = moment(req.body.dateFrom, DATE_FORMAT)
  const dateTo = moment(req.body.dateTo, DATE_FORMAT)
  const amount = req.body.amount
  const name = req.body.name
  const suggestedSessions = req.body.suggestedSessions
  const suggestedHours = req.body.suggestedHours
  const type = 'custom'
  const status = 'pending'

  const errors = validateInvoice(req)
  if (!R.isEmpty(errors)) {
    console.log(errors)
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setErrors(errors)
      .setMessage('Invalid request body')
    )
  }

  return models.sequelize.transaction(function (t) {
    return models.Invoices.create({
      dateFrom,
      dateTo,
      amount,
      suggestedSessions,
      suggestedHours,
      type,
      name,
      status,
      payee: studentId,
      recipient: tutorId,
    }, {transaction: t}).then(function (invoice) {
      const promises = []
      promises.push(invoice)
      promises.push(models.Balance.findOne({
        where: {
          payee: invoice.payee,
          recipient: invoice.recipient,
        }
      }, {transaction: t}))
      return models.sequelize.Promise.all(promises)
    }, {transaction: t}).spread(function(invoice, balance) {
      const promises = []
      promises.push(invoice)
      if (balance) {
        promises.push(balance.updateAttributes({
          amount: balance.amount + invoice.amount,
        }, {transaction: t}))
      } else {
        promises.push(models.Balance.create({
          amount: invoice.amount,
          payee: invoice.payee,
          recipient: invoice.recipient,
        }))
      }
      return models.sequelize.Promise.all(promises)
    })
  }).spread(function (invoice, balance) {
    invoice.pendingAmount = balance.amount
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(invoice)
      .build())
  }).catch(function (error) {
    console.log(error)
    let status = 500
    let message = 'Failed to create invoice.'

    if (error instanceof models.sequelize.ForeignKeyConstraintError) {
      status = 400
      message = 'Either the tutor ID or student ID is invalid.'  
    }
    return res.status(status).json(new ResponseBuilder()
      .setStatus(status)
      .setMessage(message)
      .setErrors(error)
      .build()
    )
  })
}

exports.getInvoiceSuggestions = (req, res, next) => {
  const tutorId = req.params.tutorId
  const studentId = req.params.studentId
  const groupType = req.query.type
  const dateFrom = moment(req.query.dateFrom, DATE_FORMAT)
  const dateTo = moment(req.query.dateTo, DATE_FORMAT)
    .set({ hour: 23, minute: 59, second: 59 })
    .toDate()

  const errors = validateDateRange(req)
  if (!R.isEmpty(errors)) {
    console.log(errors)
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setErrors(errors)
      .setMessage('Invalid request')
    )
  }

  models.Session.findAll({
    where: {
      tutorId,
      createdOn: {
        $lte: dateTo,
        $gte: dateFrom,
      },
    },
    include: [{
      model: models.SessionStudent,
      as: 'studentSessions',
      where: {
        studentId,
      },
      attributes: ['sessionId'],
    }],
  }).then((sessions) => {
    let sessionCount = 0
    let minutesCount = 0
    let amount = 0

    R.forEach((session) => {
      sessionCount += 1
      const len = R.length(session.studentSessions)
      if (len) {
        minutesCount += (session.durationHours * 60) + session.durationMins
      }
    }, sessions)

    const noOfHours = Math.floor(minutesCount/60)

    models.StudentTeacherMap.findOne({
      where: {
        tutorId,
        studentId,
      },
      include: [
        {
          model: models.PupilInfo,
          as: 'pupilInfo',
        },
      ],
    }).then(studentTeacherMap => {
      return new Promise((resolve, reject) => {
        if (groupType) {
          groupService.getGroupPupilInfo(studentId, tutorId)
          .then(temporaryPupilInfo => {
            if (!temporaryPupilInfo || !temporaryPupilInfo.length) {
              resolve()
            } else {
              models.PupilInfo.findById(temporaryPupilInfo[0].id)
              .then(pupilInfo => {
                console.log(pupilInfo.id)
                resolve(pupilInfo)
              })
            }
          })
        } else {
          resolve(studentTeacherMap ? studentTeacherMap.pupilInfo : null)
        }
      })
    }).then(pupilInfo => {
      if (pupilInfo && pupilInfo.fee) {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData({
            noOfSessions: sessionCount,
            noOfHours,
            noOfMins: minutesCount - (noOfHours * 60),
            amount: moneyValue((minutesCount/60) * pupilInfo.fee),
          })
          .build()
        )
      } else {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData({
            noOfSessions: sessionCount,
            noOfHours,
            noOfMins: minutesCount - (noOfHours * 60),
            amount: 0,
          })
          .build()
        )
      }
    })
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch invoice suggestion.')
      .build()
    )
  })
}

exports.getInvoiceSuggestionsForGroup = (req, res, next) => {
  const tutorId = req.params.tutorId
  const groupId = req.params.groupId
  const dateFrom = moment(req.query.dateFrom, DATE_FORMAT)
  const dateTo = moment(req.query.dateTo, DATE_FORMAT)
    .set({ hour: 23, minute: 59, second: 59 })
    .toDate()

  const errors = validateDateRange(req)
  if (!R.isEmpty(errors)) {
    console.log(errors)
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setErrors(errors)
      .setMessage('Invalid request')
    )
  }

  models.Session.findAll({
    where: {
      tutorId,
      groupId,
      createdOn: {
        $lte: dateTo,
        $gte: dateFrom,
      },
    }
  }).then((sessions) => {
    let sessionCount = 0
    let minutesCount = 0

    if (sessions) {
      R.forEach((session) => {
        sessionCount += 1
        minutesCount += (session.durationHours * 60) + session.durationMins
      }, sessions)
    }

    const noOfHours = Math.floor(minutesCount/60)

    models.StudentGroups.findOne({
      where: {
        tutorId,
        id: groupId,
      },
      include: [
        {
          model: models.PupilInfo,
          as: 'pupilInfo',
        },
      ],
    }).then(group => {
      if (group && group.pupilInfo&& group.pupilInfo.fee) {
        console.log(moneyValue((minutesCount/60) * group.pupilInfo.fee), group.pupilInfo.fee, minutesCount)
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData({
            noOfSessions: sessionCount,
            noOfHours,
            noOfMins: minutesCount - (noOfHours * 60),
            amount: moneyValue((minutesCount/60) * group.pupilInfo.fee),
          })
          .build()
        )
      } else {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData({
            noOfSessions: sessionCount,
            noOfHours,
            noOfMins: minutesCount - (noOfHours * 60),
          })
          .build()
        )
      }
    })
  }, (error) => {
    console.log(error)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to fetch invoice suggestion.')
      .build()
    )
  })
}

exports.deleteInvoice = (req, res, next) => {
  const id = req.params.id

  return models.sequelize.transaction(function (t) {
    return models.Invoices.findOne({
      where: {
        id,
        status: 'pending',
      },
    })
    .then(invoice => {
      if (!invoice) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Either the invoice ID is invalid or the invoice is not pending.')
          .build()
        )
      }
      return invoice
    })
    .then(invoice => {
      // find the balance
      return models.Balance.findOne({
        where: {
          payee: invoice.payee,
          recipient: invoice.recipient,
        }
      }, {transaction: t})
      .then(balance => {
        const amount = Number(
          parseFloat(Math.round((balance.amount - invoice.amount) * 100) / 100).toFixed(2)
        )
        return balance.updateAttributes({
          amount,
        }, {transaction: t})
      })
      .then(balance => {
        return models.Invoices.destroy({
          where: {
            id,
            status: 'pending',
          },
        }, {transaction: t})
      })
    })
    .then(deletedInvoice => {
      return res.status(204).json(new ResponseBuilder()
        .setStatus(204)
        .setMessage('The invoice has been deleted.')
        .build()
      )
    })
    .catch(err => {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setData()
        .setMessage('Failed to delete invoice.')
        .build()
      )
    })
  })
}

exports.createPayments = (req, res, next) => {
  const tutorId = req.params.tutorId
  const studentId = req.params.studentId
  const invoiceId = req.body.invoiceId
  const amount = req.body.amount
  const discount = req.body.discount
  const remarks = req.body.remarks

  const errors = validatePayment(req)
  if (!R.isEmpty(errors)) {
    console.log(errors)
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setErrors(errors)
      .setMessage('Invalid payment request')
    )
  }

  let balanceAmount = 0

  return models.sequelize.transaction(function (t) {
    return models.Balance.findOne({
      where: {
        payee: studentId,
        recipient: tutorId,
      }
    }, {transaction: t})
    .then(balance => {
      balanceAmount = balance.amount
      return models.Invoices.findOne({
        where: {
          id: invoiceId,
          status: 'pending',
        },
      }, {transaction: t})  
    })
    .then((invoice) => {
      if (!invoice) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Either the invoice ID is invalid or the invoice is not pending at all.')
          .build()
        )
      }

      let carryForward = 0
      let status
      // if the amount has been paid in full
      if ((amount + discount) === invoice.amount) {
        status = 'paid'
      }
      // if the amount hasn't been paid in full
      else {
        carryForward = invoice.amount - (amount + discount)
        status = 'partially-paid'
      }

      if (invoice.amount <= carryForward) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Balance or carry forward amount cannot be equal to or greater than invoice amount.')
          .build()
        )
      }
      else if ((amount + discount) > balanceAmount) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('The payment \'amount + discount\' cannot be greater than invoice amount - ${balanceAmount}.')
          .build()
        )
      }

      invoice.status = status
      return models.sequelize.Promise.all([invoice.save({transaction: t}), carryForward])
    })
    .spread(function(invoice, carryForward) {
      const promises = [invoice]
      promises.push(models.Payments.create({
        amount,
        discount,
        carryForward,
        remarks,
        invoiceId: invoice.id,
      }, {transaction: t}))
      promises.push(carryForward)
      return models.sequelize.Promise.all(promises)
    })
    .spread(function (updatedInvoice, payment, carryForward) {
      const promises = [updatedInvoice, payment]
      promises.push(models.Balance.findOne({
        where: {
          payee: studentId,
          recipient: tutorId,
        }
      }, {transaction: t}))
      promises.push(carryForward)
      return models.sequelize.Promise.all(promises)
    }).spread(function (updatedInvoice, payment, balance, carryForward) {
      if (!balance) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Invalid payment. Advance payments are not supported.')
          .build()
        )
      } else {
        const promises = [updatedInvoice, payment]
        payment.carryForward = Number(
          parseFloat(Math.round(payment.carryForward * 100) / 100).toFixed(2)
        )
        promises.push(balance.updateAttributes({
          amount: balance.amount - (payment.amount + payment.discount),
        }, {transaction: t}))
        return models.sequelize.Promise.all(promises)
      }
    })
  })
  .spread(function (updatedInvoice, payment, balance) {
    payment.pendingAmount = balance.amount
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(payment)
      .build()
    )
  })
  .catch(function (error) {
    console.log(error)
    let status = 500
    let message = 'Failed to create invoice.'

    if (error instanceof models.sequelize.ForeignKeyConstraintError) {
      status = 400
      message = 'Either the tutor ID or student ID is invalid.'  
    }
    return res.status(status).json(new ResponseBuilder()
      .setStatus(status)
      .setMessage(message)
      .setErrors(error)
      .build()
    )
  })
}

exports.fetchFeed = (req, res, next) => {
  const offset = req.query && +(req.query.offset) || 0
  const count = req.query && +(req.query.count) || 50
  const tutorId = req.params.id
  const type = req.query.type

  // search params
  const hashTags = req.query.hash_tags
  const subjectIds = req.query.subject_ids
  const classIds = req.query.class_ids
  const moderationType = type === 'unmoderated' ? 0 : 1

  const filterPromises = []

  let isFilterQuery = false

  filterPromises.push(models.Feed.findAll({
    where: {
      isModerated: 1,
      tutorId,
    }
  }))

  if (subjectIds) {
    filterPromises.push(models.sequelize.query(
      FEED_WITH_SUBJECTS.replace(':subject_ids', subjectIds).replace(':is_moderated', moderationType), {
        model: models.Feed,
      })
    )
    isFilterQuery = true
  } else {
    filterPromises.push(null)
  }

  if (classIds) {
    filterPromises.push(models.sequelize.query(
      FEED_WITH_CLASSES.replace(':class_ids', classIds).replace(':is_moderated', moderationType), {
        model: models.Feed,
      })
    )
    isFilterQuery = true
  } else {
    filterPromises.push(null)
  }

  if (hashTags) {
    const transformedTags = R.map(tag => `'${tag}'`, R.split(',', hashTags))
    filterPromises.push(models.sequelize.query(
      FEED_WITH_HASHTAGS.replace(':hash_tags', transformedTags).replace(':is_moderated', moderationType), {
        model: models.Feed,
      })
    )
    isFilterQuery = true
  } else {
    filterPromises.push(null)
  }

  let promise
  if (isFilterQuery) {
    promise = models.sequelize.Promise.all(filterPromises).spread((myFeedIds, subjectFeedIds, classFeedIds, tagFeedIds) => {
      let result = []
      if (subjectFeedIds && classFeedIds) {
        result = R.intersection(R.pluck('id')(subjectFeedIds), R.pluck('id')(classFeedIds))
      } else if (subjectFeedIds) {
        result = R.pluck('id')(subjectFeedIds)
      } else if (classFeedIds) {
        result = R.pluck('id')(classFeedIds)
      }
      if (tagFeedIds) {
        result = R.intersection(result, R.pluck('id')(tagFeedIds))
      }
      if (myFeedIds) {
        // result = R.intersection(result, R.pluck('id')(myFeedIds))
      }
      return result
    })
    .then(feedIds => {
      if (feedIds && feedIds.length) {
        return models.Feed.scope('base').findAll({
          where: {
            id: { $in: feedIds }
          },
          limit: count,
          offset,
          order: [
            ['createdOn', 'DESC']
          ],
          attributes: FEED_ATTRIBUTES,
        })
      } else {
        return []
      }
    })
  } else {
    promise = models.Feed.scope('base').findAll({
      where: {
        isModerated: moderationType,
      },
      limit: count,
      offset,
      order: [
        ['createdOn', 'DESC']
      ],
      attributes: FEED_ATTRIBUTES,
    })
  }

  promise.then(feeds => {
    feedService.fetchMyLikes(tutorId, feeds).then(likes => {
      if (likes && likes.length) {
        const likedIds = R.pluck('moduleId')(likes)
        R.forEach(feed => {
          feed.hasLiked = R.contains(feed.id, likedIds)
        }, feeds)
      }
      return res.status(200).json(
        new ResponseBuilder().setStatus(200).setData(transformFeed(feeds)).build()
      )
    })
  }).catch(err => {
    console.log(err)
    return res.status(400).json(
      new ResponseBuilder()
        .setStatus(400)
        .setErrors(['Invalid request.'])
        .setMessage(['Unexpected error occurred.'])
        .build()
    )
  })
}

exports.fetchUserFeed = (req, res, next) => {
  const offset = req.query && +(req.query.offset) || 0
  const count = req.query && +(req.query.count) || 50
  const tutorId = req.params.id

  models.TutorCourseDetail.findAll({
    where: {
      tutorId,
    },
  }).then((courseDetails) => {
    if (courseDetails && courseDetails.length) {
      const ORs = []
      const subjectIds = []
      const classIds = []
      R.forEach(course => {
        ORs.push(`(subject_id = ${course.subject} AND class_id = ${course.class})`)
        subjectIds.push(course.subject)
        classIds.push(course.class)
      }, courseDetails)

      const promises = []
      promises.push(models.sequelize.query(
        MY_FEED.replace(':query', `is_moderated = 1 AND (${R.join(' OR ', ORs)})`), {
          model: models.Feed,
        }
      ))
      promises.push(models.sequelize.query(
        BARE_MINIMUM_FEED, {
          model: models.Feed,
        }
      ))
      if (subjectIds.length) {
        promises.push(models.sequelize.query(
          SUBJECT_ONLY_FEED.replace(':subject_ids', R.join(', ', subjectIds)), {
            model: models.Feed,
          }
        ))
      } else {
        promises.push([])
      }
      if (classIds.length) {
        promises.push(models.sequelize.query(
          CLASS_ONLY_FEED.replace(':class_ids', R.join(', ', classIds)), {
            model: models.Feed,
          }
        ))
      } else {
        promises.push([])
      }
      return models.sequelize.Promise.all(promises).spread((myFeeds, bareFeeds, subjectOnlyFeeds, classOnlyFeeds) => {
        const feedIds = _.concat(myFeeds || [], bareFeeds || [], subjectOnlyFeeds || [], classOnlyFeeds || [])
        if (feedIds && feedIds.length) {
          return models.Feed.scope('base').findAll({
            where: {
              id: { $in: R.map(feedId => feedId.id, feedIds) }
            },
            limit: count,
            offset,
            order: [
              ['createdOn', 'DESC']
            ],
            attributes: FEED_ATTRIBUTES,
          })
        } else {
          return []
        }
      })
    } else {
      // prepare query
      return models.Feed.scope('base').findAll({
        where: {
          isModerated: 1,
        },
        limit: count,
        offset,
        order: [
          ['createdOn', 'DESC']
        ],
        attributes: FEED_ATTRIBUTES,
      })
    }
  }).then(feeds => {
    console.log('Feeds aty')
    feedService.fetchMyLikes(tutorId, feeds).then(likes => {
      if (likes && likes.length) {
        const likedIds = R.pluck('moduleId')(likes)
        R.forEach(feed => {
          feed.hasLiked = R.contains(feed.id, likedIds)
        }, feeds)
      }
      return res.status(200).json(
        new ResponseBuilder().setStatus(200).setData(transformFeed(feeds)).build()
      )
    })
  }).catch(err => {
    console.log(err)
    return res.status(400).json(
      new ResponseBuilder()
        .setStatus(400)
        .setErrors(['Invalid request.'])
        .setMessage(['Unexpected error occurred.'])
        .build()
    )
  })
}

exports.fetchFeedById = (req, res, next) => {
  const tutorId = req.params.id
  const feedId = req.params.feedId
  const view = req.query.view
  const type = req.query.type

  const whereClause = {
    id: feedId,
  }

  if (!type || !(type === 'unmoderated')) {
    whereClause.isModerated = 1
  }

  models.Feed.scope('detailed').findOne({
    where: whereClause,
  })
  .then(feed => {
    if (feed && view === 'true') {
      feed.numViews++;
      return feed.save()
    }
    return feed
  })
  .then(feed => {
    if (feed) {
      feed.likesCount = feed.likes ? feed.likes.length : 0
      feed.commentsCount = feed.comments ? feed.comments.length : 0

      feedService.fetchMyLikes(tutorId, [feed]).then(likes => {
        if (likes && likes.length) {
          feed.hasLiked = true
        } else {
          feed.hasLiked = false
        }
        return res.status(200).json(
          new ResponseBuilder()
            .setStatus(200)
            .setData(feedService.transformSingleFeed(feed))
            .build()
        )
      })
    } else {
      return res.status(404).json(
        new ResponseBuilder().setStatus(404).setMessage(`Failed to find feed ${feedId} for user ${tutorId}.`).build()
      )
    }
  })
}

exports.fetchFeedLikes = (req, res, next) => {
  const tutorId = req.params.id
  const feedId = req.params.feedId

  models.Feed.scope('withLikes').findOne({
    where: {
      // tutorId,
      id: feedId,
    },
    attributes: FEED_ATTRIBUTES,
  })
  .then(feed => {
    return res.status(200).json(
      new ResponseBuilder().setStatus(200).setData(feed.likes).build()
    )
  })
}

exports.fetchFeedComments = (req, res, next) => {
  const tutorId = req.params.id
  const feedId = req.params.feedId

  models.Feed.scope('withComments').findOne({
    where: {
      // tutorId,
      id: feedId,
    },
    attributes: FEED_ATTRIBUTES,
  })
  .then(feed => {
    return res.status(200).json(
      new ResponseBuilder().setStatus(200).setData(feed ? feed.comments : []).build()
    )
  })
}

exports.removeStudent = (req, res, next) => {
  const tutorId = req.params.id
  const studentId = req.params.studentId

  studentService.removeIndividualStudent(tutorId, studentId)
  .then(x => {
    return res.status(200).json(
      new ResponseBuilder().setStatus(200).setMessage('Individual student has been successfully removed.').build()
    )
  })
}