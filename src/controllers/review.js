const R = require('ramda')
const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const smsService = require('../services/sms')
const pushService = require('../services/push')
const userService = require('../services/user')

const findOrCreateTutorUserReview = (req, res) => {
  if (!req.body.entityId) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing entityId parameter')
      .setStatus(400)
      .build()
    )
  }
  if (!req.body.review) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing reviews parameter')
      .setStatus(400)
      .build()
    )
  }
  
  const reviewObj = {
    userId: req.body.userId,
    tutorId: req.body.entityId,
    reviews: req.body.review,
    createdOn: new Date()
  }

  return models.sequelize.transaction(function (t) {
    return models.User.findById(reviewObj.tutorId)
    .then(user => {
      if (!user) {
        return res.status(404).json(new ResponseBuilder()
          .setMessage('Failed to find the user being reviewed.')
          .setStatus(404)
          .build()
        )
      }
      return user
    }).then(user => {
      if (user) {
        user.profileCompleteness = 'review'
        return user.save({transaction: t})
      }
      return null
    }).then(user => {
      if (user) {
        userService.updateSearchScore(user.id)
        return models.TutorUserReviews.create(reviewObj, {transaction: t})
      }
      return null
    })
  }).then(reviewObj => {
    // send push notification
    if (reviewObj.userId !== reviewObj.tutorId) {
      userService.getUser(reviewObj.userId).then(actor => {
        if (actor) {
          pushService.reviewAdded({ actor: actor.name }, reviewObj.tutorId)
        }
      })
    }
    return res.status(200).json(new ResponseBuilder()
      .setMessage('Review has been successfully posted.')
      .setStatus(200)
      .build()
    )
  }).catch(err => {
    cosnole.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to post user review. Please try after some time.')
      .setStatus(500)
      .build()
    )
  })
}

exports.postReviews = (req, res, next) => {
  switch(req.query.type) {
    case 'user' :
      findOrCreateTutorUserReview(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}

exports.postInvites = (req, res, next) => {
  // get the inputs from the req
  const contacts = req.body.contacts || []
  const tutorId = req.body.tutorId

  if (tutorId && contacts.length) {
    models.User.findById(tutorId)
    .then(user => {
      if (!user) {
        return res.status(404).json(new ResponseBuilder()
          .setMessage(`Failed to find tutor with id - ${tutorId}.`)
          .setStatus(404)
          .build()
        )
      }

      const invalidNumbers = R.filter(contact => {
        const contactStr = contact + ''
        return !R.is(Number, contact) || contactStr.length !== 10
      }, contacts)

      if (invalidNumbers && invalidNumbers.length) {
        return res.status(400).json(new ResponseBuilder()
          .setMessage(`Contact should be numeric and of length 10. ${R.join(', ', invalidNumbers)}`)
          .setStatus(400)
          .build()
        )        
      } else {
        R.forEach(contact => {
          smsService.smsReviewInvite(contact, user)
        }, contacts)

        return res.status(200).json(new ResponseBuilder()
          .setMessage('SMS invites have been queued.')
          .setStatus(200)
          .build()
        )
      }

    })
  } else {
    return res.status(400).json(new ResponseBuilder()
      .setMessage(`Invalid parameters received.`)
      .setStatus(400)
      .build()
    )
  }
}