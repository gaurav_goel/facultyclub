const fs = require('fs')
const path = require('path')
const moment = require('moment')
// FIXME
const { fileIngest, maxFileSize } = require('../../config/application')
const models = require('../models')
const ResponseBuilder = require('../helper/response-builder')

const uploadService = require('../services/upload')
const userService = require('../services/user')
const FILE_STORE = path.join(__dirname, '../../uploads')

const ACCEPTED_MIME_TYPES = fileIngest.acceptedMimeTypes
const ACCEPTED_IMAGE_MIME_TYPES = fileIngest.acceptedImageMimeTypes

/**
* Buffer size used to download an asset.
*
* @attribute DOWNLOAD_BUFFER_SIZE
* @type {Number}
**/
const DOWNLOAD_BUFFER_SIZE = 2 * 1024

exports.getUploadFilePath = uploadService.getUploadFilePath

exports.upload = (upload, req, res) => {
  upload(req, res, (err) => {
    const response = uploadService.validateFileUpload(err, req)
    if (response) {
      return res.status(response.getHTTPStatus()).json(response.build())
    } else {
      models.sequelize.Promise.resolve(uploadService.createFileMeta(req.file))
      .then((fileMetaObj) => {
        if (!fileMetaObj) {
          return res.status(404).json(new ResponseBuilder()
            .setStatus(404)
            .setMessage('Failed to create meta data for a file')
            .build()
          )
        }
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(fileMetaObj)
          .build()
        )
      })
    }
  })
}

exports.tutorContentUpload = (upload, req, res) => {
  upload(req, res, (err) => {
    const tutorId = req.params.id
    const description = req.body.description

    const response = uploadService.validateFileUpload(err, req)
    if (response) {
      return res.status(response.getStatus()).json(response.build())
    }

    models.sequelize.Promise.resolve(models.User.findOne({
      where: {
        id: tutorId,
      }
    }))
    .then(user => {
      // validate that the user exists
      if (!user) {
        return res.status(404).json(new ResponseBuilder()
          .setStatus(404)
          .setMessage(`Failed to find a tutor with ID: ${tutorId}`)
          .build()
        )
      }
      return user
    })
    .then(user => {
      if (user) {
        user.profileCompleteness = 'content'
        return user.save()
      }
    })
    .then(user => {
      if (user) {
        userService.updateSearchScore(user.id)
      }
      // create the file meta
      return models.sequelize.Promise.all([
        user,
        uploadService.createFileMeta(req.file),
      ])
    })
    .spread((user, fileMetaObj) => {
      // validate file meta
      if (!fileMetaObj) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage('Failed to create meta data for a file')
          .build()
        )
      }
      // create the tutor content data
      const timeNow = moment()
      models.TutorContentUpload.create({
        tutorId,
        contentUrl: fileMetaObj.uri,
        description: description || '',
        numViews: 0,
        updatedOn: timeNow,
        createdOn: timeNow,
        mimetype: fileMetaObj.mimetype,
      }).then(contentUpload => {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(contentUpload)
          .build()
        )
      }, err => {
        console.log(err)
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to upload content')
          .build()
        )
      })
      
    })
    .catch(err => {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Failed to upload content')
        .build()
      )
    })
  })
}

exports.updateContent = (req, res) => {
  req.assert('description', 'Plase select description').notEmpty()
  req.assert('contentUrl', 'Please enter content url').notEmpty()
  req.assert('mimetype', 'Please enter mimetype').notEmpty()

  const errors = req.validationErrors()
  if (errors) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid request.')
      .setErrors(errors)
      .build()
    )
  }

  const tutorId = req.params.id
  const contentId = req.params.contentId

  const description = req.body.description
  const contentUrl = req.body.contentUrl
  const mimetype = req.body.mimetype

  models.TutorContentUpload.findOne({
    where: {
      tutorId,
      id: contentId,
    },
  }).then(content => {
    content.description = description
    content.contentUrl = contentUrl
    content.mimetype = mimetype
    content.save().then(updatedContent => {
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData(updatedContent)
        .build()
      )
    })
  })
}

/**
 * Downloads the asset files
 */
exports.egest = function (req, res, fileIdentifier, basePath) {
  if (!fileIdentifier) {
    return res.status(400).json({
      msg: 'Invalid file property in query string.'
    })
  }
  let whereClause
  if (req.query.thumb) {
    whereClause = {
      $or: [
        {
          filename: `${fileIdentifier}.thumb`,
        },
        {
          filename: `${fileIdentifier}.thumb.png`,
        },
      ],
    }
  } else {
    whereClause = {
      filename: fileIdentifier
    }
  }
  models.FileMeta.findOne({
    where: whereClause,
  }).then((fileObj) => {
    if (!fileObj) {
      return res.status(400).send()
    }

    const filePath = basePath + fileObj.path

    // file exists
    fs.exists(filePath, (exists) => {
      if (exists) {

        if (req.query.view) {
          models.TutorContentUpload.findOne({
            where: {
              contentUrl: fileObj.uri,
            }
          }).then((content) => {
            content.numViews++
            content.save()
          })
        }

        if (fileObj.mimetype.indexOf('image') === -1) {
          res.setHeader('Content-disposition', `attachment; filename=${fileObj.originalName}`)
        }
        if (fileObj.size) {
          res.setHeader('Content-Length', fileObj.size)
        }
        res.setHeader('Content-type', fileObj.mimetype)

        fs.createReadStream(filePath, { bufferSize: DOWNLOAD_BUFFER_SIZE }).pipe(res)
      } else {
        return res.status(400).json({
          msg: 'Failed to find file'
        })
      }
    })
  })
}

exports.deleteContent = (req, res) => {
  const tutorId = req.params.id
  const contentId = req.params.contentId

  models.TutorContentUpload.findOne({
    where: {
      tutorId,
      id: contentId,
    },
  }).then(content => {
    if (!content) {
      return res.status(404).json(new ResponseBuilder()
        .setMessage('Invalid tutor ID or content ID')
      )
    }

    models.sequelize.Promise.all([
      models.FileMeta.findOne({
        where: {
          uri: content.contentUrl,
        },
      }),
      models.TutorContentUpload.destroy({
        where: {
          id: contentId,
        },
      }),
    ]).spread((fileMeta, destroyedContent) => {
      try {
        const filePath = `${FILE_STORE}${fileMeta.path}`
        if (fs.existsSync(filePath)) {
          fs.unlinkSync(filePath)
        }
        else {
          console.log(`${filePath} doesn't exist`)
        }
      } catch (err) {
        console.log(err)
      }
      models.FileMeta.destroy({
        where: {
          id: fileMeta.id,
        },
      }).then(() => {
        return models.TutorContentUpload.count({
          where: {
            tutorId,
          },
        })
      }).then(contentCount => {
        console.log('### Content count - ' + contentCount)
        if (contentCount === 0) {
          return models.User.findById(tutorId)
        }
        return null
      }).then(user => {
        if (user) {
          user.reduceProfileCompleteness = 'content'
          return user.save()
        }
        return null
      }).then(() => {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setMessage('Content successfully deleted')
        )
      })
    }).catch(err => {
      console.log(err)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Failed to delete content')
        .build()
      )
    })
  })
}

exports.fileFilter = (req, file, cb) => {
  if (req.query.type === 'image' && ACCEPTED_IMAGE_MIME_TYPES.indexOf(file.mimetype) > -1) {
    cb(null, true)
  }
  cb(null, true)
  // else if (!req.query.type && ACCEPTED_MIME_TYPES.indexOf(file.mimetype) > -1) {
  // }
  // else {
  //   cb(`Invalid file type - ${file.mimetype}`, false)
  // }
}
