const R = require('ramda')
const models = require('../models')
const ResponseBuilder = require('../helper/response-builder')
const GMUtils = require('../helper/gmUtil')
const pamphletService = require('../services/pamphlet')

const validatePamphlet = (pamphlet) => {
  const errors = []
  if (!pamphlet.type) {
    errors.push('Pamphlet type is mandatory')
  }
  else {
    switch (pamphlet.type) {
      case 'type1':
        if (!pamphlet.title) errors.push('Title is mandatory')
        if (!pamphlet.description) errors.push('Description is mandatory')
        if (pamphlet.description && pamphlet.description.length > 200) {
          errors.push('Description should not exceed 200 characters')
        }
        if (!pamphlet.name) errors.push('Name is mandatory')
        if (!pamphlet.address) errors.push('Address is mandatory')
        if (!pamphlet.brandingText) errors.push('Branding text is mandatory')
        if (!pamphlet.mobile) errors.push('Contact phone is mandatory')
        if (!pamphlet.subjects) errors.push('Subjects are mandatory')
        if (!pamphlet.classes) errors.push('Classes are mandatory')
        if (!pamphlet.boards) errors.push('Boards are mandatory')
        break
      case 'type2':
        if (!pamphlet.title) errors.push('Title is mandatory')
        if (!pamphlet.name) errors.push('Name is mandatory')
        if (!pamphlet.address) errors.push('Address is mandatory')
        if (!pamphlet.brandingText) errors.push('Branding text is mandatory')
        if (!pamphlet.mobile) errors.push('Contact phone is mandatory')
        break
      case 'type3':
        if (!pamphlet.title) errors.push('Title is mandatory')
        if (!pamphlet.description) errors.push('Description is mandatory')
        if (pamphlet.description && pamphlet.description.length > 200) {
          errors.push('Description should not exceed 200 characters')
        }
        if (!pamphlet.address) errors.push('Address is mandatory')
        if (!pamphlet.mobile) errors.push('Contact phone is mandatory')
        if (!pamphlet.subjects) errors.push('Subjects are mandatory')
        if (!pamphlet.classes) errors.push('Classes are mandatory')
        break
      default:
        errors.push(`Invalid pamphlet type provided - ${pamphlet.type}`)
    }
  }

  return errors
}

exports.getPamphletTemplates = (req, res, next) =>
  pamphletService.getTemplatePamphlets()
  .then(templates => res.status(200).json(new ResponseBuilder()
    .setData(templates)
    .setStatus(200)
    .build()
  ))

exports.getUserPamphlets = (req, res, next) => {
  const userId = req.params.id

  return pamphletService.getUserPamphlets(userId)
  .then(pamphlets => res.status(200).json(new ResponseBuilder()
    .setData(pamphlets)
    .setStatus(200)
    .build()
  ))
}

exports.getUserPamphlet = (req, res, next) => {
  const userId = req.params.userId
  const id = req.params.id

  return pamphletService.getUserPamphlet(userId, id)
  .then(pamphlet => res.status(200).json(new ResponseBuilder()
    .setData(pamphlet)
    .setStatus(200)
    .build()
  ))
}

exports.createPamphlet = (req, res, next) => {
  const userId = req.params.id

  const pamphlet = {
    type: req.body.type,
    name: req.body.name,
    title: req.body.title,
    description: req.body.description,
    logo: req.body.logo,
    address: req.body.address,
    email: req.body.email,
    mobile: req.body.mobile,
    watermark: req.body.watermark,
    brandingText: req.body.brandingText,
    userId,
    subjects: req.body.subjects,
    classes: req.body.classes,
    boards: req.body.boards,
    createdOn: new Date(),
  }

  // validations
  const errors = validatePamphlet(pamphlet)

  if (errors && errors.length) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('The information provided is invalid.')
      .setErrors(errors)
      .build()
    )
  }

  return models.Pamphlets.create(pamphlet)
  .then(savedPamphlet => res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('The pamphlet was successfully created.')
      .setData(savedPamphlet)
      .build()
    )
  )
  .catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('An unwanted exception occurred. Please try again later.')
      .setStatus(500)
      .build()
    )
  })
}

exports.updatePamphlet = (req, res, next) => {
  const userId = req.params.id
  const id = req.params.pamphletId

  const pamphlet = {
    type: req.body.type,
    name: req.body.name,
    title: req.body.title,
    description: req.body.description,
    logo: req.body.logo,
    address: req.body.address,
    email: req.body.email,
    mobile: req.body.mobile,
    watermark: req.body.watermark,
    brandingText: req.body.brandingText,
    subjects: req.body.subjects,
    classes: req.body.classes,
    boards: req.body.boards,
    updatedOn: new Date(),
  }

  // validations
  const errors = validatePamphlet(pamphlet)

  if (errors && errors.length) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('The information provided is invalid.')
      .setErrors(errors)
      .build()
    )
  }

  return pamphletService.getUserPamphlet(userId, id)
  .then(savedPamphlet => {
    if (!savedPamphlet) {
      return null
    }
    savedPamphlet.type = pamphlet.type
    savedPamphlet.name = pamphlet.name
    savedPamphlet.title = pamphlet.title
    savedPamphlet.description = pamphlet.description
    savedPamphlet.logo = pamphlet.logo
    savedPamphlet.address = pamphlet.address
    savedPamphlet.email = pamphlet.email
    savedPamphlet.mobile = pamphlet.mobile
    savedPamphlet.watermark = pamphlet.watermark
    savedPamphlet.brandingText = pamphlet.brandingText
    savedPamphlet.subjects = pamphlet.subjects
    savedPamphlet.classes = pamphlet.classes
    savedPamphlet.boards = pamphlet.boards
    savedPamphlet.updatedOn = pamphlet.updatedOn

    return savedPamphlet.save()
  })
  .then(savedPamphlet => {
    if (!savedPamphlet) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Pamphlet not found.')
        .build()
      )
    }

    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(savedPamphlet)
      .setMessage('The pamphlet has been successfully updated.')
      .build()
    )
  })
  .catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('An unwanted exception occurred. Please try again later.')
      .setStatus(500)
      .build()
    )
  })
}

exports.deletePamphlets = (req, res, next) =>
  pamphletService.deletePamphlet(req.params.userId, req.params.id)
  .then(() => res.status(200).json(new ResponseBuilder()
    .setMessage('The pamphlet was successfully deleted.')
    .setStatus(200)
    .build()
  ))

exports.developPamphlet = (req, res, next) => {
  try {
    pamphletService.getPamphletById(req.params.id)
    .then(pamphlet => {
      if (!pamphlet) {
        return res.status(404).json(new ResponseBuilder()
          .setMessage('Failed to find the pamphlet.')
          .setStatus(404)
          .build()
        )
      }

      GMUtils.createPamphlet(pamphlet || {}, (err, newPamphlet) => {
        if (err) {
          console.log(err)
          return res.status(500).json(new ResponseBuilder()
            .setMessage('Failed to generate a pamphlet')
            .setStatus(500)
            .build()
          )
        }

        return pamphletService.getPamphletById(req.params.id).then(freshPamphlets => res.status(200).json(new ResponseBuilder()
          .setMessage('The pamphlet was successfully generated.')
          .setData(newPamphlet)
          .setStatus(200)
          .build()
        ))
      })
    })
  } catch(err) {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setMessage('Failed to find the pamphlet.')
      .setStatus(500)
      .build()
    )
  }
}
