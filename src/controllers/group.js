const studentService = require('../services').StudentService
const groupService = require('../services').GroupService
const Q = require('q')
const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const R = require('ramda')

const GROUP_BIRTHDAY_QUERY =
`select count(1) as bdayCount from
(
  select date_of_birth from student_group_map sgm
  inner join user s
  on s.id = sgm.student_id
  where sgm.group_id = #GROUP_ID
) as T
where DATE_FORMAT(date_of_birth,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d')`

// validation
const valdiateGroupRequest = (req) => {
  req.assert('name', 'The name of the group is mandatory.').notEmpty()
  req.assert('pupilInfo.billingCycle', 'Billing cycle is mandatory.').notEmpty()
  req.assert('tutorId', 'Tutor Id is mandatory.').notEmpty()

  return req.validationErrors()
}

const createStudentGroupMap = (groupId, studentId) => {
  return models.StudentGroupMap.create({
    groupId: groupId,
    studentId: studentId,
  })
}

const getGroupById = (id) => {
  const deferred = Q.defer()
  models.StudentGroups.findOne({
    where: {
      id,
    },
    include: [
      {
        model: models.PupilInfo,
        as: 'pupilInfo',
      },
    ],
  }).then((group) => {
    if (!group) {
      deferred.resolve(group)
    }
    else {
      group.getStudents().then((students) => {
        group.students = students
        deferred.resolve(group)
      })
    }
  })
  return deferred.promise
}

exports.getGroupsByTutorId = (req, res, next) => {
  models.StudentGroups.findAll({
    where: {
      tutorId: req.params.id,
    },
  }).then((groups) => {
    const promises = []
    R.forEach((group) => {
      promises.push(models.sequelize.query(GROUP_BIRTHDAY_QUERY.replace('#GROUP_ID', group.id)))
    }, groups)

    models.sequelize.Promise.all(promises)
    .spread(function() {
      const birthdayCounts = []
      if (arguments && arguments.length) {
        for (let i = 0; i < arguments.length; i++) {
          groups[i].isBdayToday = arguments[i][0][0].bdayCount > 0
        }
      }

      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData(groups)
        .build()
      )
    })
  })
}

exports.fetch = (req, res, next) => {
  getGroupById(req.params.id).then((group) => {
    if (!group) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Invalid group ID supplied')
        .build()
      )
    } else {
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setData(group)
        .build()
      )
    }
  }, (errors) => {
    console.log(errors)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to find group.')
      .build()
    )
  })
}

exports.addStudentToGroup = (req, res, next) => {
  if (!req.body.tutorId || !req.body.students || !req.body.students.length) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Tutor Id and Students must be specified.')
      .build()
    )
  }

  groupService.validateStudents(req.body.students)
  .then(validateTutors => {
    if (validateTutors) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(validateTutors)
        .build()
      )
    }

    const phoneNumbers = R.pluck('phone')(req.body.students)
    const groupId = req.params.id
    const tutorId = req.body.tutorId

    models.sequelize.Promise.all([
      models.User.findAll({
        where: {
          phone: {
            in: phoneNumbers,
          },
        },
      }),
      models.User.findOne({
        where: {
          id: tutorId,
        }
      })
    ])
    .spread((users, tutor) => {
      const toBeRegistered = R.differenceWith((x, y) => x == y.phone, phoneNumbers, users)
      const promises = []
      R.forEach((phone) => {
        promises.push(studentService.createStudentInfo({ phone }, null, tutor, 1))
      }, toBeRegistered)

      models.sequelize.Promise.all(promises)
      .spread(function() {
        const allUsers = []
        R.forEach((curUser) => {
          allUsers.push(curUser)
        }, users)
        if (arguments && arguments.length) {
          for (var i = 0; i < arguments.length; i++) {
            allUsers.push(arguments[i])
          }
        }

        const promises = []
        // create a map from phone to id
        R.forEach((student) => {
          if (student.id) {
            promises.push(createStudentGroupMap(groupId, student.id))
          }
        }, allUsers)

        models.sequelize.Promise.all(promises)
        .spread(() => {
          return res.status(204).json(new ResponseBuilder()
            .setStatus(204)
            .setMessage('Student successfully added to group.')
            .build()
          )
        }).catch((error) => {
          if (error && (error instanceof models.sequelize.ForeignKeyConstraintError)) {
            return res.status(404).json(new ResponseBuilder()
              .setStatus(404)
              .setMessage('Invalid group ID specified.')
              .build()
            )
          }
          return res.status(500).json(new ResponseBuilder()
            .setStatus(500)
            .setMessage('Failed to add student to group.')
            .build()
          )
        })
      }).catch((error) => {
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to create a student.')
          .build()
        )
      })
    })    
  })
}

exports.removeStudentToGroup = (req, res, next) => {
  const groupId = req.params.id
  const studentId = req.params.studentId

  const promises = []
  promises.push(models.StudentGroups.findById(groupId))
  promises.push(models.User.findById(studentId))

  models.sequelize.Promise.all(promises).spread((group, student) => {
    if (!group) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Invalid group ID.')
        .build()
      )
    }
    if (!student) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Invalid student ID.')
        .build()
      )
    }

    if (group.tutorId
      && (group.tutorId === student.inviterId)
    ) {
      student.isGroupInvite = 0;
      student.save()
    }

    models.GroupInvite.destroy({
      where:{
        groupId,
        studentId,
      }
    }).then(() => {
      console.log('Group invite successfully deleted.')
    })

    models.StudentGroupMap.destroy({
      where: {
        groupId,
        studentId,
      }
    }).then(() => {
      return res.status(204).json(new ResponseBuilder()
        .setStatus(204)
        .setMessage('Student successfully removed from group.')
        .build()
      )
    }, (error) => {
      console.log(error)
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Failed to remove student to group.')
        .build()
      )
    })
  })
}

exports.remove = (req, res, next) => {
  const groupId = req.params.id
  const tutorId = req.params.tutorId

  // TODO: Validate that the group belongs to the logged in user
  groupService.removeGroup(groupId, tutorId)
  .then(x => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('The group has been deleted.')
      .build()
    )
  })
}

exports.create = (req, res, next) => {
  const tutorId = req.body.tutorId
  const errors = valdiateGroupRequest(req)
  const students = req.body.students

  if (errors) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid create group request object.')
      .setErrors(errors)
      .build()
    )
  }

  if (students && students.length) {
    const error = studentService.validateNewInvites(R.pluck('phone')(students),
      null,
      'Two students may not have the same phone number.'
    )
    if (error) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors([error])
        .build()
      )
    }
  }

  studentService.filterIndividualStudents(students, tutorId)
  .then(individualStudents => {
    if (individualStudents && individualStudents.length) {
      res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors([`Students already added - ${R.join(', ', R.pluck('phone')(individualStudents))}.`])
        .build()
      )
      return false
    }
    return true
  })
  .then(isValid => {
    if (isValid) {
      return studentService.isGroupStudentAlready(students, tutorId).then(err => {
        if (err && err.length) {
          res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Some students are already registered with us as group students - ${R.join(', ', err)}`)
            .build()
          )
          return false
        }
        return true
      })
    }
    return isValid
  })
  .then(isValid => {
    if (isValid) {
      return groupService.validateStudents(students).then(err => {
        if (err) {
          res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setErrors([err])
            .build()
          )
          return false
        }
        return true
      })
    }
    return false
  })
  .then(isValid => {
    if (!isValid) {
      return null
    }

    const pupilInfoProps = {
      tutorId,
      tuitionType: 'group',
      billingCycle: req.body.pupilInfo.billingCycle,
      fee: req.body.pupilInfo.fee,
      classDurationHours: req.body.pupilInfo.classDurationHours,
      classDurationMins: req.body.pupilInfo.classDurationMins,
    }

    models.PupilInfo.create(pupilInfoProps).then((pupil) => {
      const groupProps = {
        name: req.body.name,
        tutorId,
        pupilInfoId: pupil.id,
      }
      models.StudentGroups.create(groupProps).then((group) => {
        // add students to the group
        if (students && students.length) {
          const phoneNumbers = R.pluck('phone')(req.body.students)
          const promises = []

          models.sequelize.Promise.all([
            models.User.findAll({
              where: {
                phone: {
                  $in: phoneNumbers,
                },
              },
            }),
            models.User.findOne({
              where: {
                id: req.body.tutorId,
              },
            }),
          ]).spread((users, tutor) => {
            const toBeRegistered = R.differenceWith((x, y) => x == y.phone, phoneNumbers, users)
            const promises = []
            R.forEach((phone) => {
              promises.push(studentService.createStudentInfo({ phone }, null, tutor))
            }, toBeRegistered)

            models.sequelize.Promise.all(promises)
            .spread(function() {
              const allUsers = []
              R.forEach((curUser) => {
                allUsers.push(curUser)
              }, users)
              if (arguments && arguments.length) {
                for (var i = 0; i < arguments.length; i++) {
                  allUsers.push(arguments[i])
                }
              }

              R.forEach((student) => {
                if (student.id) {
                  promises.push(createStudentGroupMap(group.id, student.id))
                  promises.push(groupService.createGroupInvite(student.id, tutor.id, group.id))
                }
              }, allUsers)

              models.sequelize.Promise.all(promises)
              .spread(() => {
                getGroupById(group.id).then((groupObj) => {
                  return res.status(200).json(new ResponseBuilder()
                    .setStatus(200)
                    .setData(groupObj)
                    .build()
                  )
                })
              })
            })
          })
        } else {
          getGroupById(group.id).then((groupObj) => {
            return res.status(200).json(new ResponseBuilder()
              .setStatus(200)
              .setData(groupObj)
              .build()
            )
          })
        }
      }, (error) => {
        if (error instanceof models.sequelize.UniqueConstraintError) {
          return res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Group with name '${req.body.name}' already exists.`)
            .build()
          )
        }
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to create student group.')
          .build()
        )
      })
    }, errors => {
      console.log(errors)
      return res.status(500).json(new ResponseBuilder()
        .setStatus(500)
        .setMessage('Failed to create pupil info for student group.')
        .build()
      )
    })
  }).catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to create pupil info for student group.')
      .build()
    )
  })
}

const removeStudentFromGroup = (studentId, groupId) => {
  const promises = []
  promises.push(models.StudentGroups.findById(groupId))
  promises.push(models.User.findById(studentId))

  models.sequelize.Promise.all(promises).spread((group, student) => {
    if (!group) {
      return
    }
    if (!student) {
      return
    }

    if (group.tutorId
      && (group.tutorId === student.inviterId)
    ) {
      student.isGroupInvite = 0;
      student.save()
    }

    models.GroupInvite.destroy({
      where:{
        groupId,
        studentId,
      }
    }).then(() => {
      console.log('Group invite successfully deleted.')
    })

    models.StudentGroupMap.destroy({
      where: {
        groupId,
        studentId,
      }
    }).then(() => {
      return
    }, (error) => {
      console.log(error)
      return
    })
  })
}

exports.updateGroup = (req, res, next) => {
  const groupId = req.params.id
  const tutorId = req.body.tutorId
  const students = req.body.students

  let tutorObj = null;

  if (req.body.students && req.body.students.length) {
    const error = studentService.validateNewInvites(R.pluck('phone')(req.body.students), null, 'Two students may not have the same phone number.')
    if (error) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors([error])
        .build()
      )
    }
  }

  studentService.filterIndividualStudents(students, tutorId)
  .then(individualStudents => {
    if (individualStudents && individualStudents.length) {
      res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setErrors([`Students already added - ${R.join(', ', R.pluck('phone')(individualStudents))}.`])
        .build()
      )
      return false
    }
    return true
  })
  .then(isValid => {
    if (isValid) {
      return studentService.areOtherGroupStudentsAlready(students, tutorId, groupId).then(err => {
        if (err && err.length) {
          res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Students with phone number/numbers already exist - ${R.join(', ', err)}`)
            .build()
          )
          return false
        }
        return true
      })
    }
    return isValid
  })
  .then(isValid => {
    if (isValid) {
      return groupService.validateStudents(students).then(err => {
        if (err) {
          res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setErrors([err])
            .build()
          )
          return false
        }
        return true
      })
    } else {
      return false
    }
  })
  .then(isValid => {
    if(!isValid) {
      return null
    }

    getGroupById(groupId).then((group) => {
      // update group
      const nextProps = {
        name: req.body.name || group.name
      }
      return group.updateAttributes(nextProps).then((updatedGroup) => {
        return models.sequelize.Promise.all([
          updatedGroup,
          models.User.findOne({
            where: {
              id: group.tutorId
            }
          })
        ])
      }, (error) => {
        if (error instanceof models.sequelize.UniqueConstraintError) {
          return res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Group with name '${req.body.name}' already exists.`)
            .build()
          )
        } else {
          return res.status(500).json(new ResponseBuilder()
            .setStatus(500)
            .setMessage('Failed to create student group.')
            .build()
          )
        }
      })
    }).spread((group, tutor) => {
      tutorObj = tutor;
      const pupilInfo = req.body.pupilInfo
      return new Promise((resolve) => {
        if (pupilInfo) {
          const nextProps = {
            billingCycle: pupilInfo.billingCycle || group.pupilInfo.billingCycle,
            fee: pupilInfo.fee || group.pupilInfo.fee,
            classDurationHours: !R.isNil(pupilInfo.classDurationHours) ? pupilInfo.classDurationHours : group.pupilInfo.classDurationHours,
            classDurationMins: !R.isNil(pupilInfo.classDurationMins) ? pupilInfo.classDurationMins : group.pupilInfo.classDurationMins,
          }
          group.pupilInfo.updateAttributes(nextProps).then(updatedPupilInfo => {
            group.pupilInfo = updatedPupilInfo
            resolve(group)
          })
        } else {
          resolve(group)
        }
      })
    }).then((group) => {      
      const tutor = tutorObj;
      // if (req.body.students && req.body.students.length) {
        const phoneNumbers = R.pluck('phone')(req.body.students)
        const promises = []

        models.User.findAll({
          where: {
            phone: {
              $in: phoneNumbers,
            },
          },
        }).then((users) => {
          const toBeRegistered = R.differenceWith((x, y) => x == y.phone, phoneNumbers, users)
          const toBeRemoved = R.differenceWith((x, y) => x.phone == y, group.students || [], phoneNumbers || [])

          const promises = []
          if (toBeRegistered && toBeRegistered.length) {
            R.forEach((phone) => {
              promises.push(studentService.createStudentInfo({ phone }, null, tutor, 1))
            }, toBeRegistered)
          }

          models.sequelize.Promise.all(promises)
          .spread(function() {
            const allUsers = []
            R.forEach((curUser) => {
              allUsers.push(curUser)
            }, users)
            if (arguments && arguments.length) {
              for (var i = 0; i < arguments.length; i++) {
                allUsers.push(arguments[i])
              }
            }

            if (allUsers && allUsers.length) {
              R.forEach((student) => {
                if (student.id) {
                  promises.push(groupService.createGroupInvite(student.id, tutor.id, group.id))
                }
              }, allUsers)
            }

            const toBeInserted = R.differenceWith((x, y) => x == y.phone, allUsers, group.students)

            if (toBeInserted && toBeInserted.length) {
              R.forEach((student) => {
                if (student.id) {
                  promises.push(createStudentGroupMap(group.id, student.id))
                }
              }, toBeInserted)
            }

            if (toBeRemoved && toBeRemoved.length) {
              R.forEach(studentObj => {
                try {
                  removeStudentFromGroup(studentObj.id, groupId)
                } catch (err) {
                  console.log(err)
                }
              }, toBeRemoved)
              // promises.push(models.StudentGroupMap.destroy({
              //   where: {
              //     studentId: {
              //       $in: R.pluck('id')(toBeRemoved),
              //     },
              //     groupId,
              //   },
              // }))
            }

            models.sequelize.Promise.all(promises)
            .spread(() => {
              getGroupById(group.id).then((groupObj) => {
                return res.status(200).json(new ResponseBuilder()
                  .setStatus(200)
                  .setData(groupObj)
                  .build()
                )
              })
            })
          })
        })
      // } else {
      //   getGroupById(groupId).then((groupObj) => {
      //     return res.status(200).json(new ResponseBuilder()
      //       .setStatus(200)
      //       .setData(groupObj)
      //       .build()
      //     )
      //   })
      // }
    }).catch((errors) => {
      console.log(errors)
      if (error instanceof models.sequelize.UniqueConstraintError) {
        return res.status(400).json(new ResponseBuilder()
          .setStatus(400)
          .setMessage(`Group with name '${req.body.name}' already exists.`)
          .build()
        )
      } else {
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to update student group.')
          .build()
        )
      }
    })
  })
}
