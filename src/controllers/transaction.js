const R = require('ramda')
const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const mailService = require('../helper/mailer')
const transactionService = require('../services').TransactionService
const userService = require('../services').UserService
const smsService = require('../services').SMSService
const moment = require('moment')

const DATE_FORMAT = 'MM-DD-YYYY'

const REMINDER_TYPES = ['detailed', 'brief']
const REMINDER_MODES = ['sms', 'email']

const transactionTypes = ['request', 'receive']

const getMoney = money => Math.round(money * 100) / 100

const getOutstandingAmount = transactions => {
  const transactionsJSONArray = R.map(transaction => transaction.toJSON(), transactions)
  const transactionsArray = R.reject(transaction => transaction.type === 'reminder', transactionsJSONArray)
  const groupedTransactions = R.groupBy(transaction => transaction.type, transactionsArray)
  const requestTransactions = R.pluck('amount')(groupedTransactions.request || [])
  const receiveTransactions = R.pluck('amount')(groupedTransactions.receive || [])
  const outstandingAmount = R.sum(requestTransactions) - R.sum(receiveTransactions)
  return getMoney(outstandingAmount)
}

const validateModes = modes => {
  if (modes && modes.length) {
    let isValid = true
    R.forEach(mode => {
      if(!R.contains(mode, REMINDER_MODES)) {
        isValid = false
      }
    }, modes)
    if (!isValid) {
      return `Invalid mode type. Use one of - ${R.join(', ', REMINDER_MODES)}`
    }
  }
  else {
    return 'Modes is mandatory.'
  }
}

const validatePositiveInt = (data) => {
  if(/^\+?(0|[1-9]\d*)$/.test(data)) {
    return true
  } else {
    return false
  }
}

const validateTransactionType = type => {
  if (!R.contains(type, transactionTypes)) {
    return 'Invalid transaction type'
  }
  return null
}

const validateAmount = amount => {
  try {
    if (!R.gt(amount, 0)) {
      return 'Amount should be postive'
    }
    return null
  } catch(err) {
    console.log(err)
    return 'Invalid amount supplied'
  }
}

const validateUser = user => {
  return models.User.findById(user)
  .then(user => {
    if (user) {
      return null
    } else {
      return 'Invalid tutor ID supplied.'
    }
  })
}

const validateTransaction = transaction => {
  const errors = []
  const typeError = validateTransactionType(transaction.type)
  const amountError = validateAmount(transaction.amount)
  const promises = []

  if (typeError) {
    errors.push(typeError)
  }
  if (amountError) {
    errors.push(amountError)
  }

  if (transaction.type === 'request') {
    if (!transaction.dateTo) {
      errors.push('To date is mandatory')
    } else if (!moment(transaction.dateTo, DATE_FORMAT, true).isValid()) {
      errors.push('Invalid format for to date.')
    }
    if (!transaction.dateFrom) {
      errors.push('From date is mandatory')
    } else if (!moment(transaction.dateFrom, DATE_FORMAT, true).isValid()) {
      errors.push('Invalid format for from date.')
    }
    if (R.isNil(transaction.suggestedSessions)) {
      errors.push('Suggested sessions are mandatory')
    } else if (!validatePositiveInt(transaction.suggestedSessions)) {
      errors.push('Suggested sessions must be a positive number.')
    }
    if (R.isNil(transaction.suggestedHours)) {
      errors.push('Suggested hours are mandatory')
    } else if (!validatePositiveInt(transaction.suggestedHours)) {
      errors.push('Suggested hours must be a positive number.')
    }
  }

  promises.push(validateUser(transaction.tutorId))
  promises.push(validateUser(transaction.studentId))

  return models.sequelize.Promise.all(promises).spread((tutorError, studentError) => {
    if (tutorError) {
      errors.push(tutorError)
    }
    if (studentError) {
      errors.push(studentError)
    }

    return errors
  })
}

exports.createTransaction = (req, res, next) => {
  const tutorId = parseInt(req.params.tutorId)
  const studentId = parseInt(req.params.studentId)
  const notifyStudent = req.body.notifyStudent

  const transaction = {
    type: req.body.type,
    amount: req.body.amount,
    tutorId,
    studentId,
    createdOn: new Date(),
  }

  if (transaction.type === 'request') {
    transaction.dateTo = req.body.dateTo
    transaction.dateFrom = req.body.dateFrom
    transaction.suggestedHours = req.body.suggestedHours
    transaction.suggestedSessions = req.body.suggestedSessions
  }

  validateTransaction(transaction).then(errors => {
    if (!R.isEmpty(errors)) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(R.join(', ')(errors))
        .build()
      )
    } else {
      return true
    }
  }).then(isValidTransaction => {
    if (isValidTransaction) {
      transactionService.createTransaction(transaction)
      .then(persistedTransaction => {
        return models.sequelize.Promise.all([
          models.User.findById(tutorId),
          models.User.findById(studentId)
        ]).spread((tutorObj, studentObj) => {
          if (notifyStudent === true) {
            smsService.smsSendTransactionReminder(tutorObj, studentObj, transaction.amount, transaction.type)
          }
          return res.status(200).json(new ResponseBuilder()
            .setStatus(200)
            .setData(persistedTransaction)
            .build()
          )
        })
      }, err => {
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to persist transaction.')
          .build()
        )
      }).catch(err => {
        console.log(err)
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to persist transaction.')
          .build()
        )
      })
    }
  })
}

exports.updateTransaction = (req, res) => {
  const id = req.params.id
  const tutorId = parseInt(req.params.tutorId)
  const studentId = parseInt(req.params.studentId)
  let transactionObj

  const transaction = {
    type: req.body.type,
    amount: req.body.amount,
    tutorId,
    studentId
  }

  if (transaction.type === 'request') {
    transaction.dateTo = req.body.dateTo
    transaction.dateFrom = req.body.dateFrom
    transaction.suggestedHours = req.body.suggestedHours
    transaction.suggestedSessions = req.body.suggestedSessions
  }

  transactionService.fetchTransaction(id).then(transaction => {
    if (!transaction) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage('Invalid transaction id.')
        .build()
      )
    } else {
      transactionObj = transaction
      return true
    }
  }).then(isValid => {
    if (isValid) {
      return validateTransaction(transaction)
    } else {
      return []
    }
  }).then(errors => {
    if (!R.isEmpty(errors)) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage(R.join(', ')(errors))
        .build()
      )
    } else {
      return true
    }
  }).then(isValidTransaction => {
    if (isValidTransaction) {
      transactionService.updateTransaction(id, transaction)
      .then(persistedTransaction => {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .setData(persistedTransaction)
          .build()
        )
      }).catch(err => {
        console.log(err)
        return res.status(500).json(new ResponseBuilder()
          .setStatus(500)
          .setMessage('Failed to persist transaction.')
          .build()
        )
      })
    }
  })
}

exports.deleteTransaction = (req, res, next) => {
  const id = req.params.id
  transactionService.deleteTransaction(id)
  .then(persistedTransaction => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('The transaction has been successfully deleted.')
      .build()
    )
  }).catch(err => {
    console.log(err)
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Failed to delete transaction.')
      .build()
    )
  })
}

exports.fetchTransaction = (req, res, next) => {
  const id = req.params.id
  transactionService.fetchTransaction(id)
  .then(transaction => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData(transaction)
      .build()
    )
  })
}

exports.fetchStudentTransactions = (req, res, next) => {
  const studentId = req.params.studentId
  const tutorId = req.params.tutorId

  transactionService.fetchStudentTransactions(tutorId, studentId)
  .then(transactions => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setData({
        outstanding: getOutstandingAmount(transactions),
        transactions
      })
      .build()
    )
  })
}

exports.sendReminder = (req, res, next) => {
  const tutorId = req.params.tutorId
  const studentId = req.params.studentId

  const type = req.body.type
  const modes = req.body.modes
  const notificationType = req.body.notificationType || 'reminder'
  const transactionId = req.body.transactionId

  if (notificationType != 'reminder' && !transactionId) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Something went wrong. Please try again.')
      .setErrors(['Transaction ID must be provided with notification type reminder.'])
      .build()
    )
  }

  if(!R.contains(type, REMINDER_TYPES)) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage(`Invalid reminder type. Use one of - ${R.join(', ', REMINDER_TYPES)}`)
      .build()
    )
  }

  const modesError = validateModes(modes)
  if(modesError) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage(modesError)
      .build()
    )
  }

  const promises = []
  promises.push(userService.getUser(studentId))
  promises.push(userService.getUser(tutorId))

  let studentObj
  let tutorObj

  models.sequelize.Promise.all(promises).spread((student, tutor) => {
    if (!student) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage(`Invalid student ID supplied.`)
        .build()
      )
    } else if (!tutor) {
      return res.status(404).json(new ResponseBuilder()
        .setStatus(404)
        .setMessage(`Invalid tutor ID supplied.`)
        .build()
      )
    } else {
      tutorObj = tutor
      studentObj = student
      return true
    }
  }).then(studentFound => {
    if (studentFound) {
      transactionService.fetchStudentTransactions(tutorId, studentId)
      .then(transactions => {
        let pendingAmount = getOutstandingAmount(transactions)
        if (notificationType === 'reminder' && pendingAmount > 0) {
          // create a transaction
          transactionService.createTransaction({
            type: 'reminder',
            amount: pendingAmount,
            tutorId,
            studentId,
            createdOn: new Date(),
          })
          .then(persistedTransaction => {
            if (persistedTransaction) {
              console.log('Transaction created.')
            }
          })
          R.forEach(mode => {
            if (mode === 'email') {
              const message = transactionService.getTransactionMessage(notificationType, tutorObj, pendingAmount)
              mailService.sendMail(studentObj.email, `The Faqulty Club: Payment Reminder`, message)
            } else if (mode === 'sms') {
              smsService.smsSendTransactionReminder(tutorObj, studentObj, pendingAmount, notificationType)
            }
          }, modes)
          return res.status(200).json(new ResponseBuilder()
            .setStatus(200)
            .setMessage(`Reminder for amount Rs. ${pendingAmount} has been sent to the student.`)
            .build()
          )
        } else if (notificationType === 'request' || notificationType === 'receive') {
          transactionService.fetchTransaction(transactionId)
          .then(transaction => {
            if (!transaction) {
              return res.status(400).json(new ResponseBuilder()
                .setStatus(400)
                .setMessage(`Something went wrong. Please try again later.`)
                .setErrors(['Invalid transaction ID.'])
                .build()
              )
            }
            R.forEach(mode => {
              if (mode === 'email') {
                const message = transactionService.getTransactionMessage(notificationType, tutorObj, getMoney(transaction.amount))
                mailService.sendMail(studentObj.email, `The Faqulty Club: Payment`, message)
              } else if (mode === 'sms') {
                smsService.smsSendTransactionReminder(tutorObj, studentObj, pendingAmount, notificationType)
              }
            }, modes)
            return res.status(200).json(new ResponseBuilder()
              .setStatus(200)
              .setMessage(`Message sent successfully.`)
              .build()
            )
          })
        } else {
          return res.status(400).json(new ResponseBuilder()
            .setStatus(400)
            .setMessage(`Pending amount is Rs. ${pendingAmount}. No reminder sent as the notification type is invalid.`)
            .build()
          )
        }
      })
    }  
  })
}
