const models = require('../models')
const logger = require('../config/logger').logger
const errorBuilder = require('../helper/error-decorator')
const ResponseBuilder = require('../helper/response-builder')
const notificationService = require('../services/notification')
const mailService = require('../helper/mailer')
const userService = require('../services/user')
const pushService = require('../services/push')

const findOrCreateTutorUserLike = (req, res) => {
  if (!req.body.entityId) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing entityId parameter')
      .setStatus(400)
      .build()
    )
  }
  
  const likeObj = {
    userId: req.body.userId,
    tutorId: req.body.entityId,
    userIp: req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress,
    createdOn: new Date()
  }

  models.TutorUserLikes.findOne({
    where: {
      userId: likeObj.userId,
      tutorId: likeObj.tutorId,
    }
  }).then(function(result) {
    if (result) {
      return res.status(200).json(new ResponseBuilder()
        .setMessage('OK')
        .setStatus(200)
        .build()
      )
    }
    else {
      const create = () => models.TutorUserLikes.create(likeObj)
      create().then(function() {
        // send push notification
        if (likeObj.userId !== likeObj.tutorId) {
          userService.getUser(likeObj.userId).then(actor => {
            if (actor) {
              pushService.profileLiked({ actor: actor.name }, likeObj.tutorId)
            }
          })
        }
        return res.status(200).json(new ResponseBuilder()
          .setMessage('OK')
          .setStatus(200)
          .build()
        )
      })
    }
  })
}

const findOrCreateFeedLike = (req, res) => {
  if (!req.body.entityId) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing entityId parameter')
      .setStatus(400)
      .build()
    )
  }
  
  const moduleId = req.body.entityId
  const userId = req.body.userId
  const moduleType = 'feed'
  const likeObj = {
    moduleId,
    userId,
    moduleType,
    createdOn: new Date(),
  }
  let feedObj
  let userWhoLiked

  models.Feed.scope('base').findOne({
    where: {
      id: moduleId,
    },
  })
  .then(feed => {
    feedObj = feed
    return feed
  })
  .then(() => {
    return userService.getUser(userId)
  })
  .then(user => {
    userWhoLiked = user
    return userWhoLiked
  })
  .then(() => {
    models.Likes.findOne({
      where: {
        userId,
        moduleId,
        moduleType,
      }
    }).then(function(result) {
      if (result) {
        return res.status(200).json(new ResponseBuilder()
          .setStatus(200)
          .build()
        )
      }
      else {
        models.Likes.create(likeObj)        
        .then(() => {
          if (feedObj.tutor.id !== userWhoLiked.id) {
            mailService.sendMail(feedObj.tutor.email, `The Faqulty Club: Like Notification`, `${userWhoLiked.name} has liked your post`)
          }
          return notificationService.createNotification(notificationService.TYPE_FEED_LIKE, {
            actor: feedObj.tutor.name,
            actorId: userId,
            feed: feedObj.description,
          }, feedObj.tutorId)
        })
        .then(notification => {
          // send push notification
          if (likeObj.userId !== feedObj.tutorId) {
            userService.getUser(likeObj.userId).then(actor => {
              if (actor) {
                pushService.feedLiked({ actor: actor.name, postTitle: feedObj.description }, feedObj.tutorId)
              }
            })
          }
          return res.status(200).json(new ResponseBuilder()
            .setStatus(200)
            .build()
          )
        })
      }
    })
  })
}

exports.postLikes = (req, res, next) => {
  switch(req.query.type) {
    case 'user':
      findOrCreateTutorUserLike(req, res)
      break
    case 'feed':
      findOrCreateFeedLike(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}

const deleteFeedLike = (req, res) => {
  const moduleId = req.body.entityId || req.query.entityId
  const userId = req.body.userId || req.query.userId
  const moduleType = 'feed'

  if (!moduleId) {
    return res.status(400).json(new ResponseBuilder()
      .setMessage('Missing entityId parameter')
      .setStatus(400)
      .build()
    )
  }

  const likeObj = {
    moduleId,
    userId,
    moduleType,
    createdOn: new Date(),
  }

  const whereClause = {
    userId,
    moduleId,
    moduleType,
  }

  models.Likes.findOne({
    where: whereClause,
  }).then(result => {
    models.Likes.destroy({
      where: whereClause,
    })
    // don't bother waiting for this, if it is there, it'll be deleted
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .build()
    )
  })
}

exports.deleteLikes = (req, res, next) => {
  switch(req.query.type) {
    case 'user':
      return res.status(400).json(new ResponseBuilder()
        .setMessage('User unlike is not supported yet.')
        .setStatus(400)
        .build()
      )
      break
    case 'feed':
      deleteFeedLike(req, res)
      break
    default:
      return res.status(400).json(new ResponseBuilder()
        .setMessage('Invalid or no type query parameter')
        .setStatus(400)
        .build()
      )
  }
}
