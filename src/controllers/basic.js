/**
 * GET /
 * Home page.
 */
const path = require('path')
const models = require('../models')
const ResponseBuilder = require('../helper/response-builder')
const mailService = require('../helper/mailer')
const encryptionService = require('../services/encryption')
const assignmentController = require('../controllers/assignment')
const tutorContentController = require('../controllers/upload')
const { mail: mailConfig, SERVER_URI } = require('../../config/application')

const FILE_STORE = path.join(__dirname, '../../uploads')

const MOST_FREQUENT_TAGS = 
`select count(1) as tag_count, tag from feed_tags group by tag having tag_count >= :min_occurrence order by tag_count desc limit :limit`

 const getTopHashTags = (minOccurrence, limit) => {
  return models.sequelize.query(
    MOST_FREQUENT_TAGS,
    {
      replacements: { min_occurrence: parseInt(minOccurrence), limit: parseInt(limit)  },
      model: models.FeedTags,
    }
  )
}

const validateContactAdmin = params => {
  const errors = []
  if (!params.name) {
    errors.push('Name is mandatory')
  }
  if (!params.about) {
    errors.push('About is mandatory')
  }
  return errors
}

exports.index = (req, res) => {
  models.User.scope('likes').findOne({
     where: {
       id: req.query.tutorId,
     }
  }).then(function(users) {
    res.send(users.get({
      plain: true,
    }).userLikes)
  })
};

exports.getClasses = (req, res) => {
  models.Class.findAll({
    order: 'sort_order ASC'
  })
  .then((classes) => {
    return res.send(classes)
  })
}

exports.getTags = (req, res) => {
  const minOccurrence = req.query.min_occur || 1
  const limit = req.query.limit || 20
  getTopHashTags(minOccurrence, limit).then(tags => {
    return res.send(tags)
  })
}

exports.getBoards = (req, res) => {
  models.TeachingBoard.findAll({
    order: [
      ['sort_order', 'DESC'],
      ['board', 'ASC'],
    ],
  })
  .then((boards) => {
    return res.send(boards)
  })
}

exports.getLanguages = (req, res) => {
  models.TeachingLanguage.findAll({
    order: [
      ['sort_order', 'DESC'],
      ['language', 'ASC'],
    ],
  })
  .then((languages) => {
    return res.send(languages)
  })
}

exports.postFCMToken = (req, res) => {
  const userId = req.body.userId
  const fcmToken = req.body.fcmToken
  const userType = req.body.userType

  if (!userId || !fcmToken) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('User ID and FCM token are mandatory.')
      .build()
    )
  }
  models.PushTokens.destroy({
    where: {
      userId,
    }
  }).then(() => {
    return models.PushTokens.create({
      userId,
      fcmToken,
      userType,
    })
  }).then(token => {
    return res.status(200).json(new ResponseBuilder()
      .setStatus(200)
      .setMessage('FCM token has been successfully persisted.')
      .build()
    )
  }).catch(err => {
    return res.status(500).json(new ResponseBuilder()
      .setStatus(500)
      .setMessage('Unexpected error occurred.')
      .build()
    )
  })
}

exports.contactAdmin = (req, res) => {
  const about = req.body.about
  const email = req.body.email
  const name = req.body.name
  const to = req.body.to
  const phoneNumber = req.body.phoneNumber

  const errors = validateContactAdmin({
    about,
    email,
    name,
    phoneNumber
  })
  if (errors && errors.length) {
    return res.status(400).json(new ResponseBuilder()
      .setStatus(400)
      .setMessage('Please make sure all mandatory information has been filled in.')
      .setErrors(errors)
      .build()
    )
  }
  mailService.sendMail(mailConfig.contactEmail,
    `The Faqulty Club: User Query`,
    `${name}${email ? '(email: ' + email + ')' : ''}${phoneNumber ? '(contact: ' + phoneNumber + ')' : ''} says\n${about}`)

  return res.status(200).json(new ResponseBuilder()
    .setStatus(200)
    .setMessage('The query has been posted successfully.')
    .build()
  )
}

exports.serveShared = (req, res) => {
  const context = req.params.context

  const decrypted = encryptionService.decrypt(context)
  const decryptedParts = decrypted.split(',')
  const type = decryptedParts[0]

  switch(type) {
    case 'assignments':
      const assignmentId = decryptedParts[1]
      const userId = decryptedParts[3]
      const subType = decryptedParts[2]
      assignmentController.egestAssignment(assignmentId, userId, subType, res)
      break
    case 'pamphlets':
      const contextParts = decryptedParts[1].split('file=')
      tutorContentController.egest(req, res, contextParts[1], FILE_STORE)
      break
  }
}

exports.share = (req, res) => {
  const userId = req.body.userId
  const moduleId = req.body.moduleId
  const moduleType = req.body.moduleType
  const subType = req.body.subType

  models.User.findById(userId).then(user => {
    if (!user) {
      return res.status(400).json(new ResponseBuilder()
        .setStatus(400)
        .setMessage('Invalid request. User ID is invalid.')
        .setErrors(errors)
        .build()
      )
    } else {
      let uri
      let relativeUri
      switch(moduleType) {
        case 'assignment':
          // relativeUri = encryptionService(`/api/assignments/${moduleId}/documents?type=${subType}`)
          relativeUri = encryptionService.encrypt(`assignments,${moduleId},${subType},${userId}`)
          uri = `${SERVER_URI}/api/shared-content/${relativeUri}`
          mailService.sendMail(user.email,
            `The Faqulty Club: Content`,
            `Here is the link to the content:\n${uri}`
          )
          break;
        case 'pamphlet':
          models.Pamphlets.findById(moduleId).then(pamphlet => {
            if (pamphlet && pamphlet.url) {
              relativeUri = encryptionService.encrypt(`pamphlets,${pamphlet.url},${userId}`)
              uri = `${SERVER_URI}/api/shared-content/${relativeUri}`
              // uri = `${SERVER_URI}/${pamphlet.url}`
              mailService.sendMail(user.email,
                `The Faqulty Club: Content`,
                `Here is the link to the content:\n${uri}`
              )
            }
          })
          break;
      }
      return res.status(200).json(new ResponseBuilder()
        .setStatus(200)
        .setMessage('The email has been queued.')
        .build()
      )
    }
  })
}

exports.configureResource = (epilogue, models) => {
  epilogue.resource({
    model: models.City,
    endpoints: ['/cities', '/cities/:id'],
  })

  epilogue.resource({
    model: models.Locality,
    endpoints: ['/localities', '/localities/:id'],
  })

  epilogue.resource({
    model: models.ClassSubject,
    endpoints: ['/subjects', '/subjects/:id'],
  })

  epilogue.resource({
    model: models.TeachingMode,
    endpoints: ['/modes', '/modes/:id'],
  })

  epilogue.resource({
    model: models.Schools,
    endpoints: ['/schools', '/schools/:id'],
  })

  epilogue.resource({
    model: models.TutorDegree,
    endpoints: ['/degrees', '/degrees/:id'],
  })

  epilogue.resource({
    model: models.TutorDegreeSubject,
    endpoints: ['/degree-subjects', '/degree-subjects/:id'],
  })

  epilogue.resource({
    model: models.TutorInstitute,
    endpoints: ['/institutes', '/institutes/:id'],
  })

  epilogue.resource({
    model: models.TutorInstituteAward,
    endpoints: ['/institue-awards', '/institue-awards/:id'],
  })
}
