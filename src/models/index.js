const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
// var env       = process.env.NODE_ENV || "development";
// var config    = require(path.join(__dirname, '..', 'config', 'config.json'))[env];

const testConnection = (conn) => {
	conn.authenticate()
	.then((err) => {
	  console.log('DB connection was successful')
	})
	.catch((err) => {
	  console.log('Unable to connect to the database:', err)
	  process.exit(1)
	})
}

// TODO: Use the config to get these values
// const sequelize = new Sequelize('test', 'root', 'root', {
//   host: 'ec2-54-201-39-154.us-west-2.compute.amazonaws.com',
const sequelize = new Sequelize('studypad', 'root', 'root', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 30,
    min: 10,
    idle: 10000,
  },
  define: {
    timestamps: false,
  },
  timezone: '+05:30',
  logging: false,
})
testConnection(sequelize)

const db = {
	scopes: {},
	filters: {},
	includes: {},
}

const modelListing = ["FileMeta.js", "Schools", "City.js", "Locality.js", "SubCityLocality.js", "TutorLocation.js", "Class.js", "ClassSubject.js",
  "GroupTutionCharge.js", "OneOnOneTutionCharge.js", "TeachingBoard.js", "TeachingLanguage.js", "TeachingMode.js",
  "TutorAwardsRecognition.js", "TutorBoard.js", "TutorCourseDetail.js", "TutorDegree.js", "TutorDegreeSubject.js",
  "TutorInstitute.js", "TutorInstituteAward.js", "TutorEducationDetail.js", "TutorSchool.js", "TutorTeachingLanguage.js",
  "TutorTeachingMode.js", "TutorUserLikes.js", "TutorUserReviews.js", "UserOTP.js", "User.js", "WeekdayTutionTime.js",
  "WeekendTutionTime.js", "StudentTeacherMap", "StudentGroups", "StudentGroupMap", "PupilInfo", "Session", "SessionStudent",
  "GroupInvite", "Invoices", "Payments", "Balance", "TutorContentUpload", "Subjects", "Chapters", "Answer", "Question",
  "Assignments", "AssignmentViews", "BookmarkFolder", "Bookmark", "Feed", "Likes", "Comments",
  "FeedContent", "AssignmentMigration", "Notifications", "Transactions", "FeedSubjects", "FeedClasses", "FeedTags",
  "PamphletTemplates", "Pamphlets", "PamphletSubjects", "PamphletClasses", "PamphletBoards",
  "Branding", "ChatMessages", "PushTokens", "Tags"
]

// fs
// .readdirSync(__dirname)
// .filter(function(file) {
//   return (file.indexOf(".") !== 0) && (file !== "index.js");
// })
modelListing.map((file) => {
  const model = sequelize.import(path.join(__dirname, file))
  const key = model.model.name
  db[key] = model.model
  db.scopes[key] = model.scopes
  db.filters[key] = model.filters
})

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db)
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize
module.exports = db