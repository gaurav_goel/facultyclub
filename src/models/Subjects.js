/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Subjects', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    classId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'class',
        key: 'id'
      },
      field: 'class_id'
    },
    chapterCount: {
      type: DataTypes.VIRTUAL,
      allowNull: true,
      defaultValue: '0',
      field: 'active'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'subjects'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
