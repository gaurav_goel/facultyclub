/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('ChatMessages', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    messageId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'message_id'
    },
    data: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'data'
    },
    from: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'from'
    },
    to: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'to'
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'status'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updated_on'
    }
  }, {
    tableName: 'chat_messages'
  });

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
