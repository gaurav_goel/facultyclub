/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Branding', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'logo'
    },
    brandingText: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'branding_text'
    },
    watermark: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'watermark'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'user_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updated_on'
    }
  }, {
    tableName: 'branding'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
