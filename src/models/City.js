/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('City', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'city'
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lat'
    },
    lon: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lon'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'city'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
