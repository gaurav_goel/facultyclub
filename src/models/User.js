const _ = require('lodash')
const { userTypes, completenessConfig } = require('../../config/application')
const getKey = (value, data) => _.findKey(data, (item) =>
  _( item ).values().uniq().contains(value)
)

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      // deprecated
      tuition: [
      ],
      qualification: [
        {
          model: models.TutorEducationDetail,
          as: 'tutorEducationDetail',
          include: [
            {
              model: models.TutorDegree,
              as: 'tutorDegree',
            },
            {
              model: models.TutorDegreeSubject,
              as: 'tutorDegreeSubject',
            },
            {
              model: models.TutorInstitute,
              as: 'tutorInstitute',
            },
            {
              model: models.TutorInstituteAward,
              as: 'tutorInstituteAward',
            },
          ],
        },
        {
          model: models.TutorAwardsRecognition,
          as: 'tutorAwards',
        },
        {
          model: models.TutorContentUpload,
          as: 'contentUpload',
        },
      ],
      location: [
        // {
        //   model: models.TutorLocation,
        //   as: 'tutorLocation',
        //   include: [
        //     {
        //       model: models.City,
        //       as: 'city',
        //     },
        //     {
        //       model: models.Locality,
        //       as: 'locality',
        //     },
        //   ],
        // },
        {
          model: models.SubCityLocality,
          as: 'tutorLocation',
        },
        {
          model: models.TutorAwardsRecognition,
          as: 'tutorAwards',
        },
      ],
    }
  }

  const User = sequelize.define('User', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    userType: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'user_type',
      // get: function() {
      //   const keys = getKey(this.getDataValue('userType'), userTypes)
      //   return keys && keys.length ? keys[0] : undefined
      // }
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name',
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'email',
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'password',
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'phone',
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'gender',
    },
    location: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'location',
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'city',
    },
    userDetails: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'user_details',
    },
    tagline: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'tagline',
    },
    image: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'image',
    },
    experience: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'experience',
    },
    emailVerified: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'email_verified',
    },
    phoneVerified: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'phone_verified',
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lat',
    },
    lon: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lon',
    },
    tutorSearchScore: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'tutor_search_score',
    },
    lastLogged: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'last_logged',
    },
    fblink: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'fblink',
    },
    lnlink: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'lnlink',
    },
    twitterlink: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'twitterlink',
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 1,
      field: 'active',
    },
    registrationPhase: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'registration_phase'
    },
    affilateId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'affilate_id',
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on',
    },
    testimonal: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'testimonal',
    },
    emailsent: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'emailsent',
    },
    googleToken: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'google_token',
    },
    facebookToken: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'facebook_token',
    },
    tutionCenterParent: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'tution_center_parent',
    },
    profileCompleteness: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'profile_completeness',
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'date_of_birth'
    },
    invitationStatus: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'invitation_status'
    },
    inviterId: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'inviter_id'
    },
    isGroupInvite: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'is_group_invite'
    },
    facultyProfileURI: {
      type: DataTypes.VIRTUAL,
      get: function() {
        const name = this.getDataValue('name') ? this.getDataValue('name').replace(/[\s-_]/g , '').toLowerCase() : 'unknown'
        const id = this.getDataValue('id')
        return (name !== 'unknown' && id) ?
            `https://faqulty.club/profile/${name}/${id}` :
            null
      }
    },
    likeCount: {
      type: DataTypes.VIRTUAL,
    },
    reviewCount: {
      type: DataTypes.VIRTUAL,
    },
    groupId: {
      type: DataTypes.VIRTUAL,
    },
    userTypeString: {
      type: DataTypes.VIRTUAL,
      get: function() {
        switch(this.getDataValue('userType')) {
          case 2:
            return 'tutor'
          case 3:
            return 'tuition_center'
          case 4:
            return 'student'
          default:
            return ''
        }
      }
    },
    moneyOwed: {
      type: DataTypes.VIRTUAL,
    },
  },
  {
    tableName: 'user',
    classMethods: {
      associate: function(models) {
        User.hasOne(models.OneOnOneTutionCharge, { as: 'oneOnOneTutionCharge', foreignKey: 'tutor_id'})
        User.hasOne(models.GroupTutionCharge, { as: 'groupTutionCharge', foreignKey: 'tutor_id'})
        User.hasOne(models.WeekdayTutionTime, { as: 'weekdayTutionTime', foreignKey: 'tutor_id'})
        User.hasOne(models.WeekendTutionTime, { as: 'weekendTutionTime', foreignKey: 'tutor_id'})

        User.hasMany(models.TutorCourseDetail, {
          as: 'tutorCourseDetail',
          foreignKey: 'tutor_id',
        })
        User.belongsToMany(models.TeachingBoard, { as: 'tutorBoard', through: 'tutor_board', foreignKey: 'tutor_id', otherKey: 'board' })
        User.belongsToMany(models.TeachingMode, { as: 'tutorTeachingMode', through: 'tutor_teaching_mode', foreignKey: 'tutor_id', otherKey: 'teaching_mode' })
        User.belongsToMany(models.TeachingLanguage, { as: 'tutorTeachingLanguage', through: 'tutor_teaching_language', foreignKey: 'tutor_id', otherKey: 'language' })
        User.hasMany(models.TutorSchool, { as: 'tutorSchool', foreignKey: 'tutor_id' })

        User.hasMany(models.TutorEducationDetail, { as: 'tutorEducationDetail', foreignKey: 'tutor_id' })

        User.hasMany(models.TutorAwardsRecognition, { as: 'tutorAwards', foreignKey: 'tutor_id' })
        User.hasMany(models.SubCityLocality, { as: 'tutorLocation', foreignKey: 'tutor_id' })
        User.hasMany(models.TutorUserLikes, {
          as: 'userLikes',
          // through: 'tutor_user_likes',
          foreignKey: 'tutor_id',
        })
        // User.belongsToMany(User, {
        //   as: 'userReviews',
        //   through: 'tutor_user_reviews',
        //   foreignKey: 'tutor_id',
        //   otherKey: 'user_id',
        // })
        User.hasMany(models.User, { as: 'faculties', foreignKey: 'tution_center_parent' })
        User.hasMany(models.TutorContentUpload, { as: 'contentUpload', foreignKey: 'tutor_id' })

        const scopes = {
          defaultScope: {
            // where: {
            //   active: 1,
            // },
            attributes: {
              exclude: ['password', 'google_token', 'facebook_token'],
            },
            include: getIncludes(models).location,
            exclude: ['password', 'google_token', 'facebook_token'],
          },
          scopes: {
            allUsers: {},
            detailed: {
              // where: {
              //   active: 1,
              // },
              include: _.concat(getIncludes(models).qualification, getIncludes(models).tuition, getIncludes(models).location),
              attributes: {
                exclude: ['password', 'google_token', 'facebook_token'],
              }
            },
            likes: {
              include: [{
                model: User,
                as: 'userLikes',
                attributes: ['id', 'name', 'email', 'phone', 'image'],
              }],
              attributes: {
                exclude: ['password', 'google_token', 'facebook_token'],
              }
            },
            reviews: {
              include: [{
                model: User,
                as: 'userReviews',
                attributes: ['id', 'name', 'email', 'phone', 'image'],
              }],
              attributes: {
                exclude: ['password', 'google_token', 'facebook_token'],
              }
            },
            tuition: {
              // where: {
              //   active: 1,
              // },
              include: getIncludes(models).tuition,
              attributes: {
                exclude: ['password', 'google_token', 'facebook_token'],
              }
            },
            qualification: {
              // where: {
              //   active: 1,
              // },
              include: getIncludes(models).qualification,
              attributes: {
                exclude: ['password', 'google_token', 'facebook_token'],
              }
            },
          },
        }
        User.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          User.addScope(key, scopes.scopes[key], { override: true })
        })
      },
      getEmptyUser: () => ({
        phone: '',
        email: '',
        name: '',
        password: '',
        userType: 2,
        //non-null values
        city: '',
        tagline: '',
        experience: 0,
        emailVerified: 0,
        phoneVerified: 0,
        lat: 0.0,
        lon: 0.0,
        tutorSearchScore: 0.0,
        lastLogged: new Date(),
        fblink: '',
        lnlink: '',
        twitterlink: '',
        active: 0,
        registrationPhase: '',
        affilateId: 0,
        createdOn: new Date(),
      }),
    },
    getterMethods: {
      oneOnOneTutionCharge: function () {
        return this.getDataValue('oneOnOneTutionCharge')
      },
      groupTutionCharge: function () {
        return this.getDataValue('groupTutionCharge')
      },
      weekdayTutionTime: function () {
        return this.getDataValue('weekdayTutionTime')
      },
      weekendTutionTime: function () {
        return this.getDataValue('weekendTutionTime')
      },
      coursesTaught: function () {
        return this.getDataValue('coursesTaught')
      },
      tutorBoard: function () {
        return this.getDataValue('tutorBoard')
      },
      tutorTeachingMode: function () {
        return this.getDataValue('tutorTeachingMode')
      },
      tutorTeachingLanguage: function () {
        return this.getDataValue('tutorTeachingLanguage')
      },
      tutorSchool: function () {
        return this.getDataValue('tutorSchool')
      },
      tutorLocation: function () {
        return this.getDataValue('tutorLocation')
      },
      tutorAwards: function () {
        return this.getDataValue('tutorAwards')
      },
      likeCount: function () {
        return this.getDataValue('likeCount')
      },
      userLikes: function () {
        return this.getDataValue('userLikes')
      },
      reviewCount: function () {
        return this.getDataValue('reviewCount')
      },
      tutionCenterParent: function () {
        return this.getDataValue('tutionCenterParent')
      },
      faculties: function () {
        return this.getDataValue('faculties')
      },
      userReviews: function () {
        return this.getDataValue('userReviews')
      },
      rawProfileCompleteness: function () {
        return this.getDataValue('profileCompleteness')
      },
      dateOfBirth: function () {
        return this.getDataValue('dateOfBirth')
      },
      invitationStatus: function () {
        return this.getDataValue('invitationStatus')
      },
      inviterId: function () {
        return this.getDataValue('inviterId')
      },
      isGroupInvite: function () {
        return this.getDataValue('isGroupInvite')
      },
      profileCompleteness: function () {
        if (this.getDataValue('profileCompleteness')) {
          const completenessMap = {}
          const completeness = this.getDataValue('profileCompleteness').split('')
          for(let i = 0; i < completeness.length; i++) {
            if (completenessConfig.indices[i] !== undefined && 
              completenessConfig.percentages[i] !== undefined) {
              completenessMap[completenessConfig.indices[i]] = completeness[i] === '0'
                ? 0 : completenessConfig.percentages[i]
            }
          }
          return completenessMap
        }
      },
      isBdayToday: function () {
        return this.getDataValue('isBdayToday')
      },
      moneyOwed: function (value) {
        return this.getDataValue('moneyOwed')
      },
    },
    setterMethods: {
      oneOnOneTutionCharge: function (value) {
        this.setDataValue('oneOnOneTutionCharge', value)
      },
      groupTutionCharge: function (value) {
        this.setDataValue('groupTutionCharge', value)
      },
      weekdayTutionTime: function (value) {
        this.setDataValue('weekdayTutionTime', value)
      },
      weekendTutionTime: function (value) {
        this.setDataValue('weekendTutionTime', value)
      },
      coursesTaught: function (value) {
        this.setDataValue('coursesTaught', value)
      },
      tutorBoard: function (value) {
        this.setDataValue('tutorBoard', value)
      },
      tutorTeachingMode: function (value) {
        this.setDataValue('tutorTeachingMode', value)
      },
      tutorTeachingLanguage: function (value) {
        this.setDataValue('tutorTeachingLanguage', value)
      },
      tutorSchool: function (value) {
        this.setDataValue('tutorSchool', value)
      },
      tutorLocation: function (value) {
        this.setDataValue('tutorLocation', value)
      },
      tutorAwards: function (value) {
        this.setDataValue('tutorAwards', value)
      },
      likeCount: function (value) {
        this.setDataValue('likeCount', value)
      },
      userLikes: function (value) {
        this.setDataValue('userLikes', value)
      },
      reviewCount: function (value) {
        this.setDataValue('reviewCount', value)
      },
      tutionCenterParent: function (value) {
        this.setDataValue('tutionCenterParent', value)
      },
      faculties: function (value) {
        this.setDataValue('faculties', value)
      },
      userReviews: function (value) {
        this.setDataValue('userReviews', value)
      },
      dateOfBirth: function (value) {
        this.setDataValue('dateOfBirth', value)
      },
      invitationStatus: function (value) {
        this.setDataValue('invitationStatus', value)
      },
      inviterId: function (value) {
        this.setDataValue('inviterId', value)
      },
      isGroupInvite: function (value) {
        this.setDataValue('isGroupInvite', value)
      },
      profileCompleteness: function (value) {
        if (completenessConfig.indices[value] !== undefined) {
          const completeness = this.getDataValue('profileCompleteness').split('')
          completeness[completenessConfig.indices[value]] = '1'
          this.setDataValue('profileCompleteness', completeness.join(''))
        }
      },
      reduceProfileCompleteness: function (value) {
        if (completenessConfig.indices[value] !== undefined) {
          const completeness = this.getDataValue('profileCompleteness').split('')
          completeness[completenessConfig.indices[value]] = '0'
          this.setDataValue('profileCompleteness', completeness.join(''))
        }
      },
      isBdayToday: function (value) {
        this.setDataValue('isBdayToday', value)
      },
      moneyOwed: function (value) {
        this.setDataValue('moneyOwed', value)
      },
    },
  })

  /*
   * Associations
   */
  // User.belongsToMany(User, {
  //   as: 'userLikes',
  //   through: 'tutor_user_likes',
  //   foreignKey: 'tutor_id',
  //   otherKey: 'user_id',
  // })

  /*
   * Add all scopes here
   */
   

  return {
    model: User,
    filters: {},
    includes: {},
  }
}
