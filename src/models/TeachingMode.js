/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TeachingMode', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    mode: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'mode'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'teaching_mode'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
