/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('UserOTP', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'mobile'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'email'
    },
    otp: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'otp'
    },
    verified: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'verified'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'user_otp'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
