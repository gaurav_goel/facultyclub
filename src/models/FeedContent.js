/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('FeedContent', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    feedId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'feed',
        key: 'id'
      },
      field: 'feed_id'
    },
    contentUrl: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'content_url'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    mimetype: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'mimetype'
    }
  }, {
    tableName: 'feed_content'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
