/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Answer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    questionId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'question',
        key: 'id'
      },
      field: 'question_id'
    },
    answer: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'answer'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'answer'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
