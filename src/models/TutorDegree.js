/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorDegree', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    degree: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'degree'
    },
    level: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'level'
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'active'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_degree'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
