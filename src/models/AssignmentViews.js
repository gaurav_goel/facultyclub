/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('AssignmentViews', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    assignmentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'assignments',
        key: 'id'
      },
      field: 'assignment_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'assignment_views',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.Assignments, { as: 'assignment', foreignKey: 'assignment_id'})

        const scopes = {
          defaultScope: {
            include: [
              {
                model: models.Assignments,
                as: 'assignment',
                include: [
                  {
                    model: models.Class,
                    as: 'classInfo',
                  },
                  {
                    model: models.Subjects,
                    as: 'subjectInfo',
                  },
                  {
                    model: models.Chapters,
                    as: 'chapterInfo',
                  },
                ],
              },
            ],
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
      },
    },
    getterMethods: {
      assignment: function () {
        return this.getDataValue('assignment')
      },
    },
    setterMethods: {
      assignment: function (value) {
        this.setDataValue('assignment', value)
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
