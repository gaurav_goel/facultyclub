/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('FeedTags', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    feedId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'feed',
        key: 'id'
      },
      field: 'feed_id'
    },
    tag: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'tag'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'feed_tags'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
