/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Session', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name'
    },
    durationHours: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      field: 'duration_hours'
    },
    durationMins: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      field: 'duration_mins'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    groupId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'StudentGroups',
        key: 'id'
      },
      field: 'group_id'
    }
  }, {
    tableName: 'session',
    classMethods: {
      associate: function(models) {
        model.hasMany(models.SessionStudent, { as: 'studentSessions', foreignKey: 'session_id'})
      }
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
