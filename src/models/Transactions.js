/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Transaction', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    studentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'student_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'type'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'amount'
    },
    dateFrom: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'date_from'
    },
    dateTo: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'date_to'
    },
    suggestedSessions: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'suggested_sessions'
    },
    suggestedHours: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'suggested_hours'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'transactions'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
