/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorCourseDetail', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_course_detail',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.Class, { as: 'classObj', foreignKey: 'class'})
        model.belongsTo(models.ClassSubject, { as: 'subjectObj', foreignKey: 'subject'})
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
