/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('TutorUserReviews', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'user_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'tutor_id'
    },
    reviews: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'reviews'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_user_reviews',
    classMethods: {
      associate: function(models) {
        model.hasOne(models.User, { as: 'user', foreignKey: 'id'})
      }
    }
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
