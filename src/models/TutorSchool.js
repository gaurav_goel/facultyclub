/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorSchool', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    school: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'school'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_school'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
