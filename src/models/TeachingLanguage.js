/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TeachingLanguage', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    language: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'language'
    },
    sortOrder: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'sort_order'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'teaching_language'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
