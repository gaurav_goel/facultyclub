/* jshint indent: 2 */
// performance : 0,1,2 (poor, neutral, good); attendance: 0,1 (absent, present)
module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('SessionStudent', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    sessionId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'session',
        key: 'id'
      },
      field: 'session_id'
    },
    studentId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'student_id'
    },
    performance: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'performance'
    },
    attendance: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'attendance'
    }
  }, {
    tableName: 'session_student',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.Session, { as: 'session', foreignKey: 'session_id'})
        model.belongsTo(models.User, { as: 'student', foreignKey: 'student_id'})
      }
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}