/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      base: [
        {
          model: models.User,
          as: 'user',
          attributes: ['image', 'name', 'id']
        },
      ],
    }
  }

  const model = sequelize.define('Notifications', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'type'
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'message'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'user_id'
    },
    seen: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0',
      field: 'seen'
    }
  }, {
    tableName: 'notifications',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.User, { as: 'user', foreignKey: 'user_id'})

        const scopes = {
          defaultScope: {
            attributes: {
            },
          },
          scopes: {
            base: {
              include: getIncludes(models).base,
            },
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    }
  });

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
