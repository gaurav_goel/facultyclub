/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('TutorUserLikes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'user_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'tutor_id'
    },
    userIp: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'user_ip'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_user_likes'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
