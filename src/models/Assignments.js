/* jshint indent: 2 */
const _ = require('lodash')

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      withInfo: [
        {
          model: models.Class,
          as: 'classInfo',
        },
        {
          model: models.Subjects,
          as: 'subjectInfo',
        },
        {
          model: models.Chapters,
          as: 'chapterInfo',
        },
      ],
      detailed: [
        {
          model: models.Question,
          as: 'questionSet',
        }
      ],
    }
  }

  const model = sequelize.define('Assignments', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    label: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'label'
    },
    class: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'class',
        key: 'id'
      },
      field: 'class'
    },
    subject: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'subjects',
        key: 'id'
      },
      field: 'subject'
    },
    chapter: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'chapters',
        key: 'chapterId'
      },
      field: 'chapter'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'updated_on'
    }
  }, {
    tableName: 'assignments',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.Class, { as: 'classInfo', foreignKey: 'class'})
        model.belongsTo(models.Subjects, { as: 'subjectInfo', foreignKey: 'subject'})
        model.belongsTo(models.Chapters, { as: 'chapterInfo', foreignKey: 'chapter'})
        model.hasMany(models.Question, { as: 'questionSet', foreignKey: 'assignment_id' })

        const scopes = {
          defaultScope: {
            attributes: {
            },
          },
          scopes: {
            withInfo: {
              include: getIncludes(models).withInfo,
            },
            detailed: {
              include: _.concat(getIncludes(models).withInfo, getIncludes(models).detailed)
            },
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    },
    getterMethods: {
      classInfo: function () {
        return this.getDataValue('classInfo')
      },
      subjectInfo: function () {
        return this.getDataValue('subjectInfo')
      },
      chapterInfo: function () {
        return this.getDataValue('chapterInfo')
      },
    },
    setterMethods: {
      classInfo: function (value) {
        this.setDataValue('classInfo', value)
      },
      subjectInfo: function (value) {
        this.setDataValue('subjectInfo', value)
      },
      chapterInfo: function (value) {
        this.setDataValue('chapterInfo', value)
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
