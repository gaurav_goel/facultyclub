/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('StudentTeacherMap', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tuitionType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'tuition_type'
    },
    studentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'student_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    pupilInfoId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pupilInfo',
        key: 'id'
      },
      field: 'pupil_info_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'student_teacher_map',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.User, { as: 'student', foreignKey: 'student_id'})
        model.belongsTo(models.User, { as: 'tutor', foreignKey: 'tutor_id'})
        model.belongsTo(models.PupilInfo, { as: 'pupilInfo', foreignKey: 'pupil_info_id'})
      },
    },
  });

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
