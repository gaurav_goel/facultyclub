/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('BookmarkFolder', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    bookmarks: {
      type: DataTypes.VIRTUAL,
    },
  }, {
    tableName: 'bookmark_folder',
    classMethods: {
      getterMethods: {
        bookmarks: function () {
          return this.getDataValue('bookmarks')
        },
      },
      setterMethods: {
        bookmarks: function (value) {
          this.setDataValue('bookmarks', value)
        },
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
