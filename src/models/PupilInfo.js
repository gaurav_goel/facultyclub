/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('PupilInfo', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    tuitionType: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'tuition_type'
    },
    billingCycle: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'billing_cycle'
    },
    fee: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'fee'
    },
    classDurationHours: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      field: 'class_duration_hours'
    },
    classDurationMins: {
      type: DataTypes.INTEGER(2),
      allowNull: true,
      field: 'class_duration_mins'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'pupil_info'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
