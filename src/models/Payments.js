/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Payments', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'amount'
    },
    discount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'discount'
    },
    carryForward: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'carry_forward'
    },
    invoiceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'invoices',
        key: 'id'
      },
      field: 'invoice_id'
    },
    remarks: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'remarks'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    pendingAmount: {
      type: DataTypes.VIRTUAL,
    },
  }, {
    tableName: 'payments',
    classMethods: {
      getterMethods: {
        pendingAmount: function () {
          return this.getDataValue('pendingAmount')
        },
      },
      setterMethods: {
        pendingAmount: function (value) {
          this.setDataValue('pendingAmount', value)
        },
      },
    }
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
