/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorContentUpload', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'tutor_id',
    },
    contentUrl: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'content_url',
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'description',
    },
    mimetype: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'mimetype',
    },
    numViews: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'num_views',
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'updated_on',
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on',
    }
  }, {
    tableName: 'tutor_content_upload'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
