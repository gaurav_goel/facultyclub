/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Balance', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'amount'
    },
    payee: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'payee'
    },
    recipient: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'recipient'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'balance'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
