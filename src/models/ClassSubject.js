/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('ClassSubject', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    classId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'class_id'
    },
    subject: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'subject'
    },
    parent: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'parent'
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'active'
    },
    order: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'order'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'class_subject'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
