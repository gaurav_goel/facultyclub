/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('StudentGroupMap', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    groupId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'studentGroups',
        key: 'id'
      },
      field: 'group_id'
    },
    studentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'student_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'student_group_map',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.User, { as: 'student', foreignKey: 'student_id'})
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
