/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('GroupInvite', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    groupId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'group_id'
    },
    studentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'student_id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'tutor_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'group_invite',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.User, { as: 'student', foreignKey: 'student_id'})
      }
    },
  });

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
