/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Locality', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    cityId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'city_id'
    },
    locality: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'locality'
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lat'
    },
    lon: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'lon'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'locality'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
