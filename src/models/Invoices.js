module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Invoices', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    dateFrom: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date_from'
    },
    dateTo: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'date_to'
    },
    payee: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'payee'
    },
    recipient: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'recipient'
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'type'
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'status'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'amount'
    },
    suggestedSessions: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'suggested_sessions'
    },
    suggestedHours: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'suggested_hours'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    pendingAmount: {
      type: DataTypes.VIRTUAL,
    },
  }, {
    tableName: 'invoices',
    classMethods: {
      getterMethods: {
        pendingAmount: function () {
          return this.getDataValue('pendingAmount')
        },
      },
      setterMethods: {
        pendingAmount: function (value) {
          this.setDataValue('pendingAmount', value)
        },
      },
    }
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
