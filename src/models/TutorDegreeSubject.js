/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorDegreeSubject', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    degreeId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'degree_id'
    },
    degreeSubject: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'degree_subject'
    },
    active: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'active'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_degree_subject'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
