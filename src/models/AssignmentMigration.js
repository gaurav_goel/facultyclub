/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('AssignmentMigration', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    startedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'started_on'
    },
    completedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'completed_on'
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      defaultValue: '0',
      field: 'status'
    }
  }, {
    tableName: 'assignment_migration'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
