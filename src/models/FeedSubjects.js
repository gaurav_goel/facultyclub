/* jshint indent: 2 */
const _ = require('lodash')
module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      base: [
        {
          model: models.ClassSubject,
          as: 'subject',
        },
      ]
    }
  }
  const model = sequelize.define('FeedSubjects', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    feedId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'feed',
        key: 'id'
      },
      field: 'feed_id'
    },
    subjectId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'classSubject',
        key: 'id'
      },
      field: 'subject_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'feed_subjects',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.ClassSubject, { as: 'subject', foreignKey: 'subject_id'})

        const scopes = {
          defaultScope: {
            include: getIncludes(models).base,
          },
          scopes: {
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
