/* jshint indent: 2 */
const R = require('ramda')

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('StudentGroups', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    pupilInfoId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pupilInfo',
        key: 'id'
      },
      field: 'pupil_info_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'student_groups',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.PupilInfo, { as: 'pupilInfo', foreignKey: 'pupil_info_id'})
        model.belongsToMany(models.User, { as: 'students', through: 'student_group_map', foreignKey: 'group_id', otherKey: 'student_id',
          attributes: ['id', 'name', 'image'] })
      },
    },
    getterMethods: {
      students: function () {
        return this.getDataValue('students')
      },
      isBdayToday: function () {
        return this.getDataValue('isBdayToday')
      },
      tutor: function() {
        return this.getDataValue('tutor')
      }
    },
    setterMethods: {
      students: function(value) {
        this.setDataValue('students', R.map(student => ({
          id: student.id,
          name: student.name,
          phone: student.phone,
          email: student.email,
          image: student.image,
        }), value))
      },
      isBdayToday: function(value) {
        this.setDataValue('isBdayToday', value)
      },
      tutor: function(value) {
        this.setDataValue('tutor', value)
      }
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
