/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  var model = sequelize.define('PushTokens', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'user_id'
    },
    fcmToken: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'fcm_token'
    },
    userType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'user_type'
    }
  }, {
    tableName: 'push_tokens'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
