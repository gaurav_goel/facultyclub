/* jshint indent: 2 */
const _ = require('lodash')

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      base: [
        {
          model: models.User,
          as: 'user',
          attributes: ['image', 'name', 'id']
        },
        {
          model: models.PamphletSubjects,
          as: 'subjects',
        },
        {
          model: models.PamphletClasses,
          as: 'classes',
        },
        {
          model: models.PamphletBoards,
          as: 'boards',
        },
      ],
    }
  }

  const model = sequelize.define('Pamphlets', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'type',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name',
    },
    title: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'title',
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'description',
    },
    logo: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'logo',
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'address',
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'email',
    },
    mobile: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'mobile',
    },
    watermark: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'watermark',
    },
    brandingText: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'brandingText',
    },
    url: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'url',
    },
    thumbUrl: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'thumb_url',
    },
    subjects: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'subjects',
    },
    classes: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'classes',
    },
    boards: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'boards',
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'user_id',
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on',
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updated_on',
    }
  }, {
    tableName: 'pamphlets',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.User, { as: 'user', foreignKey: 'userId'})

        const scopes = {
          defaultScope: {
            attributes: {
            },
          },
          scopes: {
            base: {
              include: getIncludes(models).base,
            },
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    },
    getterMethods: {
      user: function () {
        return this.getDataValue('user')
      },
    },
    setterMethods: {
      user: function (value) {
        this.setDataValue('user', value)
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
