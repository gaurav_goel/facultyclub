/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('FileMeta', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    originalName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'original_name'
    },
    filename: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'filename'
    },
    encoding: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'encoding'
    },
    mimetype: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'mimetype'
    },
    uri: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'uri'
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'path'
    },
    size: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'size'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'file_meta'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
