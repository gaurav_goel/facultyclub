/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Bookmark', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    moduleId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'module_id'
    },
    moduleType: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'module_type'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    folderId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'bookmarkFolder',
        key: 'id'
      },
      field: 'folder_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    name: {
      type: DataTypes.VIRTUAL,
    },
    description: {
      type: DataTypes.VIRTUAL,
    }
  }, {
    tableName: 'bookmark',
    classMethods: {
      getterMethods: {
        name: function () {
          return this.getDataValue('name')
        },
        description: function () {
          return this.getDataValue('description')
        },
      },
      setterMethods: {
        name: function (value) {
          this.setDataValue('name', value)
        },
        description: function (value) {
          this.setDataValue('description', value)
        },
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
