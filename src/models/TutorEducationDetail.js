/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorEducationDetail', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'tutor_id'
    },
    college: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'college'
    },
    prefered: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'prefered'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_education_detail',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.TutorDegree, { as: 'tutorDegree', foreignKey: 'degree'})
        model.belongsTo(models.TutorDegreeSubject, { as: 'tutorDegreeSubject', foreignKey: 'subject'})
        model.belongsTo(models.TutorInstitute, { as: 'tutorInstitute', foreignKey: 'institute'})
        model.belongsTo(models.TutorInstituteAward, { as: 'tutorInstituteAward', foreignKey: 'awards'})
      },
    },
    getterMethods: {
      tutorDegree: function () {
        return this.getDataValue('tutorDegree')
      },
      tutorDegreeSubject: function () {
        return this.getDataValue('tutorDegreeSubject')
      },
      tutorInstitute: function () {
        return this.getDataValue('tutorInstitute')
      },
      tutorInstituteAward: function () {
        return this.getDataValue('tutorInstituteAward')
      },
    },
    setterMethods: {
      tutorDegree: function (value) {
        this.setDataValue('tutorDegree', value)
      },
      tutorDegreeSubject: function (value) {
        this.setDataValue('tutorDegreeSubject', value)
      },
      tutorInstitute: function (value) {
        this.setDataValue('tutorInstitute', value)
      },
      tutorInstituteAward: function (value) {
        this.setDataValue('tutorInstituteAward', value)
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
