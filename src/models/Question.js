/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      withAnswer: [
        {
          model: models.Answer,
          as: 'answer',
        },
      ],
    }
  }

  const model = sequelize.define('Question', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    assignmentId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'assignments',
        key: 'id'
      },
      field: 'assignment_id'
    },
    question: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'question'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'question',
    classMethods: {
      associate: function(models) {
        model.hasOne(models.Answer, { as: 'answer', foreignKey: 'question_id'})

        const scopes = {
          defaultScope: {
            include: getIncludes(models).withAnswer,
          },
          scopes: {
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
      getterMethods: {
        answer: function () {
          return this.getDataValue('answer')
        },
      },
      setterMethods: {
        answer: function (value) {
          this.setDataValue('answer', value)
        },
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
