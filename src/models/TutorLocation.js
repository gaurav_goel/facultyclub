/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorLocation', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    cityId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'city',
        key: 'id'
      },
      field: 'city_id'
    },
    localityId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'locality',
        key: 'id'
      },
      field: 'locality_id'
    },
    primary: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'primary'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_location',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.City, { as: 'city', foreignKey: 'city_id'})
        model.belongsTo(models.Locality, { as: 'locality', foreignKey: 'locality_id'})
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
