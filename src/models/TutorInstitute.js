/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('TutorInstitute', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    instituteName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'institute_name'
    },
    level: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'level'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'tutor_institute'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
