/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('Chapters', {
    chapterId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'chapter_id'
    },
    chapterName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'chapter_name'
    },
    sortOrder: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'sort_order'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    subjectId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'subjects',
        key: 'id'
      },
      field: 'subject_id'
    }
  }, {
    tableName: 'chapters'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
