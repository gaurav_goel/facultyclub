/* jshint indent: 2 */
const _ = require('lodash')

module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      base: [
        {
          model: models.User,
          as: 'tutor',
          attributes: ['image', 'name', 'id', 'email']
        },
        {
          model: models.FeedContent,
          as: 'content',
        },
        {
          model: models.FeedSubjects,
          as: 'subjects',
        },
        {
          model: models.FeedClasses,
          as: 'classes',
        },
        {
          model: models.FeedTags,
          as: 'tags',
        },
      ],
      withLikes: [
        {
          model: models.Likes,
          as: 'likes',
          include: [
            {
              model: models.User,
              as: 'user',
              attributes: ['image', 'name', 'id']
            },
          ],
        },
      ],
      withComments: [
        {
          model: models.Comments,
          as: 'comments',
          include: [
            {
              model: models.User,
              as: 'user',
              attributes: ['image', 'name', 'id']
            },
          ],
        },
      ],
    }
  }

  const model = sequelize.define('Feed', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      },
      field: 'tutor_id'
    },
    isModerated: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: '0',
      field: 'is_moderated',
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'description'
    },
    numViews: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'num_views'
    },
    updatedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: 'CURRENT_TIMESTAMP',
      field: 'updated_on'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    },
    hasLiked: {
      type: DataTypes.VIRTUAL,
    }
  }, {
    tableName: 'feed',
    classMethods: {
      associate: function(models) {
        model.hasMany(models.FeedContent, { as: 'content', foreignKey: 'feed_id'})
        model.belongsTo(models.User, { as: 'tutor', foreignKey: 'tutorId'})
        model.hasMany(models.Likes, { as: 'likes', foreignKey: 'module_id'})
        model.hasMany(models.Comments, { as: 'comments', foreignKey: 'module_id'})
        model.hasMany(models.FeedSubjects, { as: 'subjects', foreignKey: 'feed_id'})
        model.hasMany(models.FeedClasses, { as: 'classes', foreignKey: 'feed_id'})
        model.hasMany(models.FeedTags, { as: 'tags', foreignKey: 'feed_id'})

        const detailedIncludeScope = _.concat(
          getIncludes(models).base,
          getIncludes(models).withLikes,
          getIncludes(models).withComments
        )

        const scopes = {
          defaultScope: {
            attributes: {
            },
          },
          scopes: {
            base: {
              include: getIncludes(models).base,
            },
            withLikes: {
              include: _.concat(
                getIncludes(models).base,
                getIncludes(models).withLikes
              ),
            },
            withComments: {
              include: _.concat(
                getIncludes(models).base,
                getIncludes(models).withComments
              ),
            },
            detailed: {
              include: detailedIncludeScope,
            }
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    },
    getterMethods: {
      tutor: function () {
        return this.getDataValue('tutor')
      },
      likesCount: function () {
        return this.getDataValue('likesCount')
      },
      commentsCount: function () {
        return this.getDataValue('commentsCount')
      },
      content: function () {
        return this.getDataValue('content')
      },
      subjects: function() {
        return this.getDataValue('subjects')
      },
      classes: function() {
        return this.getDataValue('classes')
      },
      tags: function() {
        return this.getDataValue('tags')
      },
    },
    setterMethods: {
      tutor: function (value) {
        this.setDataValue('tutor', value)
      },
      likesCount: function (value) {
        this.setDataValue('likesCount', value)
      },
      commentsCount: function (value) {
        this.setDataValue('commentsCount', value)
      },
      content: function (value) {
        this.setDataValue('content', value)
      },
      subjects: function (value) {
        this.setDataValue('subjects', value)
      },
      classes: function (value) {
        this.setDataValue('classes', value)
      },
      tags: function (value) {
        this.setDataValue('tags', value)
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
