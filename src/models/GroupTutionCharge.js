/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  const model = sequelize.define('GroupTutionCharge', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    tutorId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'tutor_id'
    },
    startingPrice: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'starting_price'
    },
    finalPrice: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'final_price'
    },
    batchSize: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'batch_size'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'group_tution_charge'
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
}
