/* jshint indent: 2 */
const _ = require('lodash')
module.exports = function(sequelize, DataTypes) {
  const getIncludes = (models) => {
    return {
      base: [
        {
          model: models.TeachingBoard,
          as: 'board',
        },
      ]
    }
  }
  const model = sequelize.define('PamphletBoards', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    pamphletId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pamphlets',
        key: 'id'
      },
      field: 'pamphlet_id'
    },
    boardId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'teachingBoard',
        key: 'id'
      },
      field: 'board_id'
    },
    createdOn: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.NOW,
      field: 'created_on'
    }
  }, {
    tableName: 'pamphlet_boards',
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.TeachingBoard, { as: 'board', foreignKey: 'board_id'})

        const scopes = {
          defaultScope: {
            include: getIncludes(models).base,
          },
          scopes: {
          },
        }
        model.addScope('defaultScope', scopes.defaultScope, { override: true })
        Object.keys(scopes.scopes).map((key) => {
          model.addScope(key, scopes.scopes[key], { override: true })
        })
      },
    },
  })

  return {
    model,
    scopes: {},
    filters: {},
    includes: {},
  }
};
