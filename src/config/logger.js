'use strict'
const winston = require('winston')
const fs = require('fs')
const mkdirp = require('mkdirp')
winston.transports.DailyRotateFile = require('winston-daily-rotate-file')

const env = process.env.NODE_ENV || 'development'
// This is the default log level. This will depend upon the environment
let logLevel = undefined
const DEFAULT_LOGGING_FOLDER = 'logs'
const DEFAULT_LOGGING_FILE_NAME = 'logs'
// This is where the log files will be saved
const DEFAULT_LOGGING_FILE_PATH = DEFAULT_LOGGING_FOLDER + '/' + DEFAULT_LOGGING_FILE_NAME
// The maximum log file size. Once the threshold is reached, a new file will be created
const LOGGING_FILE_MAXSIZE = 10 * 1024 * 1024
// The default rotation patterns which should be used
const LOG_ROTATION_DATE_PATTERN = {
  daily: '.yyyy-MM-dd',
  hourly: '.yyyy-MM-ddTHH'
}

if (env == 'development') {
  logLevel = 'debug'
} else if (env == 'test') {
  logLevel = 'error'
} else {
  logLevel = 'info'
}

// make logs directory if it doesn't exist
fs.exists(DEFAULT_LOGGING_FOLDER, function(exists) {
  if (!exists) {
    mkdirp(DEFAULT_LOGGING_FOLDER, function (err) {
      if (err) {
        conosle.log('Failed to create the deafault logging folder - ' + DEFAULT_LOGGING_FOLDER)
      }
    })
  }
})

/**
 * The logger setting object for printing to the console
 **/
const CONSOLE_TRANSPORT_OPTIONS = {
  'level': logLevel,
  'silent': false,
  'colorize': true,
  'timestamp': true,
}

/**
 * The logger setting object for printing to the log files
 **/
const FILE_TRANSPORT_OPTIONS = {
  // skip debug level logs on production
  'level': logLevel,
  'silent': false,
  timestamp: true,
  maxsize: LOGGING_FILE_MAXSIZE,
  json: true,
  filename: DEFAULT_LOGGING_FILE_PATH,
  datePattern: LOG_ROTATION_DATE_PATTERN.daily
}

// the default transport for loggers
const DEFAULT_TRANSPORTS = [
    new (winston.transports.Console)(CONSOLE_TRANSPORT_OPTIONS),
    new (winston.transports.DailyRotateFile)(FILE_TRANSPORT_OPTIONS),
  ]

// setting the default options for all winston loggers
winston.loggers.options = {
    console: CONSOLE_TRANSPORT_OPTIONS,
    file: FILE_TRANSPORT_OPTIONS
  }

// setting the default options for all winston loggers
winston.loggers.options.transports = DEFAULT_TRANSPORTS

// the default logger which should be used for general logging
exports.logger = new (winston.Logger)({
    exitOnError: false, //don't crash on exception
    transports: DEFAULT_TRANSPORTS,
    'showFileName': true
})
exports.DEFAULT_TRANSPORTS = DEFAULT_TRANSPORTS