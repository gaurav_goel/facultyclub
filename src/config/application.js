const ACCEPTED_MIME_TYPES = ['application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 
			'image/gif', 'application/msword',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document']

const AUTH_SECRET = 'secret'

exports.fileIngest = {
	acceptedMimeTypes: ACCEPTED_MIME_TYPES,
	downloadPath: '/egest?file='
}

exports.auth = {
	secret: AUTH_SECRET,
	strategy: {
		google: 'google-plus-token',
		facebook: 'facebook-token',
	},
}

exports.mail = {
	maxRetries: 5,
	allowMailSend: false,
}