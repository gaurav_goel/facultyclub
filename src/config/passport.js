const _ = require('lodash')
const md5 = require('md5')
const passport = require('passport')
const request = require('request')
const Q = require('q')
const LocalStrategy = require('passport-local').Strategy
const FacebookStrategy = require('passport-facebook-token')
const GoogleStrategy = require('passport-google-plus-token')

const User = require('../models').User;
const UserController = require('../controllers/user')

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

/**
 * Sign in using Email and Password.
 */
passport.use(new LocalStrategy({ usernameField: 'phone', passwordField: 'password' }, (phone, password, done) => {
  User.scope('allUsers').find({
    where: {
      phone,
    }
  }).then(function (user, err) {

    if (!user) {
      return done(null, false, { msg: `Invalid phone or password` });
    } else if (user.phoneVerified === 0) {
      return done(null, false, { msg: `Invalid phone or password` });
    }

    if(user.password === md5(password)) {
      return done(null, user);
    }
    return done(null, false, { msg: 'Invalid phone or password' });
  });
}));

/**
 * OAuth Strategy Overview
 *
 * - User is already logged in.
 *   - Check if there is an existing account with a provider id.
 *     - If there is, return an error message. (Account merging not supported)
 *     - Else link new OAuth account with currently logged-in user.
 * - User is not logged in.
 *   - Check if it's a returning user.
 *     - If returning user, sign in and we are done.
 *     - Else check if there is an existing account with user's email.
 *       - If there is, return an error message.
 *       - Else create a new account.
 */
const updateUser = (options) => {
  const deferred = Q.defer()
  let info
  if (!options.profile) {
    info = 'Unauthorized'
  }

  let error = null
  let userForPersistence = null
  User.scope('allUsers').findOne({
    where: {
      email: options.email,
    },
  }).then((user) => {
    if (user) {
      userForPersistence = user
      userForPersistence[options.source + 'Token'] = options.accessToken
    } else {
      userForPersistence = User.build(User.getEmptyUser())
      userForPersistence.phone = options.phone
      userForPersistence.email = options.email
      userForPersistence.name = options.displayName
      userForPersistence.image = options.photo

    }
    userForPersistence.save().then((userObj) => {
      deferred.resolve({
        error: undefined,
        user: userObj,
        info,
      })
    }, (err) => {
      deferred.resolve({
        error: err,
        user: userForPersistence,
        info,
      })
    })
  })

  return deferred.promise  
}

/**
 * Sign in with Facebook.
 */
passport.use(new FacebookStrategy({
  clientID: process.env.FACEBOOK_ID,
  clientSecret: process.env.FACEBOOK_SECRET,
  profileFields: ['name', 'email', 'link'],
  passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
  const email = profile.emails[0].value
  const photo = profile.photos[0].value
  const displayName = `${profile.name.givenName} ${profile.name.familyName}`
  const mobile = req.body.mobile

  const options = {
    email,
    photo,
    displayName,
    mobile,
    accessToken,
    profile,
    source: 'facebook',
  }

  updateUser(options).then((result) => {
    done(result.error, result.user, result.info)
  })
}))

/**
 * Sign in with Google.
 */
passport.use(new GoogleStrategy({
  clientID: process.env.GOOGLE_ID,
  clientSecret: process.env.GOOGLE_SECRET,
  passReqToCallback: true
}, (req, accessToken, refreshToken, profile, done) => {
  const email = profile.emails[0].value
  const photo = profile.photos[0].value
  const displayName = profile.displayName
  const mobile = req.body.mobile

  const options = {
    email,
    photo,
    displayName,
    mobile,
    accessToken,
    profile,
    source: 'google'
  }

  updateUser(options).then((result) => {
    done(result.error, result.user, result.info)
  })
}))

/**
 * Login Required middleware.
 */
exports.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.status(401).json({
    msg: 'Unauthorized access. Please login.'
  })
};