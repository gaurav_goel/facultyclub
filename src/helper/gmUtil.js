const _ = require('lodash')
const codeGenerator = require('voucher-code-generator')
const fs = require('fs')
const ffmpeg = require('fluent-ffmpeg')
const gm = require('gm')
const im = require('imagemagick')
const mkdirp = require('mkdirp')
const path = require('path')
const R = require('ramda')

const spawn = require('child_process').spawn

const models = require('../models')
const uploadService = require('../services/upload')
const defaultThumbnailSize = {width: 200, height: 200}

const FILE_STORE = path.join(__dirname, '../../uploads')
const FQ_IMAGES = path.join(__dirname, '../../images/fq')
const FQ_LOGO = 'blacklogo.png';
const PAMPHLATE_TEMPLATE = path.join(__dirname, '../../images/pamphlets/')
const PAMPHLATE_TEMPLATE_1 = 'pamphlet1.png'
const PAMPHLATE_TEMPLATE_2 = 'pamphlet2.png'
const PAMPHLATE_TEMPLATE_3 = 'pamphlet3.png'

const DEFAULT_FOLDER_PERMISSIONS = 493
const TEMPORARY_TEMPLATE = '/tmp/'

const { fileIngest } = require('../../config/application')
const DOWNLOAD_PATH = fileIngest.downloadPath

const generateToken = (length = 8, charset = 'alphanumeric') => codeGenerator.generate({
  length,
  count: 1,
  charset,
})[0]

const getLogoFilePath = logo => {
  console.log('Requesting a logo: ' + logo)
  const ANCHOR = '/egest?file='
  let fileName
  if (logo) {
    fileName = logo.substring(logo.indexOf(ANCHOR) + ANCHOR.length)
    return models.FileMeta.findOne({
      where: {
        filename: fileName,
      },
    }).then(meta => {
      console.log(meta.path)
      return meta ? meta.path : null
    })
  } else {
    return new Promise((resolve, reject) => {
      resolve(null)
    })
  }
}

exports.generateThumbnail = (options, cb) => {
  const width = options.width || defaultThumbnailSize.width
  const height = options.height || defaultThumbnailSize.height

  gm(options.source)
    .resize(width + '^', height + '^')
    .gravity('Center')
    .extent(width, height)
    .write(options.destination, err => {
      if (err) {
        cb(err)
      } else {
        const stats = fs.statSync(options.destination)
        models.FileMeta.build({
          originalName: options.originalName,
          encoding: options.encoding,
          mimetype: options.mimetype,
          filename: options.filename + '.thumb',
          size: stats.size,
          path: options.path + '.thumb',
          uri: DOWNLOAD_PATH + options.filename + '.thumb',
        }).save().then(() => {
          cb(err)
        })
      }
    })
}

exports.generateVideoThumbnail = (options, cb) => {
  const destination = `${FILE_STORE}${options.uploadPath}/${options.filename}.thumb.png`
  try {
    ffmpeg(options.source)
      .on('filenames', function(filenames) {
        console.log('screenshots are ' + filenames.join(', '));
      })
      .on('end', function() {
        const stats = fs.statSync(destination)
        models.FileMeta.build({
          originalName: options.originalName,
          encoding: options.encoding,
          mimetype: 'image/png',
          filename: options.filename + '.thumb',
          size: stats.size,
          path: options.path + '.thumb.png',
          uri: DOWNLOAD_PATH + options.filename + '.thumb',
        }).save().then(() => {
          cb()
        })
      })
      .on('error', function(err) {
        cb(err)
      })
      .takeScreenshots({
        count: 1,
        timemarks: [ '50%' ],
        size: `${defaultThumbnailSize.width}x?`,
        filename: options.filename + '.thumb.png',
      }, `${FILE_STORE}${options.uploadPath}`)
  } catch(err) {
    console.log(err)
    cb(err)
  }
}

exports.createPamphlet = (pamphlet, callback) => {
  const fileName = generateToken() + '.png'
  const filePath = uploadService.getUploadFilePath() + '/' + fileName
  const dest = `${uploadService.getUploadFilePath(FILE_STORE)}/`
  mkdirp.sync(dest, DEFAULT_FOLDER_PERMISSIONS)
  const destination = `${dest}${fileName}`
  // const destination = '/home/gaurav/practice/faculty/123.png'

  getLogoFilePath(pamphlet.logo).then(logoFilePath => {
    let transformations
    let allButLogoArray

    switch(pamphlet.type) {
      case 'type1':
        allButLogoArray = [
          PAMPHLATE_TEMPLATE + PAMPHLATE_TEMPLATE_1,
          // title
          '-size', '250x50',
          '-background', '#4f81be',
          '-fill', '#fff',
          '-font', 'College-Semi-condensed-Regular',
          '-gravity', 'center',
          'caption:' + pamphlet.title,
          '-geometry', '-100-292',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // classes,
          '-size', '370x48',
          '-background', '#4f81be',
          '-fill', '#fff',
          '-gravity', 'center',
          'caption:' + 'Classes : ' + pamphlet.classes,
          // 'caption:' + pamphlet.classes,
          '-geometry', '+243-292',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // description,
          '-size', '320x340',
          '-background', '#d4e3e7',
          '-fill', '#000',
          '-gravity', 'west',
          'caption:' + pamphlet.description,
          '-geometry', '+42+57',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // name
          '-size', '300x55',
          '-background', '#d4e3e7',
          '-fill', '#6F819E',
          '-gravity', 'center',
          'caption:' + pamphlet.name,
          '-geometry', '+80-93',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // brandingText
          '-size', '300x85',
          '-background', '#d4e3e7',
          '-fill', '#6F819E',
          '-gravity', 'center',
          'caption:' + pamphlet.brandingText,
          '-geometry', '+80-5',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // address
          '-size', '320x170',
          '-background', '#d4e3e7',
          '-fill', '#6F819E',
          '-gravity', 'center',
          'caption:' + pamphlet.address,
          '-geometry', '+76+145',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // boards
          '-size', '260x50',
          '-background', '#d4e3e7',
          '-fill', '#6F819E',
          '-gravity', 'east',
          'caption:' + pamphlet.boards,
          '-geometry', '+30-175',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // subjects
          '-size', '173x345',
          '-background', '#d4e3e7',
          '-fill', '#6F819E',
          '-gravity', 'west',
          'caption:' + pamphlet.subjects,
          '-geometry', '+754+58',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // mobile
          '-size', '265x80',
          '-background', '#4f81be',
          '-fill', '#fff',
          '-gravity', 'center',
          'caption:' + pamphlet.mobile,
          '-geometry', '+160+310',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // call now
          '-size', '265x80',
          '-background', '#4f81be',
          '-fill', '#fff',
          '-gravity', 'center',
          'caption:' + 'CALL NOW : ',
          '-geometry', '-110+310',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
        ]
        break
      case 'type2':
        allButLogoArray = [
          PAMPHLATE_TEMPLATE + PAMPHLATE_TEMPLATE_2,
          // title
          '-size', '500x90',
          '-background', '#e36c09',
          '-fill', '#fff',
          '-font', 'College-Semi-condensed-Regular',
          '-gravity', 'center',
          'caption:' + pamphlet.title,
          '-geometry', '-0-280',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // name
          '-size', '300x55',
          '-background', '#fff',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + pamphlet.name,
          '-geometry', '+226-143',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // brandingText
          '-size', '430x85',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + pamphlet.brandingText,
          '-geometry', '+224-36',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // address
          '-size', '430x145',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + pamphlet.address,
          '-geometry', '+226+106',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // mobile
          '-size', '265x80',
          '-background', '#4fcccd',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + pamphlet.mobile,
          '-geometry', '+167+275',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // call now
          '-size', '265x80',
          '-background', '#4fcccd',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + 'CALL NOW : ',
          '-geometry', '-110+275',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
        ]
        break
      case 'type3':
        allButLogoArray = [
          PAMPHLATE_TEMPLATE + PAMPHLATE_TEMPLATE_3,
          // title
          '-size', '800x100',
          '-fill', '#943532',
          '-font', 'College-Semi-condensed-Regular',
          '-gravity', 'center',
          'caption:' + pamphlet.title,
          '-geometry', '-0-300',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // classes,
          '-size', '390x70',
          '-fill', '#943532',
          '-gravity', 'center',
          'caption:' + 'Classes : ' + pamphlet.classes,
          // 'caption:' + pamphlet.classes,
          '-geometry', '+243-175',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // description,
          '-size', '487x200',
          '-fill', '#000',
          '-gravity', 'west',
          'caption:' + pamphlet.description,
          '-geometry', '+230+0',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // address
          '-size', '487x87',
          '-fill', '#943532',
          '-gravity', 'west',
          'caption:' + pamphlet.address,
          '-geometry', '+230+183',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // subjects
          '-size', '173x345',
          '-fill', '#943532',
          '-gravity', 'west',
          'caption:' + pamphlet.subjects,
          '-geometry', '+756+58',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // mobile
          '-size', '265x80',
          '-background', '#c4b97d',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + pamphlet.mobile,
          '-geometry', '+160+310',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
          // call now
          '-size', '265x80',
          '-background', '#c4b97d',
          '-fill', '#000',
          '-gravity', 'center',
          'caption:' + 'CALL NOW : ',
          '-geometry', '-110+310',
          // '-bordercolor', 'black',
          // '-border', '2x2',
          '-composite',
        ]
        break
      default:
        allButLogoArray = []
    }

    if (pamphlet.logo && logoFilePath) {
      transformations = R.concat(allButLogoArray, [
        // logo
        '-gravity', 'west',
        '(',
        FILE_STORE + logoFilePath,
        '-resize', '182x143',
        ')',
        '-geometry', '+0-300',
        // '-bordercolor', 'black',
        // '-border', '2x2',
        '-composite',
      ])
    } else {
      transformations = allButLogoArray
    }
    transformations.push(destination)

    im.convert(transformations, (err, stdout) => {
      // callback()
      const stats = fs.statSync(destination)
      models.FileMeta.build({
        originalName: fileName,
        encoding: '',
        mimetype: 'image/png',
        filename: fileName,
        size: stats.size,
        path: filePath,
        uri: DOWNLOAD_PATH + fileName,
      }).save().then(() => {
        // generate the thumbnail
        gm(destination)
        .resize(defaultThumbnailSize.width + '^', defaultThumbnailSize.height + '^')
        .gravity('Center')
        // .extent(defaultThumbnailSize.width, defaultThumbnailSize.height)
        .write(destination + '.thumb', err => {
          if (err) {
            callback(err)
          } else {
            const stats = fs.statSync(destination + '.thumb')
            models.FileMeta.build({
              originalName: fileName,
              encoding: '',
              mimetype: 'image/png',
              filename: fileName + '.thumb',
              size: stats.size,
              path: filePath + '.thumb',
              uri: DOWNLOAD_PATH + fileName + '.thumb',
            }).save().then(() => {
              pamphlet.url = DOWNLOAD_PATH + fileName
              pamphlet.thumbUrl = DOWNLOAD_PATH + fileName + '.thumb'
              pamphlet.save().then(newPamphlet => {
                callback(err, newPamphlet)
              })
            })
          }
        })
      })
    })
  })
}

// exports.stampPDF = (pdfFile, branding, newFileName, callback) => {
//   const fileName = newFileName + '.pdf'
//   const destination = `/tmp/${fileName}`
//   let logoImage = ''
//   let promise

//   if (branding.logo) {
//     logoImage = branding.logo.substring(logoImage.indexOf('/egest?file=') + 13)
//     console.log(logoImage, '$$$$')
//     promise = models.FileMeta.findOne({
//       where: {
//         filename: logoImage,
//       },
//     }).then((fileObj) => {
//       if (fileObj) {
//         logoImage = `${FILE_STORE}${fileObj.path}`
//         console.log(logoImage, '####')
//       }
//       return
//     })
//   } else {
//     promise = models.sequelize.Promise.all([null])
//   }

//   promise.then(() => {
//     const transformations = [
//       '-density', '200',
//       pdfFile,
//       'null:', '(',
//       logoImage, '-fill', 'rgba(60, 60, 58, 0)', '-colorize', '80', ')',
//       '-fill', 'grey10',
//       '-pointsize', '50',
//       '-gravity', 'Southwest',
//       '-geometry', '+700+500',
//       '-compose', 'dst-out', '-matte', '-layers', 'composite',
//       '-fill', 'rgba(60, 60, 58, 0.2)',
//       '-gravity', 'center',
//       "-draw", "rotate -30 text 0,0 '" + _.startCase(_.toLower(branding.watermark)) + "'",
//       '-pointsize', '20',
//       '-gravity', 'Northwest',
//       '-geometry', '+100+100',
//       '-fill', 'rgba(0, 0, 0)',
//       "-draw", "text 750,150 '" + _.startCase(_.toLower(branding.brandingText)) + "'",
//       // logo
//       // '-gravity', 'west',
//       // '-geometry', '+0-300',
//       // '(',
//       // logoImage,
//       // '-resize', '182x143',
//       // ')',
//       destination
//     ]
//     try {
//       im.convert(transformations, (err, stdout) => {
//         console.log(err)
//         callback(err, destination)
//       })
//     } catch(err) {
//       console.log(err)
//     }
//   })
// }

exports.stampPDF = (pdfFile, branding, newFileName, callback) => {
  const fileName = newFileName + '.pdf'
  const destination = `/tmp/${fileName}`

  const logo = branding.logoImage
  const brandingText = branding.brandingText
  const watermark = branding.watermark

  let promise

  if (branding.logo) {
    let logoImage = branding.logo.substring(branding.logo.indexOf('/egest?file=') + 12)
    promise = [models.FileMeta.findOne({
      where: {
        filename: logoImage,
      },
    }).then((fileObj) => {
      if (fileObj) {
        logoImage = `${FILE_STORE}${fileObj.path}`
      }
      return logoImage
    })]
  } else {
    promise = models.sequelize.Promise.all([null])
  }

  models.sequelize.Promise.all(promise).spread(logoImageAbsolute => {
    const stamper = path.join(__dirname, '../../stamper/stamper-1.0-jar-with-dependencies.jar')
    const command = spawn('java',  ['-jar', `${stamper}`, `${pdfFile}`, `${destination}`, `${branding.watermark ? branding.watermark : ''}`, `${branding.brandingText ? branding.brandingText : ''}`, `${logoImageAbsolute}`, `${FQ_IMAGES}/${FQ_LOGO}`]);
    command.stdout.on('data', data => {
      console.log(`stdout: ${data}`)
    })
    command.stderr.on('data', data => {
      console.log(`stderr: ${data}`)
      callback(data, null)
    })
    command.on('close', code => {
      console.log(`PDF created ${code}`)
      callback(null, destination)
    })
  })
}