const R = require('ramda')

module.exports = class ResponseBuilder {
  constructor(status, message, data, errors) {
  	this.httpStatus = status
    this.status = status >= 200 && status < 300
    this.msg = message

    if (data) {
	    this.response = {
	    	data
	    }
    }

    this.errors = errors

    return this
  }

  getData() {
  	return this.response && this.response.data
  }
  getError() {
  	return this.errors
  }
  getMessage() {
  	return this.msg
  }
  getStatus() {
  	return this.status
  }
  getHTTPStatus() {
  	return this.httpStatus
  }

  build() {
  	const responseObject = {}
  	responseObject.status = this.status
  	if (this.response) {
  		responseObject.data = this.response && this.response.data
  	}
  	if (this.errors && this.errors.length) {
  		responseObject.errors = this.errors
  	}
  	responseObject.msg = this.msg
  	return responseObject
  }

  setData(data) {
  	this.response = {
  		data,
  	}
  	return this
  }
  
  setErrors(errors) {
  	if (errors) {
  		if (errors instanceof Array) {
	  		this.errors = []
	  		let mapperFunction
	  		if (errors[0] instanceof Object) {
	  			mapperFunction = err => err.msg
	  		} else if (errors[0] instanceof String || typeof errors[0] === 'string') {
	  			mapperFunction = err => err
	  		}
				R.forEach((error) => {
					this.errors.push(mapperFunction(error))
				}, errors)
  		} else if (errors instanceof String) {
  			this.errors = [errors]
  		}
  	}
  	return this
  }

  setMessage(message) {
  	this.msg = message
  	return this
  }

  setStatus(status) {
  	this.httpStatus = status
    this.status = status >= 200 && status < 300
    return this
  }
}
