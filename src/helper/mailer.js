const mailConfig = require('../../config/application').mail
const mongoose = require('mongoose')
const nodemailer = require('nodemailer')
const smtpTransport = require('nodemailer-smtp-transport')

const { mail } = require('../../config/application')

const transporter = nodemailer.createTransport({
  service: "Gmail",
  auth: {
    user: mail.user,
    pass: mail.pass,
  },
});

const mailLog = new mongoose.Schema({
  tos: { type: String, required: true },
  subject: { type: String, required: true },
  body: { type: String, required: true },
  success: { type: Boolean, required: true, default: true },
  retry: { type: Number, default: 0 },
}, { timestamps: true })
const MailLog = mongoose.model('MailLog', mailLog)

const logMail = (to, subject, body) => {
  const mailLogConst = new MailLog({
    tos: to,
    subject,
    body,
    success: false,
  })
  mailLogConst.save((err) => {
    if (err) {
      console.log(err)
    }
  })
}

exports.sendMail = (to, subject, message) => {
  if (!mailConfig.allowMailSend) {
    console.log('Mail Sent Log.', to, subject, message)
    return
  }

  const mailOptions = {
    to,
    from: 'noreply@faculty.club',
    subject,
    text: message,
  }

  transporter.sendMail(mailOptions, (err) => {
    if (err) {
      console.log(err)
      // logMail(to, subject, mailOptions.body)
    }
    else {
      console.log('Mail Sent.', to, subject, message)
    }
  })
}
