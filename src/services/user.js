const models = require('../models')
const request = require('request')

const getTuitionScopedPromises = (user) => {
  const promises = []
  promises.push(user.getOneOnOneTutionCharge())
  promises.push(user.getGroupTutionCharge())
  promises.push(user.getWeekdayTutionTime())
  promises.push(user.getWeekendTutionTime())
  promises.push(user.getTutorBoard())
  promises.push(user.getTutorTeachingMode())
  promises.push(user.getTutorTeachingLanguage())
  promises.push(user.getTutorSchool())
  promises.push(models.TutorCourseDetail.findAll({
    where: {
      tutorId: user.id,
    },
  }))

  return promises
}

const getTutionDetails = user => {
  const promises = getTuitionScopedPromises(user)
  return models.sequelize.Promise.all(promises)
  .spread((tutionCharge, groupTutionCharge, weekdayTime, weekendTime, teachingBoards, teachingModes,
      teachingLanguages, tutorSchools, courseDetails) => {
    user.oneOnOneTutionCharge = tutionCharge
    user.groupTutionCharge = groupTutionCharge
    user.weekdayTutionTime = weekdayTime
    user.weekendTutionTime = weekendTime
    user.tutorCourseDetail = courseDetails
    user.tutorBoard = teachingBoards
    user.tutorTeachingMode = teachingModes
    user.tutorTeachingLanguage = teachingLanguages
    user.tutorSchool = tutorSchools

    return user
  }).catch(function(err) {
    console.log(err)
    return null
  })
}

function getUser(id) {
  return models.User.findById(id).then(user => user)
}

function getUserReviewsCount(userId) {
  return models.TutorUserReviews.count({
    where: {
      tutorId: userId,
    },
  })
}

function getUserContentCount(userId) {
  return models.TutorContentUpload.count({
    where: {
      tutorId: userId,
    },
  })
}

exports.getUser = getUser

exports.updateSearchScore = (userId) => {
  const url = `${process.env.LEGACY_SERVER_URI}/api/updatesearchscore/${userId}`
  request({
    rejectUnauthorized: false,
    method: 'PUT',
    url,
  }, function (error, response, body) {
    if (!error && response) {
      console.log('Status:', response.statusCode);
      console.log('Headers:', JSON.stringify(response.headers));
      console.log('Response:', body);
    } else {
      console.log('Error', error)
    }
  }, function (error) {
  	console.log(error)
  })
}

exports.calculateProfileCompleteness = userId => {
  let userCache
  return models.User.scope('detailed').findById(userId).then(user => {
    userCache = user
    console.log(user ? 'There is user' : 'Test test')
    if (userCache) {
      userCache.reduceProfileCompleteness = 'personalDetail'
      userCache.reduceProfileCompleteness = 'qualification'
      userCache.reduceProfileCompleteness = 'tuition'
      userCache.reduceProfileCompleteness = 'location'
      userCache.reduceProfileCompleteness = 'social'
      userCache.reduceProfileCompleteness = 'review'
      userCache.reduceProfileCompleteness = 'content'

      if (userCache.name && (userCache.invitationStatus === null || userCache.invitationStatus === 1)) {
        userCache.profileCompleteness = 'personalDetail'
      }
      if (userCache.tutorEducationDetail && userCache.tutorEducationDetail.length) {
        userCache.profileCompleteness = 'qualification'
      }
      if (userCache.city && userCache.locality) {
        userCache.profileCompleteness = 'location'
      }
      if (userCache.fblink || userCache.lnlink || userCache.twitterlink) {
        userCache.profileCompleteness = 'social'
      }

      return getTutionDetails(userCache)
    } else {
      return null
    }
  }).then(userWithTuitionInfo => {
    if (userWithTuitionInfo) {
      if (userWithTuitionInfo.oneOnOneTutionCharge || userWithTuitionInfo.groupTutionCharge
        || userWithTuitionInfo.weekdayTutionTime || userWithTuitionInfo.weekendTutionTime
        || userWithTuitionInfo.tutorCourseDetail) {
        userCache.profileCompleteness = 'tuition'
      }
      return getUserReviewsCount(userId)
    } else {
      return 0
    }
  }).then(reviewCount => {
    if (reviewCount > 0) {
      userCache.profileCompleteness = 'review'
      return getUserContentCount(userId)
    } else {
      return 0
    }
  }).then(userContentCount => {
    if (userContentCount > 0) {
      userCache.profileCompleteness = 'content'
    }
    if (userCache) {
      return userCache.save()
    } else {
      return userCache
    }
  })
}