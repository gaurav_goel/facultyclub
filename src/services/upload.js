const models = require('../models')
const Q = require('q')
const path = require('path')
const GMUtils = require('../helper/gmUtil')
const FILE_STORE = path.join(__dirname, '../../uploads')
const ResponseBuilder = require('../helper/response-builder')

const { fileIngest } = require('../../config/application')
const DOWNLOAD_PATH = fileIngest.downloadPath

/**
 * Uses the file path to convert to a path where the file should be uploaded.
 *
 * @param {String} filePath
 **/
const getUploadFilePath = (filePath = '') => {
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hours = date.getHours()
  return `${filePath}/${year}/${month}/${day}/${hours}`
}

exports.getUploadFilePath = getUploadFilePath

exports.validateFileUpload = (err, req) => {
  if (err || !req.file) {
    let errMsg
    if (err && err.code === 'LIMIT_FILE_SIZE') {
      errMsg = `Maximum file size allowed is ${maxFileSize}MB.`
    }
    else {
      errMsg = err || 'Invalid or no file supplied'
    }
    return new ResponseBuilder()
      .setStatus(400)
      .setMessage('Invalid upload request')
      .setErrors([errMsg])
  }
  return null
}

// create file meta
exports.createFileMeta = (file) => {
  const deferred = Q.defer()
  const { originalname, encoding, mimetype, filename, size } = file
  const uploadPath = getUploadFilePath()
  const filePath = uploadPath + '/' + filename

  const metaProps = {
    originalName: originalname,
    encoding,
    mimetype,
    filename,
    size,
    path: filePath,
    uri: DOWNLOAD_PATH + filename,
  }

  const thumbnailOptions = {
    source: `${FILE_STORE}${filePath}`,
    destination: `${FILE_STORE}${filePath}.thumb`,
    originalName: originalname,
    encoding,
    mimetype,
    filename,
    size,
    path: filePath,
    uri: DOWNLOAD_PATH + filename,
    uploadPath,
  }

  if (mimetype.indexOf('image') > -1) {
    GMUtils.generateThumbnail(thumbnailOptions, err => {
      if (err) {
        console.log(err)
        throw new Error(err)
      }
      deferred.resolve(models.FileMeta.build(metaProps).save())
    })
  }
  else if (mimetype.indexOf('video') > -1) {
    GMUtils.generateVideoThumbnail(thumbnailOptions, err => {
      if (err) {
        // console.log(err)
        throw new Error(err)
      }
      deferred.resolve(models.FileMeta.build(metaProps).save())
    })
  }
  else {
    deferred.resolve(models.FileMeta.build(metaProps).save())
  }

  return deferred.promise
}
