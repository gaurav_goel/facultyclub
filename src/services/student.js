const Q = require('q')
const R = require('ramda')
const models = require('../models')
const smsService = require('./sms')

const GROUP_STUDENT_QUERY =
`(select u.id, u.name, u.date_of_birth, u.invitation_status, u.phone
  from student_groups sg
  inner join student_group_map sgm
  on sg.id = sgm.group_id
  inner join user u
  on u.id = sgm.student_id
  where sg.tutor_id = :tutor_id
  and sgm.student_id in (:student_ids))
  union
  (select u.id, u.name, u.date_of_birth, u.invitation_status, u.phone
  from student_groups sg
  inner join group_invite sgm
  on sg.id = sgm.group_id
  inner join user u
  on u.id = sgm.student_id
  where sg.tutor_id = :tutor_id
  and sgm.student_id in (:student_ids))
  union
  (select id, name, date_of_birth, invitation_status, phone
  from user where id in (:student_ids_2) and is_group_invite = 1)`

const GROUP_STUDENT_QUERY_EXCEPT_GROUP =
`(select u.id, u.name, u.date_of_birth, u.invitation_status, u.phone
  from student_groups sg
  inner join student_group_map sgm
  on sg.id = sgm.group_id
  inner join user u
  on u.id = sgm.student_id
  where sg.tutor_id = :tutor_id
  and sgm.student_id in (:student_ids) and sg.id != :group_id)`

exports.createStudentInfo = (student, pupilId, tutor, isGroupInvite = 0) => {
  const studentProps = models.User.getEmptyUser()
  studentProps.name = student.name
  studentProps.phone = student.phone
  studentProps.email = student.email
  studentProps.inviterId = tutor.id
  studentProps.pupilInfoId = pupilId
  studentProps.isGroupInvite = isGroupInvite
  studentProps.invitationStatus = 0
  studentProps.userType = 4

  // send an invite to the user
  smsService.smsUserInvite(student, tutor, false)

  return models.User.create(studentProps)
}

exports.resendStudentInvite = (student, tutor) => {
  smsService.smsUserInvite(student, tutor, true)
}

exports.removeIndividualStudent = (tutorId, studentId) => models.StudentTeacherMap.destroy({
  where: {
    tutorId,
    studentId,
  },
})

exports.areGroupStudents = (students, tutorId) => {
  const deferred = Q.defer()
  const studentIDs = R.join(',', R.pluck('id')(students))
  models.sequelize.query(GROUP_STUDENT_QUERY, {
    replacements: { tutor_id: tutorId, student_ids: studentIDs, student_ids_2: studentIDs },
    model: models.User,
  }).then(groupStudents => {
    if (groupStudents) {
      deferred.resolve(R.pluck('phone')(groupStudents))
    }
    else {
      deferred.resolve()
    }
  })
  return deferred.promise
}

exports.isGroupStudentAlready = (students, tutorId) => {
  const deferred = Q.defer()
  models.User.findAll({
    where: {
      phone: {
        $in: R.pluck('phone')(students),
      },
    },
  }).then(users => {
    const studentIDs = R.join(',', R.pluck('id')(users))
    models.sequelize.query(GROUP_STUDENT_QUERY, {
      replacements: { tutor_id: tutorId, student_ids: studentIDs, student_ids_2: studentIDs },
      model: models.User,
    }).then(groupStudents => {
      if (groupStudents) {
        deferred.resolve(R.pluck('phone')(groupStudents))
      }
      else {
        deferred.resolve()
      }
    })
  })
  return deferred.promise
}

exports.areOtherGroupStudentsAlready = (students, tutorId, groupId) => {
  const deferred = Q.defer()
  models.User.findAll({
    where: {
      phone: {
        $in: R.pluck('phone')(students),
      },
    },
  }).then(users => {
    const studentIDs = R.join(',', R.pluck('id')(users))
    models.sequelize.query(GROUP_STUDENT_QUERY_EXCEPT_GROUP, {
      replacements: { tutor_id: tutorId, student_ids: studentIDs, group_id: groupId },
      model: models.User,
    }).then(groupStudents => {
      if (groupStudents) {
        deferred.resolve(R.pluck('phone')(groupStudents))
      }
      else {
        deferred.resolve()
      }
    })
  })
  return deferred.promise
}

exports.filterIndividualStudents = (students, tutorId) => {
  const deferred = Q.defer()

  models.User.findAll({
    where: {
      phone: {
        $in: R.pluck('phone')(students),
      },
      invitationStatus: 1,
    },
  }).then(users => {
    if (users && users.length) {
      models.StudentTeacherMap.findAll({
        where: {
          tutorId,
          studentId: {
            $in: R.pluck('id')(users),
          },
        },
        include: [
          {
            model: models.User,
            as: 'student',
          },
        ],
      }).then(relations => {
        if (relations) {
          deferred.resolve(R.pluck('student')(relations))
        }
        else {
          deferred.resolve([])
        }
      })
    }
    else {
      deferred.resolve([])
    }
  })

  return deferred.promise
}

exports.filterGroupStudents = (students, tutorId) => {
  const deferred = Q.defer()

  models.User.findAll({
    where: {
      phone: {
        $in: R.pluck('phone')(students),
      },
    },
  }).then(users => {
    if (users && users.length) {
      models.StudentTeacherMap.findAll({
        where: {
          tutorId,
          studentId: {
            $in: R.pluck('id')(users),
          },
        },
        include: [
          {
            model: models.User,
            as: 'student',
          },
        ],
      }).then(relations => {
        if (relations) {
          deferred.resolve(R.pluck('student')(relations))
        }
        else {
          deferred.resolve([])
        }
      })
    }
    else {
      deferred.resolve([])
    }
  })

  return deferred.promise
}

exports.validateNewInvites = (phones, emails, err) => {
  if (phones && phones.length && phones.length !== R.uniq(phones).length) {
    return err || 'Two or more invitees have the same phone numbers'
  }
  else if (emails && emails.length && emails.length !== R.uniq(emails).length) {
    return 'Two or more invitees have the same email addresses'
  }
  return null
}
