const R = require('ramda')
const models = require('../models')

exports.getAllTags = () =>
  models.Tags.findAll({})

exports.deleteTag = id =>
  models.Tags.destroy({
    where: {
      id,
    },
  })

exports.createTag = obj =>
  models.Tags.create(obj)

exports.getTag = tag =>
  models.Tags.findOne({
    where: {
      tag,
    }
  })

exports.getTags = (offset = 0, limit = 100) =>
  models.Tags.findAll({
    limit: +(limit),
    offset: +(offset),
    order: [
      ['score', 'DESC'],
      ['tag', 'ASC']
    ],
  })