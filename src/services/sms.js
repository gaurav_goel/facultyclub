const request = require('request')
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';
const transactionService = require('./transaction')

function encrypt(text) {
  var cipher = crypto.createCipher(algorithm, password);
  var crypted = cipher.update(text, 'utf8', 'hex');
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text) {
  var decipher = crypto.createDecipher(algorithm, password);
  var dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
}

const getReviewLink = (inviter, contact) => {
  const encryptedPart = encrypt(`${inviter.id || ''},${contact || ''}`)
  return `${process.env.LEGACY_SERVER_URI}/enrol/review/${encryptedPart}`
}

const getProfileLink = inviter =>
  `https://faqulty.club/profile/${inviter.name ? inviter.name.replace(' ', '') : ''}/${inviter.id}`

const sendSMS = (phone, smsBody) => {
  const url = `${process.env.SMS_URI}?message=${encodeURIComponent(smsBody)}&username=${process.env.SMS_USERNAME}&password=${process.env.SMS_PASSWORD}&sender=${process.env.SMS_SENDER}&sendto=${phone}`
  request({
    method: 'GET',
    url,
  }, function (error, response, body) {
    console.log('Status:', response.statusCode);
    console.log('Headers:', JSON.stringify(response.headers));
    console.log('Response:', body);
  }, function (error) {
  	console.log(error)
  })
}

exports.smsToken = (user, code, message) => {
  const smsBody = `${message ? message + ': ' : 'Token: '}${code}`
  const phone = user.phone
  console.log(`Sending SMS to '${phone}' with text '${smsBody}'`)
  sendSMS(phone, smsBody)
}

exports.smsOTP = (user, code) => {
  const smsBody = `${code} is the Onetime Password (OTP) for signing up into the Faqulty app. This is usable once.`
  const phone = user.phone
  console.log(`Sending SMS to '${phone}' with text '${smsBody}'`)
  sendSMS(phone, smsBody)
}

exports.smsResetPassword = (user, code) => {
  const smsBody = `Please use the code ${code} to reset your password.`
  const phone = user.phone
  console.log(`Sending SMS to '${phone}' with text '${smsBody}'`)
  sendSMS(phone, smsBody)
}

exports.smsUserInvite = (invitee, inviter, isResend = true) => {
  const profileLink = getProfileLink(inviter)
  let smsBody
  if (isResend) {
    smsBody = `Hi, ${inviter.name} has invited you to join his student group at ${process.env.STUDENT_APP_LINK} - Reminder by The Faqulty Club on behalf of ${inviter.name}`
  } else {
    smsBody = `Hi, ${inviter.name} has invited you to join his student group at ${process.env.STUDENT_APP_LINK} - Sent by The Faqulty Club on behalf of ${inviter.name}`
  }
	const phone = invitee.phone
	console.log(`Sent user invite sms to ${invitee.phone} - ${smsBody}`)
	sendSMS(phone, smsBody)
}

exports.smsReviewInvite = (contact, inviter) => {
  const profileLink = getReviewLink(inviter, contact)
  const smsBody = `Hi, ${inviter.name} has requested you to review their work at ${profileLink} - Sent by The Faqulty Club on behalf of ${inviter.name}`
  const phone = contact
  console.log(`Sent request invite sms to ${contact} - ${smsBody}`)
  sendSMS(phone, smsBody)
}

exports.smsSendTransactionReminder = (inviter, invitee, pendingAmount, type = 'reminder') => {
  let smsBody = transactionService.getTransactionMessage(
    type, inviter, pendingAmount
  )
  const phone = invitee.phone
  console.log(`Sent transaction reminder to ${phone} - ${smsBody}`)
  sendSMS(phone, smsBody)
}
