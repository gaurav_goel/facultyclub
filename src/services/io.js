const R = require('ramda')
const _ = require('lodash')
const https = require('https')
const config = require('../../config/application')
const models = require('../models')
const groupService = require('../services/group')


/*
  Listing all the events here:
    1. addUser
        {
          "fromUserId": 1
        }
    2. message
      {
        "fromUserId": 3,
        "toUserId": 1,
        "message": "hi5",
        "messageId": "5"
      }
    3. delivered, here to is the from of message event
      {
        "toUserId": 1,
        "messageId": "5"
      }
    4. receive
    5. messageReceived
    6. pendingMessages
*/

const fetchPendingMessages = userId =>
  models.ChatMessages.findAll({
    where: {
      to: userId,
      status: 'new',
    },
  }).then(chatMessages => chatMessages)

const createMessage = (data, toUserId) => {
  return models.ChatMessages.create({
    data: JSON.stringify(data),
    from: data.fromUserId,
    to: toUserId || data.toUserId,
    messageId: data.messageId,
    status: 'new',
  }).then(chatMessage => {
    console.log(`Message saved. ID - ${chatMessage.id}, message ID - ${chatMessage.messageId}.`)
  }).catch(err => {
    console.log(err)
  })
}

const markMessageDelivered = data =>
  models.ChatMessages.findOne({
    where: {
      messageId: data.messageId,
    },
  }).then(message => {
    if (message) {
      message.status = 'delivered'
      return message.save()
    }
    return null
  }).then(message => {
    if (message) {
      console.log(`Message ${message.messageId} has been delivered.`)
    }
  })

const getPushToken = userId => {
  return models.PushTokens.findOne({
    where: {
      userId,
    },
  }).then(pushToken => {
    return pushToken ? pushToken : null
  })
}

exports.setup = io => {
  // maps the user Id to sockets
  const sockets = {}
  // maps the socket IDs to sockets
  const socketByID = {}
  // maps socket IDs to user IDs
  const socketID2UserIDMap = {}

  const cacheSocket = (fromId, socket) => {
    if (fromId) {
      sockets[fromId] = socket
      socketByID[socket.id] = socket
      socketID2UserIDMap[socket.id] = fromId
    }
  }

  const emit = (toId, message) => {
    if (toId && sockets[toId]) {
      sockets[toId].emit('receive', message)
    } else if (toId) {
      getPushToken(toId).then(fcmToken => {
        if (fcmToken.fcmToken) {
          const request = https.request({
            headers: config.pushNotifications.headers[fcmToken.userType],
            host: config.pushNotifications.url,
            path: config.pushNotifications.path,
            protocol: config.pushNotifications.protocol,
            method: 'POST',
          }, function(res) {
            console.log('Push status: ' + res.statusCode);
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
              console.log('Push API Response: ' + chunk);
            });
          })

          request.write(JSON.stringify({
            to: fcmToken.fcmToken,
            data: {
              message : message,
              title : 'Received chat message',
            }
          }))
          request.end()
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }

  const emitToGroup = (gid, userId, message) => {
    groupService.getAllGroupMembers(gid).then(members => {
      if (members) {
        R.forEach(member => {
          if (userId !== member) {
            emit(member, message)
            const toUserId = message.toUserId
            createMessage(message, member)
          }
        }, R.uniq(members))
      }
    })
  }

  const emitReceived = (toId, message) => {
    if (toId && sockets[toId]) {
      sockets[toId].emit('messageReceived', message)
    }
  }

  const emitPendingMessages = (toId, messages) => {
    if (toId && sockets[toId] && messages && messages.length) {
      sockets[toId].emit('pendingMessages', {
        messages: R.map(message => {
          try {
            return JSON.parse(message.data)
          } catch(err) {
            console.log(err)
            return {};
          }
        }, messages)
      })
    }
  }

  const destroy = socketId => {
    const userId = socketID2UserIDMap[socketId]
    if (userId) {
      delete sockets[userId]
    }
    if (socketId) {
      delete socketByID[socketId]
    }
  }

  io.on('connection', function(client) {
    client.on('addUser', function(data) {
      try {
        const userId = data.fromUserId
        cacheSocket(userId, client)
        fetchPendingMessages(userId).then(messages => {
          emitPendingMessages(userId, messages)
        })
      } catch(err) {
        console.log(err)
      }
    })

    client.on('message', function(data) {
      try {
        const chatType = data.chatType
        if (data.fromUserId) {
          cacheSocket(data.fromUserId, client)
        }
        if (chatType === 'GROUP' && data.groupInfo && data.groupInfo.gid) {
          emitToGroup(data.groupInfo.gid, data.fromUserId, data)
        } else {
          if (data.fromUserId != data.toUserId) {
            emit(data.toUserId, data)
          }
          createMessage(data, undefined)
        }
      } catch(err) {
        console.log(err)
      }
    })

    client.on('delivered', function(data) {
      if (data.toUserId) {
        emitReceived(data.toUserId, data)
      }
      if (data.messageId) {
        markMessageDelivered(data)
      }
    })

    client.on('deliveredBulk', function(data) {
      if (data.toUserId) {
        emitReceived(data.toUserId, data)
      }
      if (data.messageId) {
        markMessageDelivered(data)
      }
    })

    client.on('disconnect', function() {
      destroy(client.id)
    })
  })
}