const Q = require('q')
const R = require('ramda')
const models = require('../models')

const TYPE_FEED_LIKE = 'feed_like'
const TYPE_FEED_COMMENT = 'feed_comment'

const createFeedLikeMessage = options => `${options.actor} liked your post "${options.feed}"`

const createFeedCommentMessage = options => `${options.actor} commented on your post "${options.feed}"`

const createNotificaitonMessageByType = (type, options) => {
  switch(type) {
    case TYPE_FEED_LIKE:
      return createFeedLikeMessage(options)
    case TYPE_FEED_COMMENT:
      return createFeedCommentMessage(options)
    default:
      throw 'Unknown notification type'
  }
}

const transformNotification = notification => {
  if (notification.tutor) {
    notification.user = {
      id: notification.user.id,
      name: notification.user.name,
      image: notification.user.image,
      facultyProfileURI: notification.user.facultyProfileURI,
    }
  }
  return notification
}

const transformNotifications = notifications => R.map(notification => transformNotification(notification), notifications)

exports.createNotification = (type, options, userId) => {
  if (userId !== options.actorId) {
    const props = {
      type,
      message: createNotificaitonMessageByType(type, options),
      createdOn: new Date(),
      seen: 0,
      userId,
    }
    
    return models.Notifications.create(props)
  }
  return null
}

exports.deleteNotification = (id, userId) => {
  return models.Notifications.destroy({
    where: {
      id,
      userId,
    }
  })
}

exports.deleteAllNotification = userId => {
  return models.Notifications.destroy({
    where: {
      userId,
    }
  })
}


exports.markSeen = (id, userId) => {
  return models.Notifications.update({
      seen: 1,
    },
    {
      where: {
        userId,
        id,
      }
    }
  )
}

exports.markAllSeen = userId => {
  return models.Notifications.update({
      seen: 1,
    },
    {
      where: {
        userId,
      }
    }
  )
}

exports.fetchNotifications = (userId, offset = 0, limit = 20) => {
  return transformNotifications(models.Notifications.scope('base').findAll({
    where: {
      userId,
    },
    limit: +(limit),
    offset: +(offset),
    order: [
      ['createdOn', 'DESC']
    ],
  }))
}

exports.TYPE_FEED_LIKE = TYPE_FEED_LIKE
exports.TYPE_FEED_COMMENT = TYPE_FEED_COMMENT
