const R = require('ramda')
const Q = require('q')
const models = require('../models')

// fetches all the groups, the student is a part of for a given tutor
const TUTOR_STUDENT_GROUP_QUERY =
`select sg.id, sg.name, sg.tutor_id, sg.pupil_info_id, sg.created_on
from student_groups sg
inner join student_group_map sgm
on sg.id = sgm.group_id
where tutor_id = :tutor_id
and sgm.student_id = :student_id`

const FETCH_GROUP_PUPIL_INFO =
`select pupil_info_id as id from student_groups sg
inner join student_group_map sgm
on sg.id = sgm.group_id
inner join pupil_info pi
on sg.pupil_info_id = pi.id
where sg.tutor_id = :tutor_id and sgm.student_id = :student_id`


exports.getAllGroupMembers = groupId => {
  const deferred = Q.defer()
  const promises = [];
  promises.push(models.StudentGroupMap.findAll({
    where: {
      groupId,
    },
  }))
  promises.push(models.StudentGroups.findById(groupId))
  models.sequelize.Promise.all(promises).spread((groupMaps, group) => {
    const memberIds = []
    if(groupMaps) {
      groupMaps.forEach(g => memberIds.push(g.studentId))
    }
    memberIds.push(group.tutorId)
    deferred.resolve(memberIds)
  })
  return deferred.promise
}

exports.getGroupMembers = groupId =>
  models.StudentGroupMap.findAll({
    where: {
      groupId,
    },
  }).then(members => members)

exports.createGroupInvite = (studentId, tutorId, groupId = null) => {
  models.GroupInvite.destroy({
    where: {
      studentId,
      tutorId,
    },
  }).then(() => {
    const groupInvite = {
      studentId,
      tutorId,
      groupId,
    }

    return models.GroupInvite.create(groupInvite)
  })
}

exports.getTutorStudentGroups = (studentId, tutorId) => models.sequelize.query(
  TUTOR_STUDENT_GROUP_QUERY,
  {
    replacements: { tutor_id: tutorId, student_id: studentId },
    model: models.StudentGroups,
  }
)

exports.getGroupPupilInfo = (studentId, tutorId) => models.sequelize.query(
  FETCH_GROUP_PUPIL_INFO,
  {
    replacements: { tutor_id: tutorId, student_id: studentId },
    type: models.sequelize.QueryTypes.SELECT,
  }
)

exports.validateStudents = students => {
  return models.User.findAll({
    where: {
      $and: [
        {
          phone: {
            $in: R.map(phone => parseInt(phone), R.pluck('phone')(students)),
          },
        },
        {
          userType: 2,
        },
      ],
    },
  }).then(tutors => {
    if (tutors && tutors.length) {
      const tutorPhones = R.pluck('phone')(tutors)
      return `Following contact numbers are already registered as tutors with us - ${tutorPhones.join(', ')}`
    }
    return null
  })
}

exports.removeGroup = (groupId, tutorId) => {
  models.GroupInvite.destroy({
    where:{
      groupId,
    }
  }).then(() => {
    console.log('Group invite successfully deleted.')
  })
  return models.StudentGroupMap.findAll({
    where: {
      groupId,
    },
    include: [
      {
        model: models.User,
        as: 'student'
      }
    ],
  }).then(studentGroupMaps => {
    const promises = []
    R.forEach(studentGroupMap => {
      models.GroupInvite.destroy({
        where:{
          tutorId: studentGroupMap.tutorId,
          studentId: studentGroupMap.studentId,
        }
      }).then(() => {
        console.log('Group invite successfully deleted.')
      })
      const student = studentGroupMap.student
      if (tutorId
        && (tutorId == student.inviterId)
      ) {
        student.isGroupInvite = 0;
        promises.push(student.save())
      }
    }, studentGroupMaps)

    return models.sequelize.Promise.all(promises).then(() => {
      return models.StudentGroups.destroy({
        where: {
          id: groupId,
          tutorId,
        },
      })
    })
  })
}
