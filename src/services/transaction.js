const Q = require('q')
const R = require('ramda')
const models = require('../models')

const MONEY_OWED_QUERY =
`select type, sum(amount) as amount from transactions
where student_id = :student_id
and tutor_id = :tutor_id
and type in ('request', 'receive')
group by type`

const moneyValue = value => Number(
  parseFloat(Math.round(value * 100) / 100).toFixed(2)
)

const getMoneyOwedByStudent = (tutorId, studentId) => models.sequelize.query(
  MONEY_OWED_QUERY,
  {
    replacements: { tutor_id: tutorId, student_id: studentId },
    model: models.Transactions,
  }
)

exports.fetchTransaction = id => {
  return models.Transaction.findById(id).then(transaction => {
    return transaction
  })
}

exports.fetchStudentTransactions = (tutorId, studentId) => {
  return models.Transaction.findAll({
    where: {
      tutorId,
      studentId
    }
  }).then(transactions => {
    return transactions
  })
}

exports.createTransaction = transaction => {
  return models.Transaction.create(transaction)
}

exports.updateTransaction = (id, props) => {
  return models.Transaction.findById(id).then(transaction => {
    if (!transaction) {
      throw 'Invalid transaction ID'
    }
    return transaction.updateAttributes(props)
  })
}

exports.deleteTransaction = (id) => {
  return models.Transaction.destroy({
    where: {
      id,
    }
  })
}

exports.getMoneyOwed = (tutorId, studentId) =>
  getMoneyOwedByStudent(tutorId, studentId).then(results => {
    if (results) {
      let moneyOwed = 0
      let moneyReceived = 0
      const transactions = results[0] ? results[0] : []
      R.forEach(transaction => {
        // console.log(studentId, type, amount)
        if (transaction.type === 'receive') {
          moneyReceived += transaction.amount
        }
        if (transaction.type === 'request') {
          moneyOwed += transaction.amount
        }
      }, transactions)
      return moneyValue(moneyOwed - moneyReceived)
    }
    return null
  })

exports.getTransactionMessage = (type, inviter, pendingAmount) => {
  let smsBody
  switch (type) {
    case 'request':
      smsBody = `Hi, ${inviter.name} has requested payment for the due amount of Rs. ${pendingAmount} - Sent by The Faqulty Club on behalf of ${inviter.name}`
      break
    case 'reminder':
      smsBody = `This is a kind reminder for payment of Rs. ${pendingAmount}. Please pay at the earliest - Sent by The Faqulty Club on behalf of ${inviter.name}`
      break
    case 'receive':
      smsBody = `Thanks for making the payment of Rs. ${pendingAmount}. to ${inviter.name} - Sent by The Faqulty Club on behalf of ${inviter.name}`
      break
  }

  return smsBody
}