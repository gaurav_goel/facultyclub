const R = require('ramda')
const models = require('../models')

exports.transformSingleFeed = feed => {
  if (feed.tutor) {
    feed.tutor = {
      id: feed.tutor.id,
      name: feed.tutor.name,
      image: feed.tutor.image,
      facultyProfileURI: feed.tutor.facultyProfileURI,
    }
  }
  if (feed.tags) {
    feed.tags = R.map(tag => tag.tag, feed.tags)
  }
  if (feed.subjects && feed.subjects.length) {
    feed.subjects = R.map(subjectWrapper => {
      return {
        id: subjectWrapper.subject.id,
        ceatedOn: subjectWrapper.subject.createdOn,
        subject: subjectWrapper.subject.subject,
      }
    }, feed.subjects)
  }
  if (feed.classes && feed.classes.length) {
    feed.classes = R.map(classWrapper => {
      return classWrapper.class
    }, feed.classes)
  }
  return feed
}

exports.fetchMyLikes = (userId, feeds) => {
  const feedIds = R.pluck('id')(feeds)

  return models.Likes.findAll({
    where: {
      moduleId: {
        $in: feedIds,
      },
      moduleType: 'feed',
      userId,
    }
  }).then(likes => likes)
}
