const R = require('ramda')
const models = require('../models')

exports.getUserBranding = userId =>
  models.Branding.findOne({
    where: {
      userId,
    },
  })

exports.deleteBranding = userId =>
  models.Branding.destroy({
    where: {
      userId,
    },
  })

exports.createBranding = obj =>
  models.Branding.create(obj)
