const _ = require('lodash')

exports.createOrUpdate = (model, props, isUser) => {
  if (!props) {
    return
  }

  const whereCondition = {}
  if (isUser) {
    whereCondition.id = props.id
  } else {
    whereCondition.tutor_id = props.tutor_id
  }
  const create = () => model.create(props)
  let returnPromise

  return model.findOne({
    where: whereCondition,
  })
  .then((obj) => {
    delete props.id
    if (!obj) {
      return create()
    }
    else {
      return obj.updateAttributes(props)
    }
  })
}

exports.createOrUpdateStudentSession = (model, props) => {
  if (!props) {
    return
  }

  const whereCondition = {
    sessionId: props.id
  }
  const create = () => model.create(props)
  let returnPromise

  return model.findOne({
    where: whereCondition,
  })
  .then((obj) => {
    delete props.id
    if (!obj) {
      return create()
    }
    else {
      return obj.updateAttributes(props)
    }
  })
}

/*
 * Finds out the IDs which are to be deleted. input is the array provided by user and persisted is the already saved array.
 */
exports.oneToManyCleaner = (input, persisted) => {
  const inputValues = input || []
  const savedValues = persisted || []
  const inputs = []
  const saved = []
  inputValues.map((val) => {
    if (val.id) {
      inputs.push(val.id)
    }
  })
  savedValues.map((val) => {
    if (val.id) {
      saved.push(val.id)
    }
  })

  return _.difference(saved, inputs)
}