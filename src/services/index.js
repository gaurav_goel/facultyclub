const StudentService = require('./student')
const HelperService = require('./helper')
const GroupService = require('./group')
const SMSService = require('./sms')
const UserService = require('./user')
const TransactionService = require('./transaction')
const PamphletService = require('./pamphlet')

exports.StudentService = StudentService
exports.HelperService = HelperService
exports.GroupService = GroupService
exports.SMSService = SMSService
exports.UserService = UserService
exports.TransactionService = TransactionService
exports.PamphletService = PamphletService