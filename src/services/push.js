const _ = require('lodash')
const R = require('ramda')
const request = require('request')
const config = require('../../config/application')
const https = require('https')
const models = require('../models')
const userService = require('../services/user')

const userTypes = {
  2: 'TUTOR',
  3: 'TUITION_CENTER',
  4: 'STUDENT',
}

const formatName =
  R.pipe(
    x => x ? x : '',
    R.split(' '),
    R.map(word => _.startCase(_.toLower(word))),
    R.join(' ')
  )

const getPushToken = userId => {
  return models.PushTokens.findOne({
    where: {
      userId,
    },
  }).then(pushToken => {
    return pushToken ? pushToken.fcmToken : null
  })
}

const sendPushNotification = ({ message, title }, recipientId) => {
  const promises = [
    userService.getUser(recipientId),
    getPushToken(recipientId)  
  ]

  models.sequelize.Promise.all(promises)
  .spread((recipient, fcmToken) => {
    if (fcmToken) {
      const userType = userTypes[`${recipient.userType}`]
      const request = https.request({
        headers: config.pushNotifications.headers[userType],
        host: config.pushNotifications.url,
        path: config.pushNotifications.path,
        protocol: config.pushNotifications.protocol,
        method: 'POST',
        body: JSON.stringify({
          to: fcmToken,
          data: {
            message,
            title,
          }
        })
      }, function(res) {
        console.log('Push status: ' + res.statusCode);
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
          console.log('Push API Response: ' + chunk);
        });
      })

      request.write(JSON.stringify({
        to: fcmToken,
        data: {
          message,
          title,
        }
      }))
      request.end()
    } else {
      console.log(`FCM token is unavailable for recipient ${recipientId}`)
    }
  }).catch(err => {
    console.log(err)
  })
}

exports.reviewAdded = ({ actor }, recipientId) => {
  const message = `${formatName(actor)} has added a review for you`
  const title = 'Review added'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.profileLiked = ({ actor }, recipientId) => {
  const message = `${formatName(actor)} has liked your profile`
  const title = 'Profile liked'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.birthdayWish = ({ actor, date }, recipientId) => {
  const message = `Wish ${formatName(actor)} on his birthday on ${date}`
  const title = 'Wish Birthday'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.studentRegistered = ({ actor }, recipientId) => {
  const message = `${formatName(actor)} has been added as your student`
  const title = 'Student Registered'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.feedCommented = ({ actor, postTitle }, recipientId) => {
  const message = `${formatName(actor)} has commented on your post "${_.startCase(_.toLower(postTitle))}"`
  const title = 'Feed Review Added'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.feedLiked = ({ actor, postTitle }, recipientId) => {
  const message = `${formatName(actor)} has liked your post "${_.startCase(_.toLower(postTitle))}"`
  const title = 'Feed Liked'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}

exports.tutorRegisteredStudent = ({ actor }, recipientId) => {
  const message = `${formatName(actor)} has added you as a student`
  const title = 'New tutor association'
  console.log(`Sending Push Notification: ${message}`)
  sendPushNotification({
    message, title,
  }, recipientId)
}
