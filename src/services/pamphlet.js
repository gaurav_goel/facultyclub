const R = require('ramda')
const models = require('../models')

exports.getUserPamphlets = userId =>
  models.Pamphlets.findAll({
    where: {
      userId,
    },
  })

exports.getUserPamphlet = (userId, pamphletId) =>
  models.Pamphlets.findOne({
    where: {
      userId,
      id: pamphletId,
    },
  })

exports.getTemplatePamphlets = () =>
  models.PamphletTemplates.findAll({})

exports.deletePamphlet = (userId, pamphletId) =>
  models.Pamphlets.destroy({
    where: {
      userId,
      id: pamphletId,
    },
  })

exports.getPamphletById = id =>
  models.Pamphlets.findById(id)