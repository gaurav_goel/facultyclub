/**
 * GET /
 * Home page.
 */
const models = require('../models')

exports.index = (req, res) => {
  res.send('API is ready')
};
