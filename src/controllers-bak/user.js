const async = require('async')
const codeGenerator = require('voucher-code-generator')
const jwt = require('jwt-simple')
const logger = require('../../config/logger').logger
const mailer = require('../helper/mailer')
const md5 = require('md5')
const passport = require('passport')
const Q = require('q')

const { auth } = require('../../config/application')
const { User } = require('../models')

const alphabetic = 'abcdefghijklmnopqrstuvwxyz'
const SUPPORTED_SIGNUP_SOURCES = ['local', 'google', 'facebook']

const generateJWTToken = (payload) => jwt.encode({
  user: payload,
}, auth.secret)

const generateToken = (length = 8, charset = 'alphanumeric') => codeGenerator.generate({
  length,
  count: 1,
  charset,
})[0]

const getUniqueReferral = () => {
  const deferred = Q.defer()
  const myReferral = generateToken()
  isValid('myReferral', myReferral, '', false).then(() => {
    deferred.resolve(myReferral)
  }, () => {
    deferred.reject(getUniqueReferral())
  })
  return deferred.promise
}

const isAuthorizedSignUp = (loggedInUser, email) => {
  const deferred = Q.defer()
  if (!loggedInUser || email === loggedInUser.email) {
    deferred.resolve()
  } else {
    deferred.reject({
      msg: 'Unauthorized signup/profile update request'
    })
  }
  return deferred.promise
}

const signUpNewUser = (req, myReferral) => {
  const deferred = Q.defer()

  // validate mobile number
  isValid('mobile', req.body.mobile,
      'User with given field already exists.', false)
  .then(() => {
    // generate a unique referral code
    const user = new User({
      mobile: req.body.mobile,
      email: req.body.email,
      referralCode: req.body.referralCode,
      profile: {
        fullname: req.body.fullname,
        picture: req.body.picture,
      },
      myReferral,
      verification: {
        code: generateToken(4, alphabetic),
      },
    })

    if (req.body.source === 'local') {
      user.password = md5(req.body.password)
    } else {
      user[req.body.source] = req.body.bearer
    }

    User.find(
      { $or:[ {'mobile': req.body.mobile}, {'email': req.body.email} ]}
      , (err, existingUsers) => {
      if (existingUsers && existingUsers.length) {
        let mobileExists = false
        let emailExists = false

        for (let i = 0; i < existingUsers.length; i++) {
          if (existingUsers[i].email === req.body.email) {
            emailExists = true
          }
          if (existingUsers[i].mobile == req.body.mobile) {
            mobileExists = true
          }
        }

        const msg = (emailExists ? 'Account with this email already exists. ' : '')
          + (mobileExists ? 'Account with this mobile number already exists.' : '')

        deferred.reject({
          msg: msg
        })
      }
      user.save((err) => {
        if (err) {
          deferred.reject({
            msg: err
          })
        } else {
          deferred.resolve(user)
        }
      })
    })
  }).catch(function (err) {
    deferred.reject(err)
  })

  return deferred.promise
}

const signUpExistingUser = (req, myReferral) => {
  const deferred = Q.defer()

  User.findOne({ _id: req.currentUser._id }, function(err, user) {
    if (err) {
      deferred.reject({
        msg: 'Failed to update existing user'
      })
    } else {
      let sendMail = false
      user.verification.code = generateToken(4, alphabetic)
      user.myReferral = myReferral
      user.profile = {
        fullname: req.body.fullname,
        picture: req.body.picture,
      }
      user.referralCode = req.body.referralCode

      if (!user.verification.verified) {
        user.mobile = req.body.mobile
        sendMail = true
      }

      user.save((err) => {
        if (err) {
          deferred.reject({
            msg: 'Failed to update existing user'
          })
        } else {
          if (sendMail) {
            mailer.sendMail(user.email, 'Welcome to pikkup',
              'Welcome to pikkup. Mobile verification code -' + user.verification.code)
          }
          deferred.resolve(user)
        }   
      })
    }
  }).catch(function (err) {
    deferred.reject(err)
  })

  return deferred.promise
}

const signUp = (req, strategy) => {
  const deferred = Q.defer()

  // validate referral code 
  isValid('referralCode', req.body.referralCode)
  // check authorization
  .then(() => { return isAuthorizedSignUp(req.currentUser, req.body.email) })
  // validate the sign up source
  .then(() => { return isValidSource(req.body.source) })
  // validate the password and confirm password
  .then(() => { 
    return isValidPassword(req.body.confirmPassword, req.body.password,
      req.body.source, req.currentUser)
  })
  .then(getUniqueReferral)
  /* 
    use the new referral code to create/update user.
    if the user doesn't exist yet, create the user
    TODO: however, if the user does exist, update the user with new sign up details
  */
  .then(function (myReferral) {
    strategy(req, myReferral).then((user) => {
      deferred.resolve(user)  
    }, (err) => {
      deferred.reject(err)  
    })
  }).catch(function (err) {
    deferred.reject(err)
  })

  return deferred.promise
}

const isValidPassword = (confirmPassword, password, source, user) => {
  const deferred = Q.defer()
  if (source === 'local' && !user) {
    if (!password) {
      deferred.reject({ msg: 'Password is mandatory' })
    }
    if (password !== confirmPassword) {
      deferred.reject({ msg: 'Password and confirm password do not match' })
    }
    deferred.resolve()
  } else {
    deferred.resolve()
  }
  return deferred.promise
}

const isValidSource = (source) => {
  const deferred = Q.defer()
  if (SUPPORTED_SIGNUP_SOURCES.indexOf(source) > -1) {
    deferred.resolve()
  } else {
    deferred.reject({
      msg: `Invalid signup source: ${source}`
    })
  }

  return deferred.promise
}

const isValid = (field, value, errorMsg = 'Invalid', shouldExist = true) => {
  const deferred = Q.defer()

  if (value) {
    User.findOne({[field]: value}, function(err, data) {
      if(err){
        deferred.reject(err)
      }
      if ((data && shouldExist) || (!data && !shouldExist)) {
        deferred.resolve(true)
      } else {
        deferred.reject({
          msg: `${errorMsg} ${field}: ${value}`
        })
      }
    })
  } else {
    deferred.resolve(true)
  }
  
  return deferred.promise
}

const setResponse = (res, msg = '', status = 200) => {
  if (res) {
    res.status(status).json({
      msg: msg
    })
  }
}

const findUserById = (id) => {
  const deferred = Q.defer()
  User.findById(id, function(err, user) {
    if (err || !user) {
      deferred.reject(err ? err : {
        msg: 'Invalid user ID'
      })
    } else {
      deferred.resolve(user)
    }
  })
  return deferred.promise
}

const validateSignup = (req) => {
  req.assert('email', 'Email is a mandatory field').notEmpty()
  req.assert('email', 'Invalid email entered').isEmail()
  req.assert('fullname', 'Full name is a mandatory field').notEmpty()
  req.assert('mobile', 'Mobile number must be 10 digits long').isInt().len(10)
  req.assert('source', 'The signup source must be specified').notEmpty()

  return req.validationErrors()
}

const valdiatePasswordUpdate = (req) => {
  req.assert('password', 'Password must be at least 4 characters long.').len(4)
  req.assert('confirmPassword', 'Passwords must match.').equals(req.body.password)

  return req.validationErrors()
}

/**
 * GET /login
 * Login page.
 */
exports.getLogin = (req, res) => {
  if (req.user) {
    return res.redirect('/')
  }
  return res.send({
    title: 'Login'
  })
}

exports.findUserById =findUserById
exports.getUniqueReferral = getUniqueReferral

exports.findUserByEmail = (email) => {
  const deferred = Q.defer()
  User.findOne({
    email: email
  }, function(err, user) {
    if (err || !user) {
      deferred.reject(err ? err : {
        msg: 'Invalid email ID'
      })
    } else {
      deferred.resolve(user)
    }
  })
  return deferred.promise
}

/**
 * POST /login
 * Sign in using email and password.
 */
exports.postLogin = (req, res, next) => {
    // either the password will have to supplied or a pair of bearer and source
  let mode = req.body.password ? 'password' : (req.body.bearer ? 'bearer': null)
  switch(mode) {
    case 'password':
      req.mobile = req.body.mobile
      passport.authenticate('local', (err, user, info) => {
        if (err) {
          logger.warn('Failed to authenticate using local strategy.',
            err)
        }
        
        if (info) {
          return res.status(401).send({
            msg: info
          })
        } else {
          const response = {
            token: generateJWTToken(user._id)
          }
          if (!user.mobile || !user.verification.verified) {
            response.verified = false
          } else {
            response.verified = true
          }
          response.user = user

          return res.status(200).send(response)
        }
      })(req, res, next)
      break
    case 'bearer':
      req.body.access_token = req.body.bearer
      passport.authenticate(auth.strategy[req.body.source],
        function(err, profile, info) {
          if (err) return res.status(500).send({
            msg: err
          })
          if (info) return res.status(401).send({
            msg: info
          })

          const response = {
            token: generateJWTToken(profile._id)
          }

          if (!profile.mobile || !profile.verification.verified) {
            response.verified = false
          } else {
            response.verified = true
          }
          response.user = profile

          return res.status(200).send(response)
        }
      )(req, res, next)
      break
    default:
      return res.status(400).json({
        msg: 'A password or bearer field is expected for login.'
      })
  }
}

/**
 * GET /logout
 * Log out.
 */
exports.logout = (req, res) => {
  req.logout()
  setResponse(res, 'You have been successfully logged out', 200)
  return res
}

exports.verifyMobileConfirmationCode = (req, res, next) => {
  if (!req.currentUser.verification.code) {
    return res.status(400).json({
      msg: 'Confirmation is not required for this account'
    })
  }
  const ERR_MSG = 'Invalid confirmation code'
  req.assert('code', ERR_MSG).notEmpty().len(4)

  const errors = req.validationErrors()

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  if (req.currentUser.verification.code === req.body.code){
    User.update({
      _id: req.currentUser._id
    },{
      $set: {
        verification: {
          verified: true,
          code: '',
        },
      }
    }, {
      multi: false
    }, (err, user) => {
      if (err) {
        return res.status(400).json({
          msg: ERR_MSG
        })    
      }

      return res.status(200).json({
        msg: 'Code successfully confirmed.'
      })
    })
  } else {
    return res.status(400).json({
      msg: ERR_MSG
    })
  }
}

/**
 * POST /signup
 * Create a new local account.
 */
exports.postSignup = (req, res, next) => {
  const errors = validateSignup(req)

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  signUp(req, signUpNewUser).then((user) => {
    mailer.sendMail(user.email, 'Welcome to pikkup',
      'Welcome to pikkup. Mobile verification code -' + user.verification.code)
    return res.status(200).json({
      token: generateJWTToken(user._id),
      profile: user,
    })
  }, (err) => {
    return res.status(400).json(err)
  })
}

exports.putUserDetails = (req, res, next) => {
  const errors = validateSignup(req)

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  signUp(req, signUpExistingUser).then((user) => {
    return res.status(200).json({
      token: generateJWTToken(user._id),
      profile: user,
    })
  }, (err) => {
    return res.status(400).json(err)
  })
}

exports.fetch = (req, res, next) => {
  const embed = req.query.embed ? req.query.embed.split(',').join(' ') : '' 
  const query = User.findOne({_id: req.currentUser._id})
  if(embed) {
    query.populate(embed)
  }
  query.exec(function(err, user) {
    if(err) {
      return res.status(500).json({
        msg:'Failed to fetch user'
      })
    }
    else{
      return res.status(200).send(user)
    }

  })
}

exports.postUpdatePassword = (req, res, next) => {
  req.assert('password', 'Password must be at least 4 characters long').len(4)
  req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password)

  const errors = req.validationErrors()

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  User.findById(req.currentUser._id, (err, user) => {
    if (err) {
      console.log(err)
      return res.status(500).json({
        msg: 'Failed to update password'
      })
    } else {
      user.password = md5(req.body.password)
      user.save((err) => {
        if (err) {
          return res.status(500).json({
            msg: 'Failed to update password'
          })
        }
        return res.status(200).json({
          msg: 'Successfully changed the password'
        })
      })
    }
  })
}

exports.deleteAccount = (req, res, next) => {
  User.remove({ _id: req.currentUser._id }, (err) => {
    if (err) {
      return res.status(500).json({
        msg: 'Failed to delete the account.'
      })
    } else {
      return res.status(200).json({
        msg: 'The account has been successfully deleted.'
      })
    }
  })
}

exports.getOauthUnlink = (req, res, next) => {
  const ERROR_MSG = 'Failed to unlink the account'
  const provider = req.params.source
  User.findById(req.currentUser._id, (err, user) => {
    if (err) {
      return res.status(500).json({
        msg: ERROR_MSG
      })
    }

    user[provider] = undefined
    user.save((err) => {
      if (err) {
        return res.status(500).json({
          msg: ERROR_MSG
        })
      }
      return res.status(200).json({
        msg: `${provider} account has been unlinked.`
      })
    })
  })
}

exports.postReset = (req, res, next) => {
  const errors = valdiatePasswordUpdate(req)
  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  const ERROR_MSG = 'Failed to update password'
  async.waterfall([
    function (done) {
      User
      .findOne({ passwordResetToken: req.params.token })
      .where('passwordResetExpires').gt(Date.now())
      .exec((err, user) => {
        if (err) {
          console.log(err)
          return res.status(500).json({
            msg: ERROR_MSG
          })
        }

        if (!user) {
          return res.status(400)
            .json({ msg: 'Password reset token is invalid or has expired.' })
        }

        user.password = md5(req.body.password)
        user.passwordResetToken = undefined
        user.passwordResetExpires = undefined
        user.save((err) => {
          if (err) {
            done(ERROR_MSG)
          } else {
            done(null, user)
          }
        })
      })
    },
    function (user, done) {
      mailer.sendMail(user.email, 'Your password has been changed',
        `This is a confirmation that the password for your account ${user.email} has just been updated.`)
      return res.status(200).json({
        msg: 'Password has been updated',
        token: generateJWTToken(user._id),
        user,
        verified: user.verification.verified,
      })
    }
  ], (err) => {
    if (err) {
      console.log(err)
      return res.status(500).json({
        msg: 'Unexpected error occurred while updating password token'
      })
    }
  })
}

exports.postForgot = (req, res, next) => {
  req.assert('mobile', 'Please enter a valid mobile.').notEmpty()
  const errors = req.validationErrors()

  if (errors) {
    setResponse(res, errors, 400)
    return res
  }

  async.waterfall([
    function (done) {
      done(null, generateToken(6))
    },
    function (token, done) {
      User.findOne({ mobile: req.body.mobile }, (err, user) => {
        if (!user) {
          return res.status(400).json({
            msg: 'Account with that contact number does not exist'
          })
        }
        user.passwordResetToken = token
        user.passwordResetExpires = Date.now() + 3600000; // 1 hour
        user.save((err) => {
          done(err, token, user)
        })
      })
    },
    function (token, user, done) {
      mailer.sendMail(user.email, 'Reset Password Request',
        `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n
          Following is the rest token:\n
          ${token}\n\n
          If you did not request this, please ignore this email and your password will remain unchanged.\n`)
      return res.status(200).json({
        msg: 'The password reset token has been mailed'
      })
    }
  ], (err) => {
    if (err) {
      console.log(err)
      return res.status(500).json({
        msg: 'Unexpected error occurred while generating reset password token'
      })
    }
  })
}

exports.tokenAnalyzer = (req, res, next) => {
  var token = req.body.token || req.query.token || req.headers['x-access-token']

  if (token) {
    let decoded
    try {
      decoded = jwt.decode(token, auth.secret)
    } catch (err) {
      return res.status(403).send({
        msg: 'Invalid auth token.' 
      })
    }
    
    findUserById(decoded.user).then((user) => {
      req.currentUser = user
      next()
    }, (err) => {
      return res.status(403).send({
        msg: 'Invalid auth token.' 
      })
    })
  } else {
    return res.status(403).send({
      msg: 'No auth token provided.' 
    })
  }
}