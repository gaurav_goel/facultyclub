const BasicController = require('./basic')
const UploadController = require('./upload')
const UserController = require('./user')

exports.basicController = BasicController
exports.uploadController = UploadController
exports.userController = UserController