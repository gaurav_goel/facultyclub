const fs = require('fs')
// FIXME
const FileMeta = require('../FileMeta')
const { fileIngest } = require('../../config/application')

const ACCEPTED_MIME_TYPES = fileIngest.acceptedMimeTypes
const DOWNLOAD_PATH = fileIngest.downloadPath

/**
* Buffer size used to download an asset.
*
* @attribute DOWNLOAD_BUFFER_SIZE
* @type {Number}
**/
const DOWNLOAD_BUFFER_SIZE = 2 * 1024

/**
 * Uses the file path to convert to a path where the file should be uploaded.
 *
 * @param {String} filePath
 **/
const getUploadFilePath = (filePath = '') => {
  const date = new Date()
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hours = date.getHours()
  return `${filePath}/${year}/${month}/${day}/${hours}`
}

exports.getUploadFilePath = getUploadFilePath

exports.upload = (upload, req, res) => {
  upload(req, res, (err) => {
    if (err || !req.file) {
      let errMsg
      if (err && err.code === 'LIMIT_FILE_SIZE') {
        errMsg = 'Maximum file size allowed is 50MB.'
      }
      else {
        errMsg = err || 'Invalid or no file supplied'
      }
      res.status(500).json({
        msg: errMsg,
      })
    }
    else {
      const { originalname, encoding, mimetype, filename, size } = req.file

      const fileMeta = new FileMeta({
        originalName: originalname,
        encoding,
        mimetype,
        filename,
        size,
        path: `${getUploadFilePath()}/${filename}`,
        uri: DOWNLOAD_PATH + filename,
      })

      fileMeta.save((error, fileMetaObj) => {
        if (!error) {
          fileMetaObj.uri = `/egest?file=${fileMeta.filename}` // eslint-disable-line no-param-reassign
          res.status(200).json(fileMetaObj)
        }
        return res.status(500).json({
          msg: error,
        })
      })
    }
  })
}

/**
 * Downloads the asset files
 */
exports.egest = function (req, res, fileIdentifier, basePath) {
  FileMeta.findOne({ filename: fileIdentifier }, (err, fileObj) => {
    if (!err) {
      if (!fileObj) {
        return res.status(400)
      }

      const filePath = basePath + fileObj.path

      // file exists
      fs.exists(filePath, (exists) => {
        if (exists) {
          if (fileObj.mimetype.indexOf('images') > -1) {
            res.setHeader('Content-disposition', `attachment; filename=${fileObj.originalname}`)
          }
          res.setHeader('Content-Length', fileObj.size)
          res.setHeader('Content-type', fileObj.mimetype)

          const stream = fs.createReadStream(filePath, { bufferSize: DOWNLOAD_BUFFER_SIZE })
          stream.pipe(res)
        }
        return res.status(400).send('File not found')
      })
    }

    return res.status(500).json({
      msg: 'Unexpected error occurred while fetching file info.',
    })
  })
}

exports.fileFilter = (req, file, cb) => {
  if (ACCEPTED_MIME_TYPES.indexOf(file.mimetype) > -1) {
    cb(null, true)
  }
  else {
    cb(`Invalid file type - ${file.mimetype}`, false)
  }
}
