const reviewsController = require('../controllers/review')

module.exports = (app) => {
  app.post('/api/reviews', reviewsController.postReviews)
  app.post('/api/reviews/invites', reviewsController.postInvites)
}
