const tutorController = require('../controllers/tutor')
const feedController = require('../controllers/feed')

module.exports = (app) => {
  // students related APIs
  app.get('/api/tutors/:id/students', tutorController.getStudents)
  app.get('/api/tutors/:id/students/:studentId', tutorController.getStudentProfile)
  app.delete('/api/tutors/:id/students/:studentId', tutorController.removeStudent)
  app.put('/api/tutors/:id/students/:studentId', tutorController.updateStudentProfile)
  // attendance
  app.get('/api/tutors/:id/sessions',tutorController.getSessionsForDate)
  // invoices
  app.post('/api/tutors/:tutorId/students/:studentId/invoices',tutorController.createInvoice)
  app.post('/api/tutors/:tutorId/students/:studentId/payments',tutorController.createPayments)
  app.get('/api/tutors/:tutorId/students/:studentId/invoice-data', tutorController.getInvoiceSuggestions)
  app.get('/api/tutors/:tutorId/groups/:groupId/invoice-data', tutorController.getInvoiceSuggestionsForGroup)
  app.put('/api/invoices/:id', tutorController.updateInvoice)
  app.delete('/api/invoices/:id', tutorController.deleteInvoice)
  // feed APIs
  app.post('/api/tutors/:id/feed', feedController.create)
  app.post('/api/tutors/:id/feed/views', feedController.postFeedViews)
  app.patch('/api/tutors/:id/feed/:feedId/moderate', feedController.moderate)
  app.put('/api/tutors/:id/feed/:feedId', feedController.update)
  app.get('/api/tutors/:id/feed', tutorController.fetchFeed)
  app.get('/api/tutors/:id/my-feed', tutorController.fetchUserFeed)
  app.get('/api/tutors/:id/feed/:feedId', tutorController.fetchFeedById)
  app.get('/api/tutors/:id/feed/:feedId/likes', tutorController.fetchFeedLikes)
  app.get('/api/tutors/:id/feed/:feedId/comments', tutorController.fetchFeedComments)
  app.delete('/api/tutors/:id/feed/:feedId', (req, res) => {
    feedController.delete(req, res)
  })
}
