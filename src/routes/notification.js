const notificationController = require('../controllers/notification')

module.exports = (app) => {
  app.get('/api/tutors/:tutorId/notifications', notificationController.fetchNotifications)
  app.put('/api/tutors/:tutorId/notifications/:id/seen', notificationController.markSeen)
  app.put('/api/tutors/:tutorId/notifications/seen', notificationController.markAllSeen)
  app.delete('/api/tutors/:tutorId/notifications/:id', notificationController.deleteNotification)
  app.delete('/api/tutors/:tutorId/notifications', notificationController.deleteAllNotification)
}
