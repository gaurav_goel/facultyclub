const crypto = require('crypto')
const mkdirp = require('mkdirp')
const multer = require('multer')
const path = require('path')

const tutorContentController = require('../controllers/upload')
const assignmentController = require('../controllers/assignment')

const config = require('../../config/application')

const DEFAULT_FOLDER_PERMISSIONS = 493
const FILE_STORE = path.join(__dirname, '../../uploads')
const ASSIGNMENT_FILE_STORE = path.join(__dirname, '../../uploads/assignments')

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const dest = `${tutorContentController.getUploadFilePath(FILE_STORE)}/`
    mkdirp.sync(dest, DEFAULT_FOLDER_PERMISSIONS)
    cb(null, dest)
  },
  filename: (req, file, cb) => {
    let extension = ''
    if (file.originalname.indexOf('.') > -1) {
      extension = file.originalname.substring(file.originalname.indexOf('.'))
    }
    crypto.pseudoRandomBytes(16, (err, raw) => {
      cb(err, err ? undefined : raw.toString('hex') + extension)
    })
  },
})
const assignmentStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    const dest = `${tutorContentController.getUploadFilePath(ASSIGNMENT_FILE_STORE)}/`
    mkdirp.sync(dest, DEFAULT_FOLDER_PERMISSIONS)
    cb(null, dest)
  },
  filename: (req, file, cb) => {
    crypto.pseudoRandomBytes(16, (err, raw) => {
      cb(err, err ? undefined : raw.toString('hex'))
    })
  },
})

const upload = multer({
  storage,
  fileFilter: tutorContentController.fileFilter,
  limits: {
    fileSize: config.fileIngest.maxFileSize * 1024 * 1024,
  },
})

const assignmentUpload = multer({
  assignmentStorage,
  fileFilter: (req, file, cb) => {
    if (config.fileIngest.acceptedAssignmentMimeTypes.indexOf(file.mimetype) > -1) {
      cb(null, true)
    }
    else {
      cb(`Invalid file type - ${file.mimetype}`, false)
    }
  },
  limits: {
    fileSize: config.fileIngest.maxFileSize * 1024 * 1024,
  },
})

module.exports = (app) => {
  app.post('/file-ingest', (req, res) => {
    tutorContentController.upload(upload.single('file'), req, res)
  })
  app.get('/egest', (req, res) => {
    tutorContentController.egest(req, res, req.query.file, FILE_STORE)
  })

  // tutor content
  app.post('/api/tutors/:id/contents', (req, res) => {
    tutorContentController.tutorContentUpload(upload.single('file'), req, res)
  })
  app.put('/api/tutors/:id/contents/:contentId', tutorContentController.updateContent)
  app.delete('/api/tutors/:id/contents/:contentId', (req, res) => {
    tutorContentController.deleteContent(req, res)
  })

  // assignment
  app.post('/api/assignments/upload', (req, res) => {
    assignmentController.upload(assignmentUpload.single('file'), req, res)
  })
}
