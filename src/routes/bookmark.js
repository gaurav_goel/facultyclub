const bookmarkController = require('../controllers/bookmark')

module.exports = (app) => {
  app.post('/api/tutors/:id/bookmark-folders', bookmarkController.createFolder)
  app.get('/api/tutors/:id/bookmark-folders', bookmarkController.getFolders)
  app.get('/api/tutors/:id/bookmark-folders/:folderId', bookmarkController.getFolder)
  app.put('/api/tutors/:id/bookmark-folders/:folderId', bookmarkController.updateFolder)
  app.delete('/api/tutors/:id/bookmark-folders/:folderId', bookmarkController.deleteFolder)
  app.get('/api/tutors/:id/bookmarks', bookmarkController.getAllBookmarked)
  app.post('/api/tutors/:id/bookmarks', bookmarkController.createBookmark)
  app.delete('/api/tutors/:id/bookmarks/:bookmarkId', bookmarkController.deleteBookmark)
  app.put('/api/tutors/:id/bookmarks/:bookmarkId', bookmarkController.updateBookmark)
}
