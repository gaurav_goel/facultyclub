const userController = require('../controllers/user')

module.exports = (app) => {
  app.get('/api/users/:id/likes', userController.likes)
  app.get('/api/users/:id/reviews', userController.reviews)
  app.put('/api/users/:id', userController.update)
  app.get('/api/users/:userId', userController.getUser)
  app.get('/api/tution-centre/:userId', userController.getUser)
  app.put('/api/tution-centre/:id/faculty', userController.addFaculty)
  app.put('/api/users/:id/profile-completeness', userController.updateProfileCompleteness)
}
