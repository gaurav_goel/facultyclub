const assignmentController = require('../controllers/assignment')

module.exports = (app) => {
  app.post('/api/assignments/generate-docs-tags', assignmentController.generateDocumentTags)
  app.get('/api/assignments/migrate', assignmentController.assignmentMigration)
  app.get('/api/assignments/subjects', assignmentController.subjects)
  app.get('/api/assignments/chapters', assignmentController.chapters)
  app.get('/api/assignments/:id/export', assignmentController.exportAssignment)
  app.get('/api/assignments/:id', assignmentController.getAssignment)
  app.get('/api/assignments', assignmentController.filterAssignments)
  app.get('/api/tutors/:id/assignments', assignmentController.filterTutorAssignments)
  app.get('/api/assignments/:id/generate-docs', assignmentController.generateDocuments)
  app.get('/api/assignments/:id/generate-all-docs', assignmentController.generateAllDocuments)
  app.get('/api/assignments/:id/documents', assignmentController.egest)
  app.get('/api/assignments/:id/document-links', assignmentController.documentLink)
}
