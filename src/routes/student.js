const studentController = require('../controllers/student')

module.exports = (app) => {
  app.post('/api/students', studentController.createInvite)
  app.get('/api/students/:id/tutors', studentController.getEngagements)
  app.get('/api/students/:id/groups', studentController.getStudentGroups)
  app.put('/api/attendances/:id', studentController.updateAttendance)
  app.post('/api/attendances', studentController.markAttendance)
  app.get('/api/students/:id/tutors/:tutorId/attendances', studentController.getAttendance)
  app.delete('/api/attendances', studentController.deleteAttendance)
}
