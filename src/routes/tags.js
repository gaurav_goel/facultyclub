const tagsController = require('../controllers/tags')

module.exports = (app) => {
  app.get('/api/tags', tagsController.getAllTags)
  app.post('/api/tags', tagsController.createTag)
  app.delete('/api/tags/:id', tagsController.deleteTag)
}
