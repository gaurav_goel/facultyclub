const likesController = require('../controllers/like')

module.exports = (app) => {
  app.post('/api/likes', likesController.postLikes)
  app.delete('/api/likes', likesController.deleteLikes)
}
