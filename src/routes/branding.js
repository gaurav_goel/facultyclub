const brandingController = require('../controllers/branding')

module.exports = (app) => {
  // students related APIs
  app.post('/api/tutors/:id/brandings', brandingController.createOrUpdate)
  app.get('/api/tutors/:id/brandings', brandingController.getUserBranding)
  app.delete('/api/tutors/:id/brandings', brandingController.deleteBranding)
}
