const transactionController = require('../controllers/transaction')

module.exports = (app) => {
  app.post('/api/tutors/:tutorId/students/:studentId/transactions', transactionController.createTransaction)
  app.put('/api/tutors/:tutorId/students/:studentId/transactions/:id', transactionController.updateTransaction)
  app.delete('/api/transactions/:id', transactionController.deleteTransaction)
  app.get('/api/transactions/:id', transactionController.fetchTransaction)
  app.get('/api/tutors/:tutorId/students/:studentId/transactions', transactionController.fetchStudentTransactions)
  app.post('/api/tutors/:tutorId/students/:studentId/transactions/reminders', transactionController.sendReminder)
}
