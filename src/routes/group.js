const groupController = require('../controllers/group')
const studentController = require('../controllers/student')

module.exports = (app) => {
  app.get('/api/groups/:id', groupController.fetch)
  app.delete('/api/tutors/:tutorId/groups/:id', groupController.remove)
  app.get('/api/tutors/:id/groups', groupController.getGroupsByTutorId)
  app.post('/api/groups', groupController.create)
  app.put('/api/groups/:id', groupController.updateGroup)
  app.post('/api/groups/:id/students', groupController.addStudentToGroup)
  app.delete('/api/groups/:id/students/:studentId', groupController.removeStudentToGroup)
  app.get('/api/groups/:id/attendances',studentController.getGroupAttendance)
}
