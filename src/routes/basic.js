const basicController = require('../controllers/basic')
const userController = require('../controllers/user')

module.exports = (app) => {
  app.get('/', basicController.index)

  /**
   * Sign up, login and logout
   */
  app.post('/signup', userController.postSignup)
  app.post('/login', userController.postLogin)
  app.post('/pre-login', userController.preLogin)
  app.post('/resend-otp', userController.resendOTP)
  app.post('/verify-phone', userController.verifyToken)
  app.post('/forgot-password', userController.postForgot)
  app.post('/reset-password', function(req, res, next) {
    if (req.body.currentPassword) {
      return userController.postUpdatePassword(req, res, next)
    } else {
      return userController.postResetPassword(req, res, next)
    }
  })
  app.get('/logout', userController.logout)
  app.get('/api/classes', basicController.getClasses)
  app.get('/api/boards', basicController.getBoards)
  app.get('/api/languages', basicController.getLanguages)
  app.get('/api/hashtags', basicController.getTags)

  app.post('/api/contact', basicController.contactAdmin)
  app.post('/api/push-token', basicController.postFCMToken)
  app.post('/api/share', basicController.share)
  app.get('/api/shared-content/:context', basicController.serveShared)
}
