const commentController = require('../controllers/comment')

module.exports = (app) => {
  app.post('/api/tutors/:tutorId/comments', commentController.postComment)
  app.put('/api/tutors/:tutorId/comments/:id', commentController.editComment)
  app.delete('/api/tutors/:tutorId/comments/:id', commentController.deleteComment)
  app.get('/api/tutors/:tutorId/comments/:id', commentController.fetchComment)
}
