const BasicRoutes = require('./basic')
const UploadRoutes = require('./upload')
const UserRoutes = require('./user')
const LikeRoutes = require('./like')
const CommentRoutes = require('./comment')
const StudentRoutes = require('./student')
const TutorRoutes = require('./tutor')
const GroupRoutes = require('./group')
const ReviewRoutes = require('./review')
const AssignmentRoutes = require('./assignment')
const BookmarkRoutes = require('./bookmark')
const NotificationRoutes = require('./notification')
const TransactionRoutes = require('./transaction')
const PamphletRoutes = require('./pamphlet')
const BrandingRoutes = require('./branding')
const TagsRoutes = require('./tags')

module.exports = {
  basicRoutes: BasicRoutes,
  uploadRoutes: UploadRoutes,
  userRoutes: UserRoutes,
  likeRoutes: LikeRoutes,
  commentRoutes: CommentRoutes,
  studentRoutes: StudentRoutes,
  tutorRoutes: TutorRoutes,
  groupRoutes: GroupRoutes,
  reviewRoutes: ReviewRoutes,
  assignmentRoutes: AssignmentRoutes,
  bookmarkRoutes: BookmarkRoutes,
  notificationRoutes: NotificationRoutes,
  transactionRoutes: TransactionRoutes,
  pamphletRoutes: PamphletRoutes,
  brandingRoutes: BrandingRoutes,
  tagsRoutes: TagsRoutes,
}
