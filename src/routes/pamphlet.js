const pamphletController = require('../controllers/pamphlet')

module.exports = (app) => {
  app.get('/api/pamphlets/templates', pamphletController.getPamphletTemplates),
  app.get('/api/tutors/:id/pamphlets', pamphletController.getUserPamphlets)
  app.get('/api/tutors/:userId/pamphlets/:id', pamphletController.getUserPamphlet)
  app.post('/api/tutors/:id/pamphlets', pamphletController.createPamphlet)
  app.put('/api/tutors/:id/pamphlets/:pamphletId', pamphletController.updatePamphlet)
  app.delete('/api/tutors/:userId/pamphlets/:id', pamphletController.deletePamphlets)
  app.post('/api/tutors/:tutorId/pamphlets/:id/generate', pamphletController.developPamphlet)
  
}
