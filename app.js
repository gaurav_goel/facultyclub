/**
 * Module dependencies.
 */
require('./memory')
const bodyParser = require('body-parser')
const compression = require('compression')
const dotenv = require('dotenv')
const errorHandler = require('errorhandler')
const express = require('express')
const expressValidator = require('express-validator')
const epilogue = require('epilogue')
const fs = require('fs')
const flash = require('express-flash')
const lusca = require('lusca')
const Sequelize = require('sequelize')
const notifier = require('node-notifier')
const passport = require('passport')
const path = require('path')
const session = require('express-session')
const models = require('./src/models')
const logger = require('morgan-body')
const http = require('http')
const socketioService = require('./src/services/io')
const userController = require('./src/controllers/user')
/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env.dev' })

/**
 * route handlers
 */
const { basicRoutes, likeRoutes, reviewRoutes, uploadRoutes,
  userRoutes, studentRoutes, tutorRoutes, groupRoutes,
  assignmentRoutes, bookmarkRoutes, commentRoutes, notificationRoutes,
  transactionRoutes, pamphletRoutes, brandingRoutes, tagsRoutes }
  = require('./src/routes/index')
const { configureResources } = require('./src/controllers/index')


/**
 * API keys and Passport configuration.
 */
require('./src/config/passport')

/**
 * Create Express server.
 */
const app = express()
const server = http.createServer(app)
const io = require('socket.io')(server, {path: '/socket.io'})

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000)
// cors settings
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', "OPTIONS, POST, GET, PUT, DELETE, PATCH");
  res.header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  next();
})

app.use(compression())
app.use(bodyParser.json())
// log all the requests and responses
logger(app)
app.use(bodyParser.urlencoded({ extended: true }))
app.use(expressValidator())
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  // store: new MongoStore({
  //   url: process.env.MONGODB_URI || process.env.MONGOLAB_URI,
  //   autoReconnect: true,
  // }),
}))
app.use(passport.initialize())
app.use(passport.session())

app.use(flash())
app.use((req, res, next) => {
  if (req.path === '/api/upload') {
    next()
  }
  else {
    next()
    // lusca.csrf()(req, res, next);
  }
})
app.use(lusca.xframe('ALLOW-FROM *'))
app.use(lusca.xssProtection(false))
app.use((req, res, next) => {
  res.locals.user = req.user // eslint-disable-line no-param-reassign
  next()
})
app.use(expressValidator({
  customValidators: {
    isArray: (value) => !value || Array.isArray(value),
    gte: (param, num) => param >= num,
  },
}))
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }))
app.use('/images', express.static(path.join(__dirname, 'images')))

// setup routes
basicRoutes(app)

/**
 * All the APIs listed from now onwards are authenticated
 * TODO: find a better way. Do not use the user controller directly in app.js
 */
uploadRoutes(app)
app.use(userController.tokenAnalyzer)

likeRoutes(app)
commentRoutes(app)
notificationRoutes(app)
reviewRoutes(app)
userRoutes(app)
studentRoutes(app)
tutorRoutes(app)
groupRoutes(app)
assignmentRoutes(app)
bookmarkRoutes(app)
transactionRoutes(app)
pamphletRoutes(app)
brandingRoutes(app)
tagsRoutes(app)

/**
 * Error Handler.
 */
if (process.env.NODE_ENV === 'development') {
  const errorNotification = (err, str, req) => {
    notifier.notify({
      title: `Error in ${req.method} ${req.url}`,
      message: str,
      sound: true,
    })
  }

  app.use(errorHandler({ log: errorNotification }))
}

/**
 * Connect via sequelize.
 */
epilogue.initialize({
  app: app,
  sequelize: models.sequelize,
  base: '/api',
})

// configure all the REST resources
configureResources(epilogue, models)

models.sequelize.sync().then(() => {
  /**
   * Start Express server.
   */
  server.listen(app.get('port'))
  // app.listen(app.get('port'), () => {
  //   console.log('Express server listening on port %d in %s mode',
  //     app.get('port'), app.get('env'))
  // })
})

socketioService.setup(io)

module.exports = app
