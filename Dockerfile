FROM nodesource/node:6.2

ADD package.json package.json
RUN npm install --loglevel error
ADD . .

EXPOSE 3000

CMD ["npm", "start"]
