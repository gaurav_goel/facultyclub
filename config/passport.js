const _ = require('lodash');
const md5 = require('md5');
const passport = require('passport');
const request = require('request');
const Q = require('q');
const LocalStrategy = require('passport-local').Strategy;

const User = require('../src/models/User');
const UserController = require('../src/controllers/user')

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

/**
 * Sign in using Email and Password.
 */
passport.use(new LocalStrategy({ usernameField: 'mobile' }, (mobile, password, done) => {
  User.findOne({ mobile: mobile }, (err, user) => {
    if (!user) {
      return done(null, false, { msg: `Invalid mobile` });
    }
    user.comparePassword(md5(password), (err, isMatch) => {
      if (isMatch) {
        return done(null, user);
      }
      return done(null, false, { msg: 'Invalid mobile or password' });
    });
  });
}));

/**
 * OAuth Strategy Overview
 *
 * - User is already logged in.
 *   - Check if there is an existing account with a provider id.
 *     - If there is, return an error message. (Account merging not supported)
 *     - Else link new OAuth account with currently logged-in user.
 * - User is not logged in.
 *   - Check if it's a returning user.
 *     - If returning user, sign in and we are done.
 *     - Else check if there is an existing account with user's email.
 *       - If there is, return an error message.
 *       - Else create a new account.
 */
const updateUser = (options) => {
  const deferred = Q.defer()
  let info
  if (!options.profile) {
    info = 'Unauthorized'
  }

  let error
  UserController.findUserByEmail(options.email)
  .then(function(user) {
    User.update({
      _id: user._id
    },{
      $set: {
        [options.source]: options.accessToken,
      }
    },{
      multi: false
    }, (err) => {
      if (err) {
        deferred.resolve(error)
      } else {
        deferred.resolve({
          error,
          user,
          info,
        })
      }
    })
  }, function(err) {
    const user = new User({
      [options.source]: options.accessToken,
      mobile: options.mobile,
      email: options.email,
      profile: {
        fullname: options.displayName,
        picture: options.photo,
      }
    })

    user.save((err) => {
      if (err) {
        deferred.resolve(error)
      } else {
        deferred.resolve({
          error,
          user,
          info,
        })
      }
    })
  })
  return deferred.promise
}

/**
 * Login Required middleware.
 */
exports.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.status(401).json({
    msg: 'Unauthorized access. Please login.'
  })
};
