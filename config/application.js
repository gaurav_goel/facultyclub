const ACCEPTED_AUDIO_TYPES = ['audio/mp3']
const ACCEPTED_IMAGE_MIME_TYPES = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif']
const ACCEPTED_DOC_MIME_TYPES = ['application/pdf', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document']
		.concat(ACCEPTED_IMAGE_MIME_TYPES)
const ACCEPTED_MIME_TYPES = ['video/mp4']
	.concat(ACCEPTED_DOC_MIME_TYPES)
	.concat(ACCEPTED_AUDIO_TYPES)

const ACCEPTED_ASSIGNMENT_MIME_TYPES = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet']

const AUTH_SECRET = 'secret'

exports.SERVER_URI = 'http://localhost:4000'

exports.fileIngest = {
	acceptedMimeTypes: ACCEPTED_MIME_TYPES,
	acceptedImageMimeTypes: ACCEPTED_IMAGE_MIME_TYPES,
	acceptedAssignmentMimeTypes: ACCEPTED_ASSIGNMENT_MIME_TYPES,
	downloadPath: '/egest?file=',
	maxFileSize: 50, // in MBs
}

exports.auth = {
	secret: AUTH_SECRET,
	strategy: {
		google: 'google-plus-token',
		facebook: 'facebook-token',
	},
}

exports.completenessConfig = {
	percentages: {
		0: 20,
		1: 10,
		2: 25,
		3: 10,
		4: 5,
		5: 25,
		6: 5,
	},
	indices: {
		personalDetail: 0,
		location: 1,
		tuition: 2,
		qualification: 3,
		social: 4,
		content: 5,
		review: 6,
		0: 'personalDetail',
		1: 'location',
		2: 'tuition',
		3: 'qualification',
		4: 'social',
		5: 'content',
		6: 'review',
	}
}

exports.mail = {
	maxRetries: 5,
	allowMailSend: true,
	contactEmail: 'admin@edtropy.com',
	user: 'faqulty.club@edtropy.com',
	pass: 'jaydeep999',
}

exports.userTypes = {
	tutor: 2,
	tution_center: 3,
	student: 4,
}

exports.pushNotifications = {
	url: 'fcm.googleapis.com',
	path: '/fcm/send',
	protocol: 'https:',
	headers: {
		TUTOR: {
			'Content-Type': 'application/json',
			Authorization: 'key=AAAAPVsoQ9M:APA91bHuq_DE2IoRqMEHDPg-qm9C944Mnz_Rjqn-9ow2brPP8AyfNYeGmmwC0z5FpVNpKC905QoTgfdoI91LSyBCQUZ3XohiZ8imKXsYYqO3h19pzXx1Y0TXYxVDzG1Qin5UrW3IuH2Z'
		},
		STUDENT: {
			'Content-Type': 'application/json',
			Authorization: 'key=AIzaSyCG0DOriTjXQ5jACS6OPTqW7s-9fTMvXb4'
		},
	}
}

// make sure to add '/' in the last
exports.assignmentImagesLocation = '/home/gaurav/practice/faculty/images/'
