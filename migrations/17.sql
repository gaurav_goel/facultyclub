drop table if exists subjects;
drop table if exists assignments;
drop table if exists assignment_views;
drop table if exists question;
drop table if exists answer;

-- add subject ID
alter table `chapters`
add column `subject_id` integer(11) NOT NULL;

alter table `chapters`
drop column `class_id`;

-- Table for storing subjects
CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `class_id` integer(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `pupil_info`
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `class_id` (`class_id`);

-- auto increment
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE subjects ADD FOREIGN KEY (`class_id`) REFERENCES class(`id`);

-- this is a hack
delete from chapters;
ALTER TABLE `chapters` ADD FOREIGN KEY (`subject_id`) REFERENCES subjects(`id`);

-- Table for storing assignments
CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `label` varchar(20) NOT NULL,
  `class` integer(11) NOT NULL,
  `subject` integer(11) NOT NULL,
  `chapter` integer(11) NOT NULL,
  `tutor_id` integer(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `pupil_info`
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `label` (`label`);

-- auto increment
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE assignments ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE assignments ADD FOREIGN KEY (`class`) REFERENCES class(`id`);
ALTER TABLE assignments ADD FOREIGN KEY (`subject`) REFERENCES subjects(`id`);
ALTER TABLE assignments ADD FOREIGN KEY (`chapter`) REFERENCES chapters(`chapter_id`);

-- Table for recently viewed
CREATE TABLE `assignment_views` (
  `id` integer(11) NOT NULL,
  `assignment_id` integer(11) NOT NULL,
  `tutor_id` integer(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `assignment_views`
ALTER TABLE `assignment_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `assignment_id` (`assignment_id`);

-- auto increment
ALTER TABLE `assignment_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE assignment_views ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE assignment_views ADD FOREIGN KEY (`assignment_id`) REFERENCES assignments(`id`);

-- Table for question sets
CREATE TABLE `question` (
	`id` integer(11) NOT NULL,
	`assignment_id` integer(11) NOT NULL,
	`question` text NOT NULL,
	`created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `question`
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `assignment_id` (`assignment_id`);

-- auto increment
ALTER TABLE `question`
  MODIFY `id` integer(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE question ADD FOREIGN KEY (`assignment_id`) REFERENCES assignments(`id`);

-- Table for answer keys
CREATE TABLE `answer` (
	`id` integer(11) NOT NULL,
	`question_id` integer(11) NOT NULL,
	`answer` text NOT NULL,
	`created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `question`
ALTER TABLE `answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `question_id` (`question_id`);

-- auto increment
ALTER TABLE `answer`
  MODIFY `id` integer(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE answer ADD FOREIGN KEY (`question_id`) REFERENCES question(`id`);