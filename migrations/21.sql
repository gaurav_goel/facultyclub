drop table if exists assignment_migration;

-- Table for storing feed
CREATE TABLE `assignment_migration` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `started_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `completed_on` timestamp NULL,
  `status` integer(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `feed`
ALTER TABLE `assignment_migration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

-- auto increment
ALTER TABLE `assignment_migration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;