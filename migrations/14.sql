drop table if exists payments;
drop table if exists invoices;
drop table if exists balance;

CREATE TABLE `invoices`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_from` date not null,
  `date_to` date not null,
  `payee` int(11) not null,
  `recipient` int(11) not null,
  `type` varchar(20) not null,
  `status` varchar(20) not null,
  `amount` numeric(15,2) not null,
  `suggested_sessions` int(11),
  `suggested_hours` int(11),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE invoices ADD FOREIGN KEY (`payee`) REFERENCES user(`id`);
ALTER TABLE invoices ADD FOREIGN KEY (`recipient`) REFERENCES user(`id`);

CREATE TABLE `payments`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` numeric(15,2) not null,
  `discount` numeric(15,2) not null,
  `carry_forward` numeric(15,2) not null,
  `remarks` varchar(200),
  `invoice_id` int(11),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE payments ADD FOREIGN KEY (`invoice_id`) REFERENCES invoices(`id`);

CREATE TABLE `balance`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` numeric(15,2) not null,
  `payee` int(11) not null,
  `recipient` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id),
  CONSTRAINT balance_uq UNIQUE (payee, recipient)
);

ALTER TABLE invoices ADD FOREIGN KEY (`payee`) REFERENCES user(`id`);
ALTER TABLE invoices ADD FOREIGN KEY (`recipient`) REFERENCES user(`id`);