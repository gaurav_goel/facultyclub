alter table user
add column profile_completeness varchar(10) default '0000000000';

-- migrating classes
delete from class where id < 5;
insert into class (class, created_on) values ('Junior', now());
insert into class (class, created_on) values ('College', now());