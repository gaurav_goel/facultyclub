drop table if exists feed_subjects;
drop table if exists feed_classes;
drop table if exists feed_tags;

CREATE TABLE `feed_subjects`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_id` int(11) not null,
  `subject_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `feed_subjects`
  ADD KEY `id` (`id`),
  ADD KEY `feed_id` (`feed_id`),
  ADD KEY `subject_id` (`subject_id`);

ALTER TABLE `feed_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE feed_subjects ADD FOREIGN KEY (`feed_id`) REFERENCES feed(`id`);
ALTER TABLE feed_subjects ADD FOREIGN KEY (`subject_id`) REFERENCES class_subject(`id`);

CREATE TABLE `feed_classes`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_id` int(11) not null,
  `class_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `feed_classes`
  ADD KEY `id` (`id`),
  ADD KEY `feed_id` (`feed_id`),
  ADD KEY `class_id` (`class_id`);

ALTER TABLE `feed_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE feed_classes ADD FOREIGN KEY (`feed_id`) REFERENCES feed(`id`);
ALTER TABLE feed_classes ADD FOREIGN KEY (`class_id`) REFERENCES class(`id`);

CREATE TABLE `feed_tags`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_id` int(11) not null,
  `tag` varchar(40) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `feed_tags`
  ADD KEY `id` (`id`),
  ADD KEY `feed_id` (`feed_id`),
  ADD KEY `tag` (`tag`);

ALTER TABLE `feed_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE feed_tags ADD FOREIGN KEY (`feed_id`) REFERENCES feed(`id`);
