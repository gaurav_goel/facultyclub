alter table class
add column sort_order int;

update class set sort_order = 1 where class = 'Junior';
update class set sort_order = 2 where class = '5';
update class set sort_order = 3 where class = '6';
update class set sort_order = 4 where class = '7';
update class set sort_order = 5 where class = '8';
update class set sort_order = 6 where class = '9';
update class set sort_order = 7 where class = '10';
update class set sort_order = 8 where class = '11';
update class set sort_order = 9 where class = '12';
update class set sort_order = 10 where class = 'College';