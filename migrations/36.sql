ALTER TABLE pamphlets MODIFY mobile VARCHAR(100);

alter table chapters add column sort_order integer;
update chapters set sort_order = 10;

update chapters set sort_order = 1 where chapter_name = 'Quadratic Equations';
update chapters set sort_order = 2 where chapter_name = 'Arithmetic Progression';
update chapters set sort_order = 3 where chapter_name = 'Lines';
update chapters set chapter_name = 'Coordinate Geometry' where chapter_name = 'Lines';
update chapters set sort_order = 4 where chapter_name = 'Trigonometry';
update chapters set chapter_name = 'Some Applications of Trigonometry' where chapter_name = 'Trigonometry';

update chapters set sort_order = 5 where chapter_name = 'Circles';
update chapters set sort_order = 6 where chapter_name = 'Construction';
	update chapters set chapter_name = 'Constructions' where chapter_name = 'Construction';
update chapters set sort_order = 7 where chapter_name = 'Areas Related to Circles ';
update chapters set sort_order = 8 where chapter_name = 'Surface Area Volumes';
update chapters set chapter_name = 'Surface Area and Volumes' where chapter_name = 'Surface Area Volumes';

update chapters set sort_order = 9 where chapter_name = 'Probability';
update chapters set sort_order = 10 where chapter_name = 'Our Environment';
update chapters set sort_order = 11 where chapter_name = 'Management of Natural Resources';
update chapters set sort_order = 12 where chapter_name = 'Human Eye';
update chapters set sort_order = 13 where chapter_name = 'How do Organisms Reproduce';
update chapters set sort_order = 14 where chapter_name = 'Heredity and Evolution';
update chapters set sort_order = 15 where chapter_name = 'Probability';
update chapters set sort_order = 16 where chapter_name = 'Probability';
update chapters set sort_order = 17 where chapter_name = 'Probability';
update chapters set sort_order = 18 where chapter_name = 'Probability';
update chapters set sort_order = 19 where chapter_name = 'Probability';
update chapters set sort_order = 20 where chapter_name = 'Probability';
update chapters set sort_order = 21 where chapter_name = 'Probability';