alter table teaching_board
add column sort_order int;

update teaching_board set sort_order = 501 where board = 'CBSE';
update teaching_board set sort_order = 500 where board = 'ICSE';

alter table teaching_language
add column sort_order int;

update teaching_language set sort_order = 501 where language = 'English';
update teaching_language set sort_order = 500 where language = 'Hindi';

alter table user
modify column tagline varchar(250);

drop table if exists feed_content;

-- Table for storing feed
CREATE TABLE `feed_content` (
  `id` int(11) NOT NULL,
  `feed_id` int(11) NOT NULL,
  `content_url` text NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mimetype` varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `feed`
ALTER TABLE `feed_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `feed_id` (`feed_id`);

-- auto increment
ALTER TABLE `feed_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE `feed_content` ADD FOREIGN KEY (`feed_id`) REFERENCES feed(`id`) ON DELETE CASCADE;

ALTER TABLE `feed`
drop column content_url;

ALTER TABLE `feed`
drop column mimetype;

ALTER TABLE `feed`
add column is_moderated integer(1) default 0;

-- following is a hack to get the next query to work
update user set last_logged = '2016-07-09 08:31:49' where last_logged = '0000-00-00 00:00:00';
alter table user
modify column email varchar(256);
alter table user_otp
modify column email varchar(256);