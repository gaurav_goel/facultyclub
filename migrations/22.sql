ALTER TABLE `session` 
DROP FOREIGN KEY `session_ibfk_2`;
ALTER TABLE session ADD FOREIGN KEY (`group_id`) REFERENCES student_groups(`id`) ON DELETE CASCADE;

ALTER TABLE `student_group_map` 
DROP FOREIGN KEY `student_group_map_ibfk_1`;
ALTER TABLE student_group_map ADD FOREIGN KEY (`group_id`) REFERENCES student_groups(`id`) ON DELETE CASCADE;

ALTER TABLE `session_student` 
DROP FOREIGN KEY `session_student_ibfk_1`;
ALTER TABLE session_student ADD FOREIGN KEY (`session_id`) REFERENCES session(`id`) ON DELETE CASCADE;

-- unique key for student id and tutor id
ALTER TABLE student_teacher_map
ADD UNIQUE (tutor_id, student_id);

ALTER TABLE `pupil_info`
MODIFY COLUMN `fee` numeric(15, 2) NULL;

ALTER TABLE pupil_info
MODIFY COLUMN class_duration_hours int(2);

ALTER TABLE pupil_info
MODIFY COLUMN class_duration_mins int(2);