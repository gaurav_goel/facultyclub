drop table if exists transactions;

CREATE TABLE `transactions`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) not null,
  `tutor_id` int(11) not null,
  `type` varchar(20) not null,
  `amount` numeric(15,2) not null,
  `suggested_sessions` int(11),
  `suggested_hours` int(11),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD KEY `id` (`id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `type` (`type`);

-- auto increment
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE transactions ADD FOREIGN KEY (`student_id`) REFERENCES user(`id`);
ALTER TABLE transactions ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);

ALTER TABLE transactions ADD COLUMN `date_from` date;
ALTER TABLE transactions ADD COLUMN `date_to` date;
