alter table session
add column group_id int(11) NULL;

ALTER TABLE session ADD FOREIGN KEY (`group_id`) REFERENCES student_groups(`id`);