drop table if exists group_invite;

CREATE TABLE `group_invite`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11),
  `student_id` int(11) not null,
  `tutor_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);
