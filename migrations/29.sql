drop table if exists chat_messages;
drop table if exists push_tokens;

CREATE TABLE `chat_messages`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` varchar(50),
  `data` varchar(4000),
  `from` int(11) not null,
  `to` int(11) not null,
  `status` varchar(20),
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `chat_messages`
  ADD KEY `id` (`id`),
  ADD KEY `from` (`from`),
  ADD KEY `to` (`to`),
  ADD KEY `message_id` (`message_id`),
  ADD KEY `status` (`status`);

ALTER TABLE `chat_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `push_tokens`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` integer(11),
  `fcm_token` varchar(400) not null,
  `user_type` varchar(20),
  primary key (id)
);

ALTER TABLE `push_tokens`
  ADD KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE push_tokens ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`) ON DELETE CASCADE;

ALTER TABLE `push_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `branding`
  MODIFY `logo` varchar(200);