drop table if exists session_student;
drop table if exists session;

CREATE TABLE `session`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20),
  `duration_hours` int(2),
  `duration_mins` int(2),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tutor_id` int(11),
  primary key (id)
);

ALTER TABLE session ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);

-- performance : 0,1,2; attendance: 0,1
CREATE TABLE `session_student`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11),
  `student_id` int(11),
  `performance` int(1),
  `attendance` int(1),
  primary key (id)
);

-- foreign keys
ALTER TABLE session_student ADD FOREIGN KEY (`session_id`) REFERENCES session(`id`);
ALTER TABLE session_student ADD FOREIGN KEY (`student_id`) REFERENCES user(`id`);
