-- adding access tokens to the user table
ALTER TABLE `user`
  ADD column google_token varchar(250);

ALTER TABLE `user`
  ADD column facebook_token varchar(250);