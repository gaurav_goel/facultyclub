DROP TABLE if exists student_teacher_map;
DROP TABLE if exists pupil_invites;
DROP TABLE if exists student_group_map;
DROP TABLE if exists student_groups;
DROP TABLE if exists pupil_info;

-- allow dob for user data type
alter table user
add column date_of_birth date;
-- allow invitation status
alter table user
add column invitation_status tinyint(1);

-- pupil info
CREATE TABLE `pupil_info` (
  `id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `tuition_type` varchar(20) NOT NULL,
  `billing_cycle` varchar(250) NOT NULL,
  `fee` numeric(15,2) NOT NULL,
  `class_duration_hours` int(2) NOT NULL,
  `class_duration_mins` int(2) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `pupil_info`
--
ALTER TABLE `pupil_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `tuition_type` (`tuition_type`);

-- auto increment
ALTER TABLE `pupil_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE pupil_info ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);

-- groups
CREATE TABLE `student_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `pupil_info_id` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `pupil_info`
--
ALTER TABLE `student_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `pupil_info_id` (`pupil_info_id`);

-- auto increment
ALTER TABLE `student_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE student_groups ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE student_groups ADD FOREIGN KEY (`pupil_info_id`) REFERENCES pupil_info(`id`);

-- student group map table
CREATE TABLE `student_group_map` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `student_group_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `student_id` (`student_id`);

-- auto increment
ALTER TABLE `student_group_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE student_group_map ADD FOREIGN KEY (`group_id`) REFERENCES student_groups(`id`);
ALTER TABLE student_group_map ADD FOREIGN KEY (`student_id`) REFERENCES user(`id`);

-- pupil_invites
CREATE TABLE `pupil_invites` (
  `id` int(11) NOT NULL,
  `name` varchar(250),
  `phone` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `pupil_info_id` int(11) NOT NULL,
  `mail` varchar(100),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `pupil_invites`
--
ALTER TABLE `pupil_invites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `mail` (`mail`),
  ADD KEY `phone` (`phone`);

-- auto increment
ALTER TABLE `pupil_invites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE pupil_invites ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE pupil_invites ADD FOREIGN KEY (`pupil_info_id`) REFERENCES pupil_info(`id`);

-- pupil_invites
CREATE TABLE `student_teacher_map` (
  `id` int(11),
  `tuition_type` varchar(20),
  `student_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `pupil_info_id` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- indexes
ALTER TABLE `student_teacher_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `pupil_info_id` (`pupil_info_id`),
  ADD KEY `tuition_type` (`tuition_type`);

-- auto increment
ALTER TABLE `student_teacher_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE student_teacher_map ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE student_teacher_map ADD FOREIGN KEY (`student_id`) REFERENCES user(`id`);
ALTER TABLE student_teacher_map ADD FOREIGN KEY (`pupil_info_id`) REFERENCES pupil_info(`id`);
