drop table if exists tags;

CREATE TABLE `tags`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(100) not null,
  `score` int(11) DEFAULT 0,
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp,
  primary key (id),
  UNIQUE(tag)
);

ALTER TABLE `tags`
  ADD KEY `id` (`id`);

ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;