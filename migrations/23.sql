drop table if exists notifications;

-- Table for storing feed
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `type` varchar(20) NOT NULL,
  `message` varchar(400) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `seen` integer(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `feed`
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

ALTER TABLE notifications ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`);

-- auto increment
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;