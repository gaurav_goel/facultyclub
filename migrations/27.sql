drop table if exists pamphlet_templates;
drop table if exists pamphlet_subjects;
drop table if exists pamphlet_classes;
drop table if exists pamphlet_boards;
drop table if exists pamphlets;

CREATE TABLE `pamphlet_templates`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) not null,
  `description` varchar(100),
  `sample` varchar(100),
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `pamphlet_templates`
  ADD KEY `id` (`id`),
  ADD KEY `type` (`type`);

ALTER TABLE `pamphlet_templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--insert into pamphlet_templates(1, 'type1', 'some sample description', '')

CREATE TABLE `pamphlets`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) not null,
  `name` varchar(100),
  `title` varchar(100),
  `description` varchar(200),
  `logo` varchar(100),
  `address` varchar(100),
  `email` varchar(50),
  `mobile` varchar(20),
  `watermark` varchar(50),
  `brandingText` varchar(50),
  `user_id` int(11) not null,
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL,
  primary key (id)
);

ALTER TABLE `pamphlets`
  ADD KEY `id` (`id`),
  ADD KEY `type` (`type`);

ALTER TABLE pamphlets ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`);

ALTER TABLE `pamphlets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `pamphlet_subjects`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pamphlet_id` int(11) not null,
  `subject_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `pamphlet_subjects`
  ADD KEY `id` (`id`),
  ADD KEY `pamphlet_id` (`pamphlet_id`),
  ADD KEY `subject_id` (`subject_id`);

ALTER TABLE `pamphlet_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE pamphlet_subjects ADD FOREIGN KEY (`pamphlet_id`) REFERENCES pamphlets(`id`) ON DELETE CASCADE;
ALTER TABLE pamphlet_subjects ADD FOREIGN KEY (`subject_id`) REFERENCES tutor_degree_subject(`id`);

CREATE TABLE `pamphlet_classes`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pamphlet_id` int(11) not null,
  `class_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `pamphlet_classes`
  ADD KEY `id` (`id`),
  ADD KEY `pamphlet_id` (`pamphlet_id`),
  ADD KEY `class_id` (`class_id`);

ALTER TABLE `pamphlet_classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE pamphlet_classes ADD FOREIGN KEY (`pamphlet_id`) REFERENCES pamphlets(`id`) ON DELETE CASCADE;
ALTER TABLE pamphlet_classes ADD FOREIGN KEY (`class_id`) REFERENCES class(`id`);

CREATE TABLE `pamphlet_boards`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pamphlet_id` int(11) not null,
  `board_id` int(11) not null,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  primary key (id)
);

ALTER TABLE `pamphlet_boards`
  ADD KEY `id` (`id`),
  ADD KEY `pamphlet_id` (`pamphlet_id`),
  ADD KEY `board_id` (`board_id`);

ALTER TABLE `pamphlet_boards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE pamphlet_boards ADD FOREIGN KEY (`pamphlet_id`) REFERENCES pamphlets(`id`) ON DELETE CASCADE;
ALTER TABLE pamphlet_boards ADD FOREIGN KEY (`board_id`) REFERENCES teaching_board(`id`);

ALTER TABLE pamphlets add column url varchar(50);
ALTER TABLE pamphlets add column thumb_url varchar(50);
ALTER TABLE pamphlets add column subjects varchar(100);
ALTER TABLE pamphlets add column classes varchar(100);
ALTER TABLE pamphlets add column boards varchar(100);

ALTER TABLE pamphlet_templates add column is_logo_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_classses_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_subjects_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_boards_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_degrees_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_address_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_name_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_branding_text_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_description_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_contact_required int(1) default 1;
ALTER TABLE pamphlet_templates add column is_title_required int(1) default 1;
ALTER TABLE pamphlet_templates add title varchar(80);