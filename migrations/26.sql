ALTER TABLE bookmark 
   DROP INDEX module_id_2, 
   ADD UNIQUE KEY `uq_bookmark` (`module_id`,`module_type`,`tutor_id`, `folder_id`);