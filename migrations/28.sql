drop table if exists branding;

CREATE TABLE `branding`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(50),
  `branding_text` varchar(300),
  `watermark` varchar(100),
  `user_id` int(11) not null,
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL,
  primary key (id)
);

ALTER TABLE `branding`
  ADD KEY `id` (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE branding ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`);

ALTER TABLE `branding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;