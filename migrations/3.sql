drop table if exists `file_meta`;
--
-- Creates a new table for tutor locations
--
CREATE TABLE `file_meta` (
  `id` int(11) NOT NULL,
  `original_name` varchar(200) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `encoding` varchar(200) NOT NULL,
  `mimetype` varchar(60) NOT NULL,
  `uri` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `size` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `tutor_location`
--
ALTER TABLE `file_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for table `tutor_location`
--
ALTER TABLE `file_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;