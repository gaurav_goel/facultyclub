drop table if exists `tutor_location`;
--
-- Creates a new table for tutor locations
--
CREATE TABLE `tutor_location` (
  `id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `locality_id` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `tutor_location`
--
ALTER TABLE `tutor_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`),
  ADD KEY `tutor_city` (`city_id`),
  ADD KEY `tutor_locality` (`locality_id`);

--
-- AUTO_INCREMENT for table `tutor_location`
--
ALTER TABLE `tutor_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- FOREIGN_KEYS for table `tutor_location`
--
ALTER TABLE tutor_location ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE tutor_location ADD FOREIGN KEY (`city_id`) REFERENCES city(`id`);
ALTER TABLE tutor_location ADD FOREIGN KEY (`locality_id`) REFERENCES locality(`id`);