drop table if exists likes;
drop table if exists comments;
drop table if exists feed;

-- Table for storing feed
CREATE TABLE `feed` (
  `id` int(11) NOT NULL,
  `description` varchar(400),
  `tutor_id` integer(11) NOT NULL,
  `content_url` text NOT NULL,
  `num_views` integer(11) DEFAULT 0,
  `updated_on` timestamp NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `mimetype` varchar(20)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `feed`
ALTER TABLE `feed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`);

-- auto increment
ALTER TABLE `feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE `feed` ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `module_id` integer(11) NOT NULL,
  `module_type` varchar(20) NOT NULL,
  `user_id` integer(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `bookmark`
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `module_type` (`module_type`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `likes`
ADD UNIQUE (module_id, module_type, user_id);

-- auto increment
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE `likes` ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`);

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `module_id` integer(11) NOT NULL,
  `module_type` varchar(20) NOT NULL,
  `user_id` integer(11) NOT NULL,
  `comment` varchar(400) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `module_type` (`module_type`),
  ADD KEY `user_id` (`user_id`);

-- auto increment
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE `comments` ADD FOREIGN KEY (`user_id`) REFERENCES user(`id`);