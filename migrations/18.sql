drop table if exists bookmark;
drop table if exists bookmark_folder;

-- Table for storing bookmark_folder
CREATE TABLE `bookmark_folder` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `tutor_id` integer(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `bookmark_folder`
ALTER TABLE `bookmark_folder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `tutor_id` (`tutor_id`);

-- auto increment
ALTER TABLE `bookmark_folder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `bookmark_folder`
ADD UNIQUE (name, tutor_id);

-- foreign keys
ALTER TABLE `bookmark_folder` ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);

-- Table for storing bookmark
CREATE TABLE `bookmark` (
  `id` int(11) NOT NULL,
  `module_id` integer(11) NOT NULL,
  `module_type` varchar(20) NOT NULL,
  `tutor_id` integer(11) NOT NULL,
  `folder_id` integer(11),
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Indexes for table `bookmark`
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `module_type` (`module_type`),
  ADD KEY `folder_id` (`folder_id`),
  ADD KEY `tutor_id` (`tutor_id`);

ALTER TABLE `bookmark`
ADD UNIQUE KEY `uq_bookmark` (`module_id`,`module_type`,`tutor_id`, `folder_id`);

-- auto increment
ALTER TABLE `bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- foreign keys
ALTER TABLE `bookmark` ADD FOREIGN KEY (`tutor_id`) REFERENCES user(`id`);
ALTER TABLE `bookmark` ADD FOREIGN KEY (`folder_id`) REFERENCES bookmark_folder(`id`);